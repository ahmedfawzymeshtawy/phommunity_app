<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudioOptionSchedules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studio_option_schedules', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('option_id')->unsigned();
            $table->foreign('option_id')
                ->references('id')->on('studio_options')
                ->onDelete('cascade');

            $table->date('date');
            $table->time('from');
            $table->time('to');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('studio_option_schedules');
    }
}
