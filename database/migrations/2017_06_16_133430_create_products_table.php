<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');

            $table->string('type');

            $table->string('name');
            $table->text('description');
            $table->string('video_url');
            $table->decimal('price');
            $table->string('phone_numbers');
            $table->string('sign');
            $table->string('branches');
            $table->integer('category_id');
            $table->integer('brand_id');
            $table->boolean('verified');
            $table->boolean('available');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
