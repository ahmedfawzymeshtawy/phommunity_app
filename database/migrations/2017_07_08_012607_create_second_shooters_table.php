<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecondShootersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('second_shooters', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('frontend_user_id');
            $table->text('about');
            $table->string('area');
            $table->tinyInteger('rating');
            $table->boolean('featured');
            $table->boolean('approved');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('second_shooters');
    }
}
