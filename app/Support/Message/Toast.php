<?php

namespace App\Support\Message;

class Toast extends AbstractMessage {
    protected $type = 'toast';
    protected $sessionKey = 'toast';
}