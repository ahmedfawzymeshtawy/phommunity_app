<?php

namespace App\Support\Message;

abstract class AbstractMessage {
    protected $type;
    protected $level;
    protected $message;
    protected $sessionKey;


    public function setMessage($message, $level)
    {
        $this->message = $message;
        $this->level = $level;
        return $this;
    }

    public function success($message)
    {
        $this->setMessage($message, 'success');
        return $this;
    }

    public function error($message)
    {
        $this->setMessage($message, 'error');
        return $this;
    }

    public function flash()
    {
        if (!session('flash_messages')) {
            session()->flash('flash_messages', []);
        }

        session()->flash('flash_messages.' . $this->sessionKey, [
            'type' => $this->type,
            'level' => $this->level,
            'message' => $this->message
        ]);
    }
}