<?php

namespace App\Support\Message;

class Alert extends AbstractMessage {
    protected $type = 'alert';
    protected $sessionKey = 'alert';
}