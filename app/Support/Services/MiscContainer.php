<?php
namespace App\Support\Services;

use App\Models\Image;
use App\Models\Misc;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;

class MiscContainer {
    public $collection;
    public $imageFields = [
        'logo_irent', 'logo_isell', 'logo_ilearn'
    ];
    public $jsonEncodedFields = [
        'evaluation_feedback', 'frontend_page_isell', 'frontend_page_irent',
        'frontend_page_ilearn', 'frontend_page_labs', 'frontend_page_studio',
        'frontend_page_assistants_and_second_shooters', 'studio_slides','lab_slides'
    ];

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
        $this->loadRecords();
    }

    public function loadRecords()
    {
        $records = Misc::all();

        $imageItems = new Collection();
        foreach ($records as $record) {
            if (in_array($record->field, $this->imageFields)) {
                $imageItems->push($record);
            } else {
                if(in_array($record->field, $this->jsonEncodedFields)) {
                    $record->value = json_decode($record->value, true);
                }
                $this->collection->push($record);
            }
        }

        $this->loadImageRecords($imageItems);
    }

    public function loadImageRecords(Collection $imageItems)
    {
        $imageRecords = Image::whereIn('id', $imageItems->pluck('value'))->get()->keyBy('id');

        foreach ($imageItems as $item) {
            $item->value = $imageRecords->get($item->value);
            $this->collection->push($item);
        }
    }

    public function updateFields($newData)
    {
        foreach ($newData as $item) {
            $this->updateField($item['field'], $item['value']);
        }
    }

    public function updateField($field, $newValue)
    {
        $this->getRecord($field)->fill(['value' => $newValue])->save();
    }

    public function updateImageField($field, UploadedFile $uploadedImage)
    {
        $image = new Image();
        $image->upload($uploadedImage);

        $dbRecord = Misc::where('field', $field)->first();
        if ($dbRecord) {
            $dbRecord->image()->delete();

            $dbRecord->image()->create([
                'thumb_filename' => $image->thumb_filename,
                'image_filename' => $image->image_filename,
                'original_filename' => $image->original_filename
            ]);

            $this->$field->value = $image;
        }
    }

    public function getRecord($fieldName)
    {
        foreach ($this->collection as $record) {
            if ($record->field == $fieldName) return $record;
        }

        $record = new Misc();
        $record->fill(['field' => $fieldName])->save();
        $this->collection->push($record);
        return $record;
    }

    public function __get($fieldName)
    {
        $record = $this->getRecord($fieldName);
        return $record ? $record->value : null;
    }
}