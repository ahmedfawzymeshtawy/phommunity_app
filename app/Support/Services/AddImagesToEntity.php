<?php
namespace App\Support\Services;

use App\Models\Image;
use Illuminate\Support\Collection;

class AddImagesToEntity {
    protected $entity;
    protected $uploadedImages;
    protected $method;
    protected $options;
    protected $tag;


    public function __construct($uploadedImages, $entity, $method = 'images', $tag = null, $options = [])
    {
        if (!is_array($uploadedImages)) {
            $uploadedImages = [$uploadedImages];
        }

        $this->uploadedImages = $uploadedImages;
        $this->entity = $entity;
        $this->method = $method;
        $this->options = $options;
        $this->tag = $tag;
    }

    public function execute()
    {
        $images = new Collection();

        foreach ($this->uploadedImages as $uploadedImage) {
            $images->push($this->saveImage($uploadedImage));
        }

        return $images;
    }

    public function saveImage($uploadedImage)
    {
        $image = new Image(['tag' => $this->tag]);
        $image->upload($uploadedImage, $this->options);

        if ($this->entity) {
            $this->entity->{$this->method}()->create(
                $this->extractImageAttributes($image)
            );
        } else {
            $image->save();
        }

        return $image;
    }



    public function extractImageAttributes($image)
    {
        return [
            'image_filename' => $image->image_filename,
            'thumb_filename' => $image->thumb_filename,
            'original_filename' => $image->original_filename,
            'tag' => $image->tag
        ];
    }
}