<?php

function flashMessage($message, $type = 'success')
{
    session()->flash('flash_message', $message);
    session()->flash('flash_message_type', $type);
}

function toast($message, $type = 'success')
{
    session()->flash('flash_messages.toast', [
        'level' => $type,
        'message' => $message
    ]);
}

function createLocalizedColumns(&$table, array $columns)
{
    $site_locales = config('site.locales');

    foreach ($columns as $column_name => $data_type) {
        foreach ($site_locales as $locale) {
            if (is_array($data_type)) {
                $table->{$data_type[0]}($column_name . '_' . $locale, $data_type[1]);
            } else {
                $table->$data_type($column_name . '_' . $locale);
            }
        }
    }
}

function changeCPLocaleUrl($locale = null)
{
    if (is_null($locale)) {
        $locale = config('control_panel.default_locale');
    }

    $segments = request()->segments();

    $segments[1] = $locale;

    return url(implode('/', $segments));
}

function changeSiteLocale($locale = null)
{
    if (is_null($locale)) {
        $locale = config('site.default_locale');
    }

    $segments = request()->segments();

    $segments[0] = $locale;

    $url = url(implode('/', $segments));

    if (request()->getQueryString()) $url .= '?' . request()->getQueryString();

    return $url;
}

function otherCPLocale()
{
    foreach (config('control_panel.locales') as $locale) {
        if ($locale != app()->getLocale()) return $locale;
    }
}

function siteLocales()
{
    return config('site.locales');
}

function defaultLocale()
{
    return config('site.default_locale');
}

function slugLocale()
{
    if (config('site.slug_locale')) {
        return config('site.slug_locale');
    } elseif (in_array('en', siteLocales())) {
        return 'en';
    } else {
        return defaultLocale();
    }
}

function localizedTitle($locale)
{
    if (count(siteLocales()) == 1) return;

    return trans('labels.localized_title', ['locale' => trans('locales.' . $locale)]);
}

function LocalizedLabel($label, $locale)
{
    if (count(siteLocales()) == 1) return trans('labels.' . $label);

    return trans('labels.translated_label', [
        'locale' => trans('locales.' . $locale),
        'label' => trans('labels.' . $label)
    ]);
}

function extractLabelAndLocale($label)
{
    $l = explode('_', $label);

    $site_locales = siteLocales();

    $name = [];
    $locale = '';

    foreach ($l as $i) {
        in_array($i, $site_locales) ? $locale = $i : $name[] = $i;
    }

    $name = implode('_', $name);

    return [$name, $locale];
}

function pageLayouts()
{
    $layouts = [];

    foreach (config('layouts') as $layout_class) {
        $layout = new $layout_class();

        $layouts[] = [
            'name' => $layout->getName(),
            'slug' => $layout->getSlug(),
            'thumbnail' => $layout->renderThumbnail(),
            'positions' => $layout->getPositions()
        ];
    }

    return $layouts;
}

function pageModules()
{
    $modules = [];

    foreach (config('modules') as $module_class) {
        $module = new $module_class();

        $modules[] = [
            'slug' => $module->getSlug(),
            'name' => $module->getName(),
        ];
    }

    return $modules;
}

function locales()
{
    $locales = [];

    foreach (config('site.locales') as $locale) {
        $locales[] = [
            'code' => $locale,
            'name' => trans('locales.' . $locale),
            'localized_title' => localizedTitle($locale),
        ];
    }

    return $locales;
}

function layouts()
{
    $layouts = [];

    foreach (config('layouts') as $layout_class) {
        $layout = new $layout_class();

        $layouts[] = [
            'name' => $layout->getName(),
            'slug' => $layout->getSlug(),
            'thumbnail' => $layout->renderThumbnail(),
            'positions' => $layout->getPositions()
        ];
    }

    return $layouts;
}

function modules()
{
    $modules = [];

    foreach (config('modules') as $module_class) {
        $module = new $module_class();

        $modules[] = [
            'slug' => $module->getSlug(),
            'name' => $module->getName(),
        ];
    }

    return $modules;
}

function localeIcon($locale = null)
{
    if (is_null($locale)) {
        $locale = app()->getLocale();
    }

    switch ($locale) {
        case 'en':
            return 'EN';
        case 'ar':
            return 'ع';
    }
}

function selectPlaceholder($list, $label)
{
    return [0 => placeholderText($label)] + $list;
}

function placeholderText($label)
{
    return ':: ' . trans('actions.select_entity', ['entity' => $label]) . ' ::';
}

function miscLinks()
{
    return [
        ['label' => trans('labels.home'), 'uri' => '/'],
        ['label' => trans('labels.careers'), 'uri' => '/careers'],
        ['label' => trans('labels.contact_us'), 'uri' => '/contact-us'],
    ];
}

function labelTranslated($key, $parameters = [], $delimiter = ' | ')
{
    $translated = [];

    foreach (siteLocales() as $locale) {
        $translated[] = trans($key, $parameters, 'messages', $locale);
    }

    return implode($delimiter, $translated);
}

function currentNav()
{
    return config('site.current_nav');
}

use Illuminate\Support\Debug\Dumper;
use Illuminate\Support\Str;

function isValidUrl($path)
{
    if (Str::startsWith($path, ['#', '//', 'mailto:', 'tel:', 'http://', 'https://'])) {
        return true;
    }

    return filter_var($path, FILTER_VALIDATE_URL) !== false;
}

function generateNavLink($uri, $locale = null)
{
    if (is_null($locale)) {
        if (count(config('site.locales')) == 1) {
            $locale = '';
        } else {
            $appLocale = app()->getLocale();

            if (in_array($appLocale, siteLocales())) {
                $locale = $appLocale;
            } else {
                $locale = config('site.default_locale');
            }
        }
    }

    return isValidUrl($uri) ? $uri : url($locale . $uri);
}

function slugify($value)
{
    $value = str_replace(' ', '-', trim($value));
    $value = preg_replace('/[^A-Za-z0-9\-\p{Arabic}]/u', '', $value);

    $value = preg_replace('/-+/', '-', $value);

    return strtolower($value);
}

function getSiteLocaleSegment()
{
    if (count(siteLocales()) == 1) {
        $insertSegment = config('site.insert_locale_segment');

        return $insertSegment ? defaultLocale() : '';
    } else {
        $locale = request()->segment(1);

        return in_array($locale, siteLocales()) ? $locale : '';
    }
}

function getVideoInfo($url, $key)
{
    $curl = curl_init("http://www.youtube.com/oembed?url=" . $url . "&format=json");
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $return = curl_exec($curl);
    curl_close($curl);
    $response = json_decode($return, true);

    preg_match('#(?:https://)?(?:www\.)?(?:youtube\.com/(?:v/|watch\?v=)|youtu\.be/)([\w-]+)?#', $url, $match);
    $id = $match[1];
    $embedUrl = 'http://www.youtube.com/embed/' . $id . '?autoplay=0&enablejsapi=1';

    $videoInfo = [
        'title' => $response['title'],
        'thumbnail_url' => $response['thumbnail_url'],
        'embed_url' => $embedUrl,
        'id' => $id,
    ];

    return $videoInfo[$key];
}

function d()
{
    array_map(function ($x) {
        (new Dumper)->dump($x);
    }, func_get_args());
}

function sendPusherData($channel,$event,$data){
    $pusher = new Pusher\Pusher( "0eb38de360901308b5be", "e157db5b1b1c497475d8", "657113", array("cluster" => "eu") );
    // $pusher = new Pusher\Pusher( env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), array('cluster' => env('PUSHER_CLUSTER')) );
    $pusher->trigger( $channel, $event, $data );
}

function send_pusher_notification($channel,$event,$data){
    $pusher = new Pusher\Pusher( '0eb38de360901308b5be', 'e157db5b1b1c497475d8', '657113', array('cluster' => 'eu') );
//    $data=['link'=>'https://google.com','message'=>'done receive','title'=>'test notification'];
    $pusher->trigger( $channel, $event, json_encode($data) );
}



function send_notifications_users($usersId, $data, $type = 0) {
    $limit = 100;
    $event = $type === 0 ? 'user_notification' : 'admin_user_notification';
    $prefix = $type === 0 ? 'user_channel_' : 'admin_user_channel_';
    $channels = collect((array)$usersId)->map(function ($item, $key) use($prefix) { return $prefix . $item; });
    $channelsArr = $channels->toArray();
    // more than 100 "$limit" user channel
    $channelsCount = count($channelsArr);
    if ( $channelsCount > $limit ) {
        $amount = (int) floor($channelsCount / $limit);
        $remaining = $channelsCount - ($amount * $limit); 
        // for each 100 "$limit" user channel do
        for ($i = 0; $i < $amount; $i++) {
            // get amount notifications
            $counter = $i * $limit;
            $usersChannels = array_slice($channelsArr, $counter, $limit);
            // send amount notifications
            send_pusher_notification($usersChannels, $event, $data);
        }
        // get remaining notifications
        $counter = $amount * $limit;
        $usersChannels = array_slice($channelsArr, $counter, $limit);
        // send remaining notifications
        send_pusher_notification($usersChannels, $event, $data);
        save_users_notifications($usersId, $data, $type);
    } else {
        // less than 100 "$limit" user channel
        send_pusher_notification($channelsArr, $event, $data);
        save_users_notifications($usersId, $data, $type);
    }
}


function save_users_notifications($usersId, $data, $type) {
    foreach ($usersId as $user){
        save_notification($user,$data,$type);
    }
}

function send_notification_users($users, $data, $type = 0){
    if(is_array($users)){
        send_notifications_users($users, $data, $type);
    }
}

function send_notification_admins($users, $data, $type = 1){
    if(is_array($users)){
        send_notifications_users($users, $data, $type);
    }
}

function save_notification($user,$data,$type){
    $notfi = new \App\Models\Notification();
    $notfi->type    = $type;
    $notfi->user_id = $user;
    $notfi->title   = $data['title'];
    $notfi->message = $data['message'];
    $notfi->link    = isset($data['link']) ? $data['link'] : '';
    $notfi->modal   = isset($data['modal']) ? $data['modal'] : '0';
    $notfi->save();
}


function IsNullOrEmptyString($str){
    return (!isset($str) || trim($str) === '');
}

// fix empty links array 
function unsetEmptyArrItem($reqArr, $key){
    if ( isset($reqArr[$key]) && $reqArr[$key] != '' && count($reqArr[$key]) > 0 ) {    
        for ($i=0; $i < count($reqArr[$key]) ; $i++) { 
            if ( $reqArr[$key][$i] == '' || $reqArr[$key][$i] == null ) {
                unset($reqArr[$key][$i]);
            }
        }
    }
    return $reqArr;
}








