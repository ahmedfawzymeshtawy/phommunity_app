<?php
namespace App\Support\Traits;

trait ImageDeleteTrait {

    public static function bootImageDeleteTrait()
    {
        static::deleted(function ($modal) {
            \File::delete(IMAGE_PATH . $modal->image_filename);
            \File::delete(THUMB_PATH . $modal->thumb_filename);
        });
    }
}