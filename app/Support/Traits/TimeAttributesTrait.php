<?php
namespace App\Support\Traits;

use Carbon\Carbon;

trait TimeAttributesTrait
{

    public function __get($name)
    {
        if (in_array($name, $this->times)) {
            return Carbon::parse( $this->getAttribute($name));
        }

        return parent::__get($name);
    }
}