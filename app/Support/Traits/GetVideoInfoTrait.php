<?php
namespace App\Support\Traits;


trait GetVideoInfoTrait {
    protected $videoInfo;

    public function getVideoInfo($key)
    {
        if (!$this->videoInfo) {
            $curl = curl_init("http://www.youtube.com/oembed?url=" . $this->video_url . "&format=json");
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $return = curl_exec($curl);
            curl_close($curl);
            $response = json_decode($return, true);
            preg_match('#(?:https://)?(?:www\.)?(?:youtube\.com/(?:v/|watch\?v=)|youtu\.be/)([\w-]+)?#', $this->video_url, $match);

            $id = isset($match[1]) ? $match[1] : '';
            $embedUrl = 'https://www.youtube.com/embed/' . $id . '?autoplay=0&enablejsapi=1';

            $this->videoInfo = [
                'title' => $response['title'],
                'thumbnail_url' => $response['thumbnail_url'],
                'embed_url' => $embedUrl,
                'id' => $id,
            ];
        }

        return $this->videoInfo[$key];
    }
}