<?php

if(!defined('IMAGE_PATH')) {
    define('IMAGE_PATH', env('UPLOAD_PATH').'images/');
}

if(!defined('THUMB_PATH')) {
    define('THUMB_PATH', env('UPLOAD_PATH').'thumbs/');
}

if(!defined('DOCUMENT_PATH')) {
    define('DOCUMENT_PATH', env('UPLOAD_PATH').'documents/');
}