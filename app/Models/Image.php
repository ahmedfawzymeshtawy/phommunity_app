<?php
namespace App\Models;

use App\Support\Traits\ImageDeleteTrait;
use App\Support\Traits\ImageUploadTrait;
use Illuminate\Database\Eloquent\Model;

class Image extends Model {

    use ImageUploadTrait, ImageDeleteTrait;

    protected $table = 'images';

    protected $fillable = [
        'thumb_filename', 'image_filename', 'original_filename', 'tag'
    ];

    public function imageFullUrl($absolute = true)
    {
        return asset( 'uploads/images/'. $this->image_filename );
    }

    public function imageFullUrll($absolute = true)
    {
        return env('UPLOAD_PATH').'images/'. $this->image_filename ;
    }

    public function thumbFullUrl($absolute = true)
    {
        return asset( env('UPLOAD_PATH').'thumbs/'. $this->thumb_filename );
    }

    public function imageable()
    {
        return $this->morphTo();
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($image) {
            $imgPath = '/home4/pfppvkmy/public_html/phommunity/public/uploads/images/' . $image->image_filename;
            $thumPath = '/home4/pfppvkmy/public_html/phommunity/public/uploads/thumbs/' . $image->thumb_filename;
            if (file_exists($imgPath)) unlink($imgPath);
            if (file_exists($thumPath)) unlink($thumPath);
        });
    }

}
