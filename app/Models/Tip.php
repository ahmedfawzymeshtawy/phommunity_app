<?php
namespace App\Models;

use App\Support\Traits\AddImageTrait;
use App\Support\Traits\GetVideoInfoTrait;
use Illuminate\Database\Eloquent\Model;

class Tip extends Model {

    use AddImageTrait, GetVideoInfoTrait;

    protected $fillable = [
        'title', 'description', 'video_url', 'notifications'
    ];


    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable');
    }
}
