<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventInstructor extends Model {

    protected $fillable = [
        'name','event_id','bio','image'
    ];

    public function event()
    {
        return $this->belongsTo('App\Models\Event');
    }

    public function imageName()
    {
        $picName = str_replace("uploads/images/","", $this->image);
        return $picName;
    }

    // public static function boot() {
    //     parent::boot();

    //     static::deleting(function($instructor) {
    //         if ( $instructor->image != '' ) unlink($instructor->image);
             
    //     });
    // }

}
