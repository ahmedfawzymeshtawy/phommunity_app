<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use NotificationChannels\PusherPushNotifications\PusherChannel;
use NotificationChannels\PusherPushNotifications\PusherMessage;
use Illuminate\Notifications\Notification;

class Notification extends Model {
    
    protected $fillable = [
        'title', 'message', 'link','type','user_id','readed'
    ];


    public function users()
    {
        return $this->belongsToMany('App\Models\FrontendUser');
    }

    public function via($notifiable)
    {
        return [PusherChannel::class];
    }

    public function toPushNotification($notifiable)
	{
	    $message = "Your {$notifiable->service} account was approved!";

	    return PusherMessage::create()
	        ->iOS()
	        ->badge(1)
	        ->body($message)
	        ->withAndroid(
	            PusherMessage::create()
	                ->title($message)
	                ->icon('icon')
	        );
	}
}
