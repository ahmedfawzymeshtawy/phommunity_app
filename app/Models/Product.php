<?php

namespace App\Models;

use App\Support\Traits\AddImageTrait;
use App\Support\Traits\GetVideoInfoTrait;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    use AddImageTrait, GetVideoInfoTrait;

    protected $casts = [
        'description' => 'array',
    ];

    protected $fillable = [
        'name', 'description', 'category_id', 'brand_id',
        'video_url', 'price', 'phone_numbers', 'sign',
        'type', 'verified', 'branches', 'available','priceHalf','notes'
    ];

    public function getPhoneNumbersAttribute($value)
    {
        return collect(json_decode($value));
    }

    public function wishlist()
    {
        return $this->hasOne('App\Models\Wishlist','type_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function brand()
    {
        return $this->belongsTo('App\Models\Brand');
    }

    public function orders()
    {
        return $this->morphMany('App\Models\Order', 'orderable');
    }

    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable');
    }

    public function getBranchesAttribute()
    {
        $branches = [];

        if($this->attributes['branches']) {
            $branches = json_decode($this->attributes['branches']);
        }

        return collect($branches);
    }

    public static function boot()
    {

        parent::boot();
        static::deleted(function ($model) {
            foreach ($model->images as $image) $image->delete();
        });
    }
}
