<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class AdminUser extends Authenticatable {

    protected $table = 'admin_users';

    protected $fillable = [
        'name', 'email', 'password', 'permissions'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
    
    protected $permissionsList;


    public function canSee($entity)
    {
        if (!$this->permissionsList) {
            $this->permissionsList = $this->attributes['permissions']
                ? json_decode($this->attributes['permissions'])
                : [];
        }

        return in_array($entity, $this->permissionsList);
    }

    public function chatMessage()
    {
        return $this->hasMany('App\Models\ChatMessage');
    }


    public function isSuperAdmin()
    {
        return ($this->superAdmin == 'true');
    }
}
