<?php

namespace App\Models;

use App\Support\Traits\AddImageTrait;
use App\Support\Traits\TimeAttributesTrait;
use Illuminate\Database\Eloquent\Model;

class StudioOptionSchedule extends Model
{
    use TimeAttributesTrait;

    protected $fillable = [
        'option_id', 'date', 'from', 'to'
    ];
    protected $dates = ['date'];
    protected $times = ['from', 'to'];


    public function option()
    {
        return $this->belongsTo('App\Models\StudioOption', 'option_id');
    }
}
