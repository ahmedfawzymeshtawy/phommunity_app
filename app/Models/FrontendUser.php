<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use App\Models\Order;
use App\Models\IrentOrders;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use App\Notifications\PasswordResetNotification;



class FrontendUser extends Authenticatable 
{

    use AuthenticableTrait , HasApiTokens, Notifiable;
    
    protected $appends = ['chatNewMessagesCount'];

    protected $table = 'frontend_users';

    protected $fillable = [
        'first_name', 'last_name', 'email', 'mobile', 'level',
        'camera_brand', 'camera_model',
        'interested_in', 'password', 'active',
        'current_points', 'verification_token','gender','first_sign',
        'avatar', 'provider','provider_id','provider_email',
        'country','state','city','birthday',
    ];

    protected $hidden = [
        'password', 'remember_token', 'verification_token'
    ];
    

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordResetNotification($token));
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getFullNameAttribute()
    {
        $fullName = '';
        if ($this->first_name) $fullName .= ucfirst($this->first_name) . ' ';
        if ($this->last_name) $fullName .= ucfirst($this->last_name);
        return $fullName;
    }


    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    
    public function photo()
    {
        return $this->morphOne('App\Models\Image', 'imageable');
    }

    public function assistant()
    {
        return $this->hasOne('App\Models\Assistant');
    }

    public function secondShooter()
    {
        return $this->hasOne('App\Models\SecondShooter');
    }

    public function points()
    {
        return $this->hasMany('App\Models\Point');
    }

    public function IrentOrders()
    {
        return $this->hasMany('App\Models\Order')->where('type','I-Rent');
    }

    public function messages()
    {
        return $this->hasMany('App\Models\Message');
    }

    public function chatMessage()
    {
        return $this->hasMany('App\Models\ChatMessage');
    }

    public function evaluations()
    {
        return $this->hasMany('App\Models\Evaluation', 'user_id');
    }

    public function wishlist()
    {
        return $this->hasMany('App\Models\Wishlist')->orderBy('created_at','desc');
    }

    public function notifications()
    {
        return $this->hasMany('App\Models\Notification', 'user_id');
    }

    public function newMessagesCountRelation()
    {
        return $this->hasOne('App\Models\Message')
            ->selectRaw('frontend_user_id, count(*) as count')
            ->where('new', 1)
            ->groupBy('frontend_user_id');
    }

    public function getNewMessagesCountAttribute()
    {
        if ($this->newMessagesCountRelation) {
            return $this->newMessagesCountRelation->count;
        }

        return 0;
    }

    public function newChatMessagesCountRelation()
    {
        return $this->hasOne('App\Models\ChatMessage')
            ->selectRaw('frontend_user_id, count(*) as count')
            ->where('seen', 0)
            ->groupBy('frontend_user_id');
    }

    public function getChatNewMessagesCountAttribute()
    {
        if ($this->newChatMessagesCountRelation) {
            return $this->newChatMessagesCountRelation->count;
        }

        return 0;
    }

    public function getCurrentPointsAttribute()
    {
        $points = 0;

        foreach ($this->points as $record) {
            if ($record->type == Point::TYPE_ADD) {
                $points += $record->count;
            } else {
                $points -= $record->count;
            }
        }

        return $points;
    }

    public function canRequest($user)
    {
        foreach ($user->orders as $order) {
            if (
                $order->frontend_user_id == $this->id &&
                $order->status == Order::STATUS_PENDING
            ) {
                return false;
            }
        }

        return true;
    }

    public function getPendingRequestFor($user)
    {
        foreach ($user->orders as $order) {
            if (
                $order->frontend_user_id == $this->id &&
                $order->status == Order::STATUS_PENDING
            ) {
                return $order;
            }
        }

        return false;
    }


    public static function boot() {
        parent::boot();
        static::deleting(function($user) {
            // before delete() a User call this
            // Delete
            // user->photo
            if (!empty($user->photo)) $user->photo->delete();
            // user->points
            if (!empty($user->points) && count($user->points) > 0 ) $user->points()->delete();
            // user->messages
            if (!empty($user->messages) && count($user->messages) > 0 ) $user->messages()->delete();
            // user->chatMessage
            // if (!empty($user->chatMessage) && count($user->chatMessage) > 0 ) $user->chatMessage()->delete();
            // user->evaluations
            // if (!empty($user->evaluations) && count($user->evaluations) > 0 ) $user->evaluations()->delete();
            // // user->wishlist
            // if (!empty($user->wishlist) && count($user->wishlist) > 0 ) $user->wishlist()->delete();
            // user->notifications
            if (!empty($user->notifications) && count($user->notifications) > 0 ) $user->notifications()->delete();
            // user->orders commentHistory - statusHistory
            $user->orders->each(function($order, $key){
                $order->commentHistory()->delete();
                $order->statusHistory()->delete();
                $order->feedback()->delete();
            });
            // user->orders
            if (!empty($user->orders) && count($user->orders) > 0 ) $user->orders()->delete();
        });
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
        
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function egycity()
    {
        return $this->belongsTo('App\Models\EgyptCities');
    }

}
