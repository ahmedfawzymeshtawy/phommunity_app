<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RamadanCommentHistory extends Model {

    protected $fillable = [
        'user_id','ramadan_id','comment'
    ];

    public function ramadan()
    {
        return $this->belongsTo('App\Models\Ramadan');
    }

    public function getAdminName()
    {
        if ($this->user_id != '') {
            $admin = \App\Models\AdminUser::where('id', $this->user_id)->get();
            return $admin->first() ? $admin->first()->name : '';
        }
    }

}
