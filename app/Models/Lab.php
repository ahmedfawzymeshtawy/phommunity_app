<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lab extends Model {
    
    protected $fillable = [
        'title', 'about', 'image_id', 'price',
        'contact_numbers', 'available'
    ];

    public function getContactNumbersAttribute($value)
    {
        return collect(json_decode($value));
    }

    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable');
    }

    public function orders()
    {
        return $this->morphMany('App\Models\Order', 'orderable');
    }
}
