<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\Course;

class ScoreSuggest extends Model {

    protected $fillable = [ 'category', 'score', 'course_id' ];


    public function course()
    {
        return Course::find($this->course_id);
    }

}
