<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ramadan extends Model {

    protected $fillable = [
        'name','mobile','branch','mail','courses','workshops','job','note'
    ];

    public function commentHistory()
    {
        return $this->hasMany('App\Models\RamadanCommentHistory')->latest();
    }

    public function statusHistory()
    {
        return $this->hasMany('App\Models\RamadanStatusHistory', 'ramadan_id')->latest();
    }

    public function getAdminName()
    {
        if ($this->admin_id != '') {
            $admin = \App\Models\AdminUser::where('id', $this->admin_id)->get();
            return $admin->first() ? $admin->first()->name : '';
        }
    }


    public static function boot() {
        parent::boot();
        static::deleting(function($ramadan) {
            $ramadan->commentHistory()->delete();
            $ramadan->statusHistory()->delete();
        });
    }
    
}
