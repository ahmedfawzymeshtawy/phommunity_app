<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Organizer extends Model {

    protected $fillable = [
        'name','bio','links','image'
    ];

    protected $casts = [
        'links' => 'array',
        'bio' => 'array',
    ];

    // public function instructable()
    // {
    //     return $this->morphTo();
    // }

    public function getLinksAttribute($value){
        return json_decode($value, true);
    }


    public function courses()
    {
        return $this->belongsToMany('App\Models\Course');
    }

    public function imageName()
    {
        $picName = str_replace("uploads/images/","", $this->image);
        return $picName;
    }

    // public static function boot() {
    //     parent::boot();

    //     static::deleting(function($instructor) {
    //         if ( $instructor->image != '' ) unlink($instructor->image);
             
    //     });
    // }

}
