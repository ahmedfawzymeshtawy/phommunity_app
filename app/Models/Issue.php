<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Issue extends Model
{
    protected $guard=['id'];
    public function client()
    {
      $client='guest';
      if($this->belongsTo('App\Models\FrontendUser','frontend_users'))
      {
        $client=$this->belongsTo('App\Models\FrontendUser','frontend_users');
      }
      return $client;
    }
}
