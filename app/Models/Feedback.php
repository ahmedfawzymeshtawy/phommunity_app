<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model {

    protected $fillable = [
        'frontend_user_id', 'body','notes'
    ];
    protected $table = 'feedback';

    public function frontendUser()
    {
        return $this->belongsTo('App\Models\FrontendUser');
    }
}
