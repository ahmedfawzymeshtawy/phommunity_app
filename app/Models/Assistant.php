<?php
namespace App\Models;

use App\Support\Traits\AddImageTrait;
use Illuminate\Database\Eloquent\Model;

class Assistant extends Model {

    use AddImageTrait;

    protected $fillable = [
        'frontend_user_id', 'about',
        'area', 'rating', 'approved',
        'featured'
    ];


    public function frontendUser()
    {
        return $this->belongsTo('App\Models\FrontendUser', 'frontend_user_id', 'id');
    }

    public function orders()
    {
        return $this->morphMany('App\Models\Order', 'orderable');
    }

    public function getReviewsCountAttribute()
    {
        $count = 0;

        foreach ($this->orders as $order) {
            if($order->meta('review')) $count++;
        }

        return $count;
    }

}
