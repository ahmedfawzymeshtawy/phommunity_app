<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RamadanStatusHistory extends Model {

    protected $fillable = [
        'user_id','ramadan_id','status'
    ];

    public function ramadan()
    {
        return $this->belongsTo('App\Models\Ramadan');
    }

    public function getAdminName()
    {
        if ($this->user_id != '') {
            $admin = \App\Models\AdminUser::where('id', $this->user_id)->get();
            return $admin->first() ? $admin->first()->name : '';
        }
    }

}
