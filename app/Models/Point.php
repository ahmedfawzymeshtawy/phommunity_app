<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    const TYPE_ADD = 'add';
    const TYPE_DEDUCT = 'deduct';

    protected $fillable = [
        'frontend_user_id', 'admin_user_id', 'notes',
        'receipt_number', 'count', 'type'
    ];


    public function frontendUser()
    {
        return $this->belongsTo(FrontendUser::class);
    }

    public function adminUser()
    {
        return $this->belongsTo(AdminUser::class);
    }
}
