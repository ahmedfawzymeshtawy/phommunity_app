<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IrentOrdersProducts extends Model {

    protected $fillable = [
        'order_id','product_id','name','amount', 'duration','price12','price24'
    ];

    public function order()
    {
        return $this->belongsTo('App\Models\IrentOders', 'irent_orders');
    }

}
