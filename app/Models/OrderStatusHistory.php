<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStatusHistory extends Model {

    protected $fillable = [
        'user_id','order_id','status'
    ];

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    public function irentOrder()
    {
        return $this->belongsTo('App\Models\IrentOders');
    }

    public function getAdminName()
    {
        if ($this->user_id != '') {
            $admin = \App\Models\AdminUser::where('id', $this->user_id)->get();
            return $admin->first() ? $admin->first()->name : '';
        }
    }

}
