<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    const STATUS_PENDING = 'pending';

    protected $fillable = [
        'frontend_user_id', 'type', 'meta', 'notes', 'new', 'status'
    ];
    protected $metaFields;
    protected $casts = [
        'meta' => 'json',
       
    ];

    public function frontendUser()
    {
        return $this->belongsTo('App\Models\FrontendUser');
    }

    public function orderable()
    {
        return $this->morphTo();
    }

    public function course()
    {
        return $this->belongsTo('App\Models\Course', 'orderable_id')->latest();
    }

    public function event()
    {
        return $this->belongsTo('App\Models\Event', 'orderable_id')->latest();
    }

    public function rooms()
    {
        return $this->belongsTo('App\Models\Lab', 'orderable_id')->latest();
    }

    public function feedback()
    {
        return $this->hasMany('App\Models\OrderFeedback', 'order_id')->latest();
    }

    public function commentHistory()
    {
        return $this->hasMany('App\Models\CommentHistory')->latest();
    }

    public function statusHistory()
    {
        return $this->hasMany('App\Models\OrderStatusHistory')->latest();
    }

    public function getAdminName()
    {
        if ($this->admin_id != '') {
            $admin = \App\Models\AdminUser::where('id', $this->admin_id)->get();
            return $admin->first() ? $admin->first()->name : '';
        }
    }

    public function products(){
        return json_decode($this->meta('products'), true);
    }
    
    public function userName(){
        if(isset($this->frontendUser->first_name)) {
            return $this->frontendUser->first_name .' '. $this->frontendUser->last_name;
        }
    }

    public function productNames(){
        $productsArr = $this->products();
        $productsNames = '';
        for ($i=0; $i < count($productsArr) ; $i++) { 
            if ((count($productsArr) - 1) == $i) {
                $productsNames .= $productsArr[$i]['name'];
            } else {
                $productsNames .= $productsArr[$i]['name'] .', ';
            }
        }
        return $productsNames;
    }

    public function total()
    {
        $productsArr = $this->products();
        $total = 0;
        for ($i=0; $i < count($productsArr) ; $i++) { 
            if ( isset($productsArr[$i]['day24']) && $productsArr[$i]['day24'] == 'true' ) {
                
                $total = $total + ($productsArr[$i]['amount'] * $productsArr[$i]['price24']);
            } else {
                $total = $total + ($productsArr[$i]['amount'] * $productsArr[$i]['price12']);
            }
        }
        return $total;
    }


    public function meta($field, $default = '')
    {
        if (!$this->metaFields) {
            $this->metaFields = json_decode($this->attributes['meta'], true);
        }

        if (is_array($this->metaFields) && array_key_exists($field, $this->metaFields)) {
            return $this->metaFields[$field];
        }

        return $default;
    }

    public static function boot() {
        parent::boot();
        static::deleting(function($order) {
            $order->commentHistory()->delete();
            $order->statusHistory()->delete();
        });
    }
}
