<?php
namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model {


    protected $fillable = [
        'user_name', 'user_id', 'category', 'mark',
    ];


    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($event) {
            //  $event->instructors()->delete();
            //  $event->organizers()->delete();
        });
    }
}
