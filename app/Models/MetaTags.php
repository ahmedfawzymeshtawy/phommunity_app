<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MetaTags extends Model {

    protected $fillable = ['keywords', 'description'];

    public function page()
    {
        return $this->belongsToMany('App\Models\Pages');
    }
}
