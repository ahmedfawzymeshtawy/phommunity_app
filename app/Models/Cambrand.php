<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cambrand extends Model
{
    protected $table="cambrands";

    public function models(){
        return $this->hasMany(CamModel::CLASS,'brand_id','id');
    }
    //
}
