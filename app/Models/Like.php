<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Like extends Model {

    protected $fillable = [
        'frontend_user_id', 'post_id'
    ];

    public function frontendUser()
    {
        return $this->belongsTo('App\Models\FrontendUser');
    }

    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }
}
