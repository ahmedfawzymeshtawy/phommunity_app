<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model {
    
    protected $appends = ['date', 'time'];

    protected $fillable = [
        'frontend_user_id', 'admin_id', 'message', 'seen','admin', 'file_path', 'image', 'file'
    ];


    public function frontendUser()
    {
        return $this->belongsTo('App\Models\FrontendUser');
    }

    public function adminUser()
    {
        return $this->belongsTo('App\Models\AdminUser', 'admin_id');
    }

    public function getDateAttribute() {
        return $this->created_at != null ? $this->created_at->format('d/m/Y') : '';
    }

    public function getTimeAttribute() {
        return $this->created_at != null ? $this->created_at->format('h:i A') : '';
    }

}
