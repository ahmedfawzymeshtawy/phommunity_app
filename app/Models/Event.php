<?php
namespace App\Models;

use App\Support\Traits\AddImageTrait;
use App\Support\Traits\GetVideoInfoTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use App\Models\Course;

class Event extends Model {

    use AddImageTrait, GetVideoInfoTrait;

    protected $dates = ['date_from', 'date_to'];

    protected $appends = ['courses_related','events_related'];

    protected $casts = [
        'requirements' => 'array',
        'descriptions' => 'array',
        'phone_numbers' => 'array',
        'related_events' => 'array',
        'related_courses' => 'array',
    ];

    protected $fillable = [
        'name', 'descriptions', 'date_from', 'date_to', 'time_from', 'time_to','price','related_courses','related_events',
        'location', 'video_url', 'address','requirements','faq','cover_photo','phone_numbers'
    ];

    
    public function getCoursesRelatedAttribute() {
        return Course::find( $this->related_courses );
    }

    public function getEventsRelatedAttribute() {
        return Event::find( $this->related_events );
    }


    public function instructors()
    {
        return $this->belongsToMany('App\Models\Instructor', 'event_instructor', 'event_id','instructor_id');
    }
        
    public function organizers()
    {
        return $this->belongsToMany('App\Models\Organizer', 'event_organizer', 'event_id','organizer_id');
    }
    
    public function eventTypes()
    {
        return $this->belongsToMany('App\Models\EventType', 'event_event_type', 'event_id','type_id');
    }

    public function wishlist()
    {
        return $this->hasOne('App\Models\Wishlist','type_id');
    }
    
    public function orders()
    {
        return $this->morphMany('App\Models\Order', 'orderable');
    }

    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable')->where('tag', 'event');
    }

    public function coverPicName()
    {
        $picName = str_replace(env('UPLOAD_PATH')."images/","", $this->cover_photo);
        return $picName;
    }

    public function getPhoneNumbersAttribute($value)
    {
        return collect(json_decode($value));
    }

    public function isDue()
    {
        return Carbon::parse($this->date_to)->lt(Carbon::now());
    }

    public function addDocument(UploadedFile $file)
    {
        $this->document = time() . '_' . $file->getClientOriginalName();
        $file->move(DOCUMENT_PATH, $this->document);
    }

    public static function boot() {
        parent::boot();

    }
}
