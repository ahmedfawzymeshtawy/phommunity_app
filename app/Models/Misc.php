<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Misc extends Model {
    protected $fillable = ['field', 'value'];
    protected $table = 'misc';


    public function image()
    {
        return $this->morphOne('App\Models\Image', 'imageable');
    }
}
