<?php

namespace App\Models;

use App\Support\Traits\AddImageTrait;
use Illuminate\Database\Eloquent\Model;

class StudioOption extends Model
{
    use AddImageTrait;

    protected $dates = ['date_available'];

    protected $fillable = [
        'title', 'about', 'price', 'contact_numbers'
    ];


    public function getContactNumbersAttribute($value)
    {
        return collect(json_decode($value));
    }

    public function image()
    {
        return $this->morphOne('App\Models\Image', 'imageable');
    }

    public function schedules()
    {
        return $this->hasMany('App\Models\StudioOptionSchedule', 'option_id');
    }

    public function orders()
    {
        return $this->morphMany('App\Models\Order', 'orderable');
    }

    public static function boot()
    {
        static::deleting(function ($model) {
            $model->schedules()->delete();
        });

        parent::boot();
    }
}
