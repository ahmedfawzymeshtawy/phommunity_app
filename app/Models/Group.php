<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model {

    protected $fillable = ['name', 'notes'];


    public function users()
    {
        return $this->belongsToMany('App\Models\FrontendUser')
            ->withPivot(['approved'])
            ->withTimestamps();
    }

    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }
}
