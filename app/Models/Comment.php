<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

    protected $fillable = [
        'frontend_user_id', 'post_id', 'body'
    ];

    public function frontendUser()
    {
        return $this->belongsTo('App\Models\FrontendUser');
    }

    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }
}
