<?php
namespace App\Models;

use App\Support\Traits\AddImageTrait;
use App\Support\Traits\GetVideoInfoTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Models\Event;

class Course extends Model {

    use AddImageTrait, GetVideoInfoTrait;

    protected $dates = ['date_from', 'date_to'];

    protected $appends = ['courses_related','events_related'];

    protected $casts = [
        'lectures' => 'array',
        'requirements' => 'array',
        'descriptions' => 'array',
        'phone_numbers' => 'array',
        'related_events' => 'array',
        'related_courses' => 'array',
        'next_rounds' => 'array',
    ];

    protected $fillable = [
        'name', 'descriptions','cover_photo','lecture_hours','lectures_count','lectures',
        'video_url','position','price','requirements','next_rounds',
        'date_from','date_to','phone_numbers','faq','related_events','related_courses'
    ];

    
    public function getCoursesRelatedAttribute() {
        return Course::find( $this->related_courses );
    }

    public function getEventsRelatedAttribute() {
        return Event::find( $this->related_events );
    }


    public function instructors()
    {
        return $this->belongsToMany('App\Models\Instructor', 'course_instructor', 'course_id','instructor_id');
    }

    public function organizers()
    {
        return $this->belongsToMany('App\Models\Organizer', 'course_organizer', 'course_id','organizer_id');
    }
    

    public function wishlist()
    {
        return $this->hasOne('App\Models\Wishlist','type_id');
    }

    public function orders()
    {
        return $this->morphMany('App\Models\Order', 'orderable');
    }

    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable')->where('tag', 'course');
    }

    public function instructorImage()
    {
        return $this->morphOne('App\Models\Image', 'imageable')->where('tag', 'instructor');
    }

    public function coverPicName()
    {
        $picName = str_replace(env('UPLOAD_PATH')."images/","", $this->cover_photo);
        return $picName;
    }

    public function getDaysAttribute()
    {
        return collect(json_decode($this->attributes['days'], true));
    }


    public function getPhoneNumbersAttribute($value)
    {
        return collect(json_decode($value, true));
    }

    public function isDue()
    {
        return Carbon::parse($this->date_to)->lt(Carbon::now());
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($course) { // before delete() method call this
            //  $course->instructors()->delete();
             // do the rest of the cleanup...
        });
    }
}
