<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    protected $fillable = [
        'frontend_user_id', 'group_id', 'body'
    ];

    public function frontendUser()
    {
        return $this->belongsTo('App\Models\FrontendUser');
    }

    public function group()
    {
        return $this->belongsTo('App\Models\Group');
    }

    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }

    public function likes()
    {
        return $this->hasMany('App\Models\Like');
    }

    public function addImages($uploadedImages)
    {
        $ids = [];
        foreach ($uploadedImages as $uploadedImage) {
            $image = new Image();
            $image->upload($uploadedImage)->save();
            $ids[] = $image->id;
        }

        $this->images()->attach($ids);
    }

    public function files(){
        return $this->hasMany(Files::CLASS,'post_id','id');
    }
}
