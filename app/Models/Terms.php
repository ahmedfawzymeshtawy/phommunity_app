<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Terms extends Model
{
    protected $table="terms";

    public static function Types(){
        return [
          'irent','isell','assistant','second shooter'
        ];
    }
    //
}
