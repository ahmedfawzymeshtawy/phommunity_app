<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderFeedback extends Model {

    protected $fillable = ['feedback', 'order_id'];

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }
}
