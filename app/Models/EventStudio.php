<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventStudio extends Model {

    protected $fillable = [
        'name','mobile','branch','mail','type','job','note'
    ];
}
