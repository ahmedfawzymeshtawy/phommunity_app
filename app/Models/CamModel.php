<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CamModel extends Model
{
    protected $table="models";
    public function brand(){
        return $this->belongsTo(Cambrand::CLASS,'brand_id','id');
    }
    //
}
