<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoursesEvent extends Model {

    protected $fillable = [
        'name','mobile','branch','mail','courses','job','note'
    ];

    public function commentHistory()
    {
        return $this->hasMany('App\Models\CoursesEventCH')->latest();
    }

    public function statusHistory()
    {
        return $this->hasMany('App\Models\CoursesEventSH')->latest();
    }

    public function getAdminName()
    {
        if ($this->admin_id != '') {
            $admin = \App\Models\AdminUser::where('id', $this->admin_id)->get();
            return $admin->first() ? $admin->first()->name : '';
        }
    }


    public static function boot() {
        parent::boot();
        static::deleting(function($coursesEvent) {
            $coursesEvent->commentHistory()->delete();
            $coursesEvent->statusHistory()->delete();
        });
    }
    
}
