<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VisitorMessage extends Model {

    protected $fillable = [
        'sender_name', 'sender_email_address',
        'sender_phone_number',
        'body', 'new'
    ];
}
