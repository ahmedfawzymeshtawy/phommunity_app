<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model {

    protected $fillable = [
        'frontend_user_id', 'type','type_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\FrontendUser');
    }
    
    public function course()
    {
        return $this->belongsTo('App\Models\Course', 'type_id');
    }
    
    public function event()
    {
        return $this->belongsTo('App\Models\Event', 'type_id');
    }

    public function irentProduct()
    {
        return $this->belongsTo('App\Models\Product', 'type_id');
    }

    public function isellProduct()
    {
        return $this->belongsTo('App\Models\Product', 'type_id');
    }

}
