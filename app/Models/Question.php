<?php
namespace App\Models;

use App\Support\Traits\AddImageTrait;
use Illuminate\Database\Eloquent\Model;

class Question extends Model {

    use AddImageTrait;

    protected $fillable = [
        'title', 'answers', 'mark', 'category'
    ];

    public function image()
    {
        return $this->morphOne('App\Models\Image', 'imageable');
    }

    public function getAnswersAttribute($value)
    {
        return collect(json_decode($value));
    }

    public function getMessagesAttribute($value)
    {
        return collect(json_decode($value));
    }
}
