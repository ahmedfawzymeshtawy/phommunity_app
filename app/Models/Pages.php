<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model {

    protected $fillable = ['name', 'description'];

    protected $casts = [
        'description' => 'array',
        
    ];

    public function metaTags()
    {
        return $this->hasMany('App\Models\MetaTags', 'page_id');
    }
}
