<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IrentOrders extends Model {

    protected $fillable = [
        'user_id','phone_number','branch','time','date','dates','status','note','comment'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\FrontendUser', 'user_id');
    }
    
    public function products()
    {
        return $this->hasMany('App\Models\IrentOrdersProducts', 'order_id');
    }

    public function commentHistory()
    {
        return $this->hasMany('App\Models\CommentHistory','order_id')->latest();
    }

    public function statusHistories()
    {
        return $this->hasMany('App\Models\OrderStatusHistory', 'order_id')->latest();
    }
    
    public function getAdminName()
    {
        if ($this->admin_id != '') {
            $admin = \App\Models\AdminUser::where('id', $this->admin_id)->get();
            return $admin->first() ? $admin->first()->name : '';
        }
    }
    
    public function total()
    {
        return $this->products->sum(function($product) {
            if ($product->duration == '24' ) {
                return $product->amount * $product->price24;
            } else {
                return $product->amount * $product->price12;
            }
        });
    }

    public static function boot() {
        parent::boot();
        static::deleting(function($order) {
            $order->products()->delete();
            $order->commentHistory()->delete();
            $order->statusHistories()->delete();
        });
    }

}
