<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageSender extends Model
{
    protected $fillable = [
        'name', 'email_address', 'phone_number'
    ];


    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function sender()
    {
        return $this->hasOne('App\Models\MessageSender');
    }
}
