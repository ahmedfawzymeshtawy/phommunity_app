<?php
namespace App\Models;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;

class Video extends Model {

    protected $fillable = [
        'url', 'description', 'active'
    ];

    protected $info;

    public function getInfo($key)
    {
        if(!$this->info) $this->loadInfo();

        return $this->info[$key];
    }

    public function loadInfo()
    {
        $curl = curl_init("http://www.youtube.com/oembed?url=". $this->url ."&format=json");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $return = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($return, true);

        preg_match('#(?:https://)?(?:www\.)?(?:youtube\.com/(?:v/|watch\?v=)|youtu\.be/)([\w-]+)?#', $this->url, $match);
        $id = isset($match[1]) ? $match[1] : '';
        $embedUrl = 'http://www.youtube.com/embed/' . $id . '?autoplay=0&enablejsapi=1';

        $this->info = [
            'title' => $response['title'],
            'thumbnail_url' => $response['thumbnail_url'],
            'embed_url' => $embedUrl,
            'id' => $id,
        ];
    }
}
