<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class JoinOurTeam extends Model
{
    protected $table ='join_our_team';

    protected $fillable = [
        'name', 'email', 'job', 'note', 'cv'
    ];


    public function addDocument(UploadedFile $file)
    {
        $this->cv = time() . '_' . $file->getClientOriginalName();
        $file->move(public_path(env('UPLOAD_PATH').'documents/'), $this->cv);// for server env
    }


    public static function boot() {
        parent::boot();
        static::deleting(function($join) { 
            // delete uploaded file if exist
            if ($join->cv != null && $join->cv != '') {

                $oldPath = public_path() .env('UPLOAD_PATH').'documents/' . $join->cv;// for server env
                if ( $oldPath != '' && $oldPath != null ) {
                    if (file_exists($oldPath)) unlink($oldPath);
                }
            }
        });
    }

}
