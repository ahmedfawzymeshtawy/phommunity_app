<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\FrontendUser;
use App\Models\Notification;
use Auth ;
class ApiNotificationsController extends Controller
{

    public function index()
    {
        $currentUser = auth('api')->user();

        if ($currentUser == null) {
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }

        $notifications = Notification::orderBy('id','desc')->where('user_id', $currentUser->id)->where('type', 0 )->paginate(20);

        $updatedNotifications = Notification::where('user_id', $currentUser->id)->where('type', 0)->update(['readed' => 1 ]);

        return response()->json(['status'=>'01','data'=> ['notification' => $notifications ]],200);
    }

    public function unread()
    {
        $currentUser = auth('api')->user();

        if ( $currentUser == null) {
            return response()->json(['status'=>'02','error'=>['User not found']]);
        }
        $notifications = Notification::where('user_id', $currentUser->id)->where('type', 0 )->where('readed','0')->get();

        return response()->json(['status'=>'01','data'=>['unread' => $notifications,'unreadCount' => $notifications->count() ] ],200);
    }

    
    public function delete($id){

        $notification = Notification::find($id);

        $notification->delete();

        return response()->json(['status'=>'01','message'=>'deleted' ],200);
    }
}
