<?php

namespace App\Http\Controllers\Api;

use App\Models\FrontendUser;
use App\Models\Order;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Assistant;
use Response;
use Hash;

class ApiAssistantController extends Controller
{
    public function index(Request $request , $user_id)
    {
        $user =FrontendUser::findOrFail($user_id);

        $builder = Assistant::query()
            ->has('frontendUser')
            ->with('frontendUser.photo', 'orders')
            ->latest();

        $assistants = $builder->get();

        $featuredAssistants = Assistant::query()
            ->has('frontendUser')
            ->where('featured' , '=' ,1)
            ->with('frontendUser.photo', 'orders')
            ->latest()->get();


        $assistantOrders = Order::with('orderable')
                                ->where('orderable_type','=','App\Models\SecondShooter')
                                ->where('status','=','pending')
                                ->where('frontend_user_id','=',$user->id)->get();

        return Response()->json(['status'=>'01','data'=> ['assistants' => $assistants, 'topAssistants' =>$featuredAssistants , 'assistantOrders' => $assistantOrders] ],200);
    }

    public function topten(Request $request)
    {
        $builder = Assistant::query()
            ->has('frontendUser')
            ->with('frontendUser.photo', 'orders')
            ->latest();
        
            if ($request->has('q')) {
                $builder->whereHas('frontendUser', function ($query) use ($request) {
                    return $query->where(DB::raw('concat(first_name," ",last_name)') , 'LIKE', '%' . $request->get('q') . '%')
                        ->orWhere('email', 'LIKE', '%' . $request->get('q') . '%');
                });

            }

            $assist = $builder->get();
            $assistants = $assist->filter(function ($item) {
                return $item->featured == 1;
            });

           return response()->json(['status'=>'01','data'=>['top10' => $assistants ] ],200);
        
    }


    public function doJoin(Request $request)
    {
        $user = auth('api')->user();
        if($user == null){
            return response()->json(['status'=>'02','message'=>'User not found']);
        }

        $request->validate([
            'about' => 'required',
            'area' =>'required'
        ]) ;

        $input = $request->only('about', 'area');
        $input['frontend_user_id'] = $user->id ;
        $input['featured'] = 0;
        $input['rating'] = 0;
        $input['approved'] = 1;

        $assistant = new Assistant($input);
        if ($assistant->save()) {

           return response()->json(['status'=>'01','message'=>'success'],200);
        }else{

            return response()->json(['status'=>'02','error'=>['failed'] ],200);

        }
    }

    public function del(Request $request){
        $user=auth('api')->user();
        if(!$user){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }

       
      
        
            $request->validate(['password' => 'required']) ;

        if ( Hash::check($request->password, $user->password) ){
             $second=Assistant::where('frontend_user_id',$user->id)->delete();
                if($second){
                
                return response()->json(['status'=>'01','message' => 'Deleted successfully'],200);  
            }else{
                return response()->json(['status'=>'02','error' => ['failed']],200);
            }
        }else{
            return response()->json(['status'=>'02','error' => ['Assistant not found']],200);
        }
        
    }

    public function update(Request $request){
        $user=auth('api')->user() ;
        if(!$user){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }
        
        $request->validate(['about' =>'required','area' =>'required']);
        $input['about'] = $request->about ;
        $input['area'] = $request->area ;
        $input['frontend_user_id'] = $user->id ;

        $second=Assistant::where('frontend_user_id',$user->id);
        if($second){
            if($second->update($input)){

                    return response()->json(['status'=>'01','message' => 'success'],200);   
                
            }else{
                return response()->json(['status'=>'02','error' => ['failed']],200);
            }
        }else{
            return response()->json(['status'=>'02','error' => ['Assistant not found']],200);
        }
    }

}
