<?php

namespace App\Http\Controllers\Api;

use App\Models\FrontendUser;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Feedback;
use App\Models\AdminUser;

class ApiFeedbackController extends Controller
{

    public function sendFeedback(Request $request)
    {

        $this->validate($request, [
            'body'    => 'required|string|max:255'
        ]);
            
        $user = auth('api')->user();
        if(!$user){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }


        $feedback = new Feedback;
        $feedback->body = $request->get('body');
        $feedback->frontend_user_id = $user->id;
        
        if ( $feedback->save() ) {

            $data = [ 
                'title'     => 'Feedback',
                'link'      => url('admin/feedback'), 
                'message'   => 'new feedback from user ' . $user->first_name . ' '
            ];
    
            $userIds = AdminUser::pluck('id')->toArray();
           // send_notification_admins($userIds, $data);
            
            return response()->json(['status' => '01','message'=>'Send Successfully'],200);
        }

        return response()->json(['status' => '02','error'=>['failed, please try again']],200);
    }
}
