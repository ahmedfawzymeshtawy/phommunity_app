<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Arr ;
use App\Http\Controllers\Controller;
use App\Models\FrontendUser;
use App\Models\PasswordReset;
use Carbon\Carbon;
use Auth;


class ForgotPasswordController extends Controller
{

     public function doForgetPassword(Request $request)
    {

        $this->validate($request, [
            'email'     => 'required|email|max:255',
        ]);

        $user = FrontendUser::where('email', $request->get('email'))->first();

        if ($user) {
            $record = PasswordReset::where('email', $user->email)->firstOrCreate([
                'email' => $user->email,
                'token' => Str::random(64),
                'created_at' => Carbon::now()
            ]);

            \Mail::send('emails.forget_password', ['user' => $user, 'record' => $record], function ($m) use ($user) {
                $user_fullName = $user->first_name.' '.$user->last_name ;
                $m->to($user->email, $user_fullName);
                $m->subject('Reset Password');
            });

            
                return response()->json([
                    'status'=>'01',
                    'message'=>'email has been sent'],200);
            

        } else {
            return response()->json(['status'=>'02','error'=>['user could not be found']],200);
        }
        
    }


    public function showResetPassword($token)
    {
        $record = \DB::table('password_resets')->where('token', $token)->first();
        if (!$token || !$record) {
            throw new NotFoundHttpException();
        }

        return view('site.auth.reset_password',compact('token'));
    }


}
