<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\FrontendUser;
use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Models\PasswordReset;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Response;

class ApiSocialRegister extends Controller
{
  
     public function SocialSignup(Request $request) {
        
        // validate registeration request Data
        $this->validate($request, [
            'first_name'    => 'required|string|max:255',
            'last_name'     => 'required|string|max:255',
            'email'         => 'required|email',
            'provider'         => 'required|string|max:255',
            'provider_id'  => 'required|unique:frontend_users,provider_id',
            'provider_email'  => 'required|email',
            'photo'         => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $input = $request->only(
            'first_name', 'last_name', 'email', 'provider', 'provider_id', 'provider_email'
        );
        
        $user = FrontendUser::where('email', "=", $request->input('email'))->first() ;
        if($user){
           
              // if duplicated email
            if($user->email == $request->input('email') && $user->provider_id == $request->input('provider_id')&& $user->provider_email == $request->input('provider_email') ) {
                return response()->json(['status' => '02','error' => ["You already signed up with ". $user->email." account ".$request->input('provider').", please use another email "]],200);
            }else{
                return response()->json(['status' => '02',
                    'error' => ["email already taken"]],200);
            }
        }
            
        $user = new FrontendUser($input);
        $user->active            = 0;
        $user->gender            = '';
        $user->country            = '';
        $user->state            = '';
        $user->city            = '';
        $user->birthday            = '';

        if ($user->save()) {
            if ($request->hasFile('photo')) {
                if ($request->hasFile('photo')) {       

                    $img = $request->photo ;
                    
                    $name= rand(). '.'.$img->getClientOriginalExtension();
                    
                    $img->move(env('UPLOAD_PATH').'images',$name); 
                    
                    //file_put_contents($path, $img);

                    $newImage = new Image();
                    $newImage->image_filename = $name;
                    $newImage->original_filename = $name;
                    $newImage->thumb_filename='thumb_'.$name;
                    $newImage->imageable_id = $user->id;
                    $newImage->imageable_type = 'App\Models\FrontendUser';
                    $newImage->save();
                }
            }

            $frontendUser = Image::where('imageable_id','=',$user->id )
                    ->where('imageable_type','=','App\Models\FrontendUser' )->get()->first();
            if( $frontendUser){
                $user->avatar =  $frontendUser->imageFullUrll();
            }else{
                $user->avatar = '' ;
            }
            return response()->json(['status'  => '01','user'=>$user],200);
        }

    
    }


    
    protected function isValidInput(Request $request)
    {
        $this->validate($request, [
            'first_name'    => 'required|string|max:255',
            'last_name'     => 'required|string|max:255',
            'email'         => 'required|email|max:255',
            'password'      => 'required|confirmed|min:6|max:255',
            'mobile'        => 'required|string|min:8|max:15|unique:frontend_users,mobile',
            'gender'        => 'required|max:10',
            'Firebase_token' => 'required',
            'level'         => 'nullable|string|max:255',
            'camera_brand'  => 'nullable|numeric',
            'camera_model'  => 'nullable|numeric',
            'interested_in' => 'required|array',
            'birthday'      => 'required|string|max:200',
            'country'       => 'required|string|max:200',
            'state'         => 'required|string|max:200',
            'city'          => 'nullable|string|max:200',
            'photo'         => 'nullable|image|mimes:jpeg,png,jpg|max:2048'
        ]);
    }

    public function finishRegister($id, Request $request)
    {
        if($id == ''){
            return response()->json([
                    'status' => '02',
                    'error' =>[ 'provider id missing' ] 
                ],200);
        }
        $user = FrontendUser::where('provider_id', '=' , $id)->first();

        if($user == null){
            return response()->json([
                    'status' => '02',
                    'error' =>[ 'user not exist' ] 
                ],200);
        }

    if($user->first_sign == 0 &&  $user->active == 0){

        $this->isValidInput($request);

        $input = $request->only(
            'first_name', 'last_name', 'email', 'password', 'mobile', 'level',
            'camera_brand', 'camera_model', 'interested_in','gender','Firebase_token','country',
            'state','city','birthday'
        );

        $input['password'] = bcrypt($request->get('password'));
        $input['active'] = 1;
        $user->pusher_token = $request->Firebase_token;
        $user->level            = $request->level;
        $user->camera_brand     = $request->camera_brand;
        $user->camera_model     = $request->camera_model;
        $user->current_points   = 0;
        $user->first_sign   = 1;

        if ($request->interested_in) {
            $input['interested_in']  = json_encode($request->interested_in);
        }

        if ($user->update($input)) {

            if ($request->hasFile('photo')) {
                // delete old provider image if exist
                if( $user->photo != null && $user->photo != '' ) $user->photo->delete();
                $user->avatar = '';
                $user->save();

                    $img = $request->photo ;
                    
                    $name= rand(). '.'.$img->getClientOriginalExtension();
                    
                    $img->move(env('UPLOAD_PATH').'images',$name); 
                    
                    //file_put_contents($path, $img);

                    $newImage = new Image();
                    $newImage->image_filename = $name;
                    $newImage->original_filename = $name;
                    $newImage->thumb_filename='thumb_'.$name;
                    $newImage->imageable_id = $user->id;
                    $newImage->imageable_type = 'App\Models\FrontendUser';
                    
                 }
            }
        

            if (!Auth::attempt(['email' => $user->email, 'password' => $request->input('password')])){
                return response()->json([
                    'status' => '02',
                    'error' =>[ 'Unauthorized to login' ] 
                ],200);
            }
                

            $user = $request->user();
            $tokenResult = $user->createToken('APP_NAME');
            $token = $tokenResult->token;

            if ($request->remember_me)
                $token->expires_at = Carbon::now()->addWeeks(1);

            $token->save();

    }else{
            $user = $user;
            $tokenResult = $user->createToken('APP_NAME');
            $token = $tokenResult->token;

            if ($request->remember_me)
                $token->expires_at = Carbon::now()->addWeeks(1);

            $token->save();
    }
   
        $frontendUser = Image::where('imageable_id','=',$user->id )
            ->where('imageable_type','=','App\Models\FrontendUser' )->get()->first();
        if( $frontendUser){
            $avatar =  $frontendUser->imageFullUrll();
        }else{
            $avatar = '' ;
        }
        
         $title = "Welcome Notification" ;
            $message = "Welcome To Phommunity The First Photography Community In Egypt Now You Can Log In With Your ID is ".$user->id."" ;
            $this->notification($request->input('Firebase_token'),$message, $title);
        
       return response()->json([
            'status' => '01',
            'data' =>[
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString(),
                'avatar' => $avatar ,
                'name' => $user->first_name.' '.$user->last_name,
                'email' => $user->email,
                'mobile' => $user->mobile,
                'id' =>$user->id
            ]
        ],200);
        
    }  
    
    function notification($token,$message, $title)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token=$token;

        $notification = [
            'title' => $title,
            'messgae'=>$message,
            'sound' => true,
        ];
        
        $extraNotificationData = ["message" => $notification];

        $fcmNotification = [
            'to'        => $token, 
            'data' => $extraNotificationData
        ];

        $headers = array(
            'Authorization: key=' .env('FCM_LEGACY_KEY'),
            'Content-Type: application/json'
        );


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
           // dd($result);
        return true;
    }

}
