<?php

namespace App\Http\Controllers\Api;

use App\Models\FrontendUser;
use App\Models\Order;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Models\SecondShooter;
use App\Support\Services\AddImagesToEntity;
use Response;
use File;
use DB;
use Hash;

class ApiSecondShooterController extends Controller
{
    public function index(Request $request , $user_id )
    {
        $user =FrontendUser::findOrFail($user_id);

        $builder = SecondShooter::query()
            ->has('frontendUser')
            ->with('frontendUser.photo', 'samples', 'orders')
            ->latest();

        if ($request->has('level')) {
            $builder->whereHas('frontendUser', function ($query) use ($request) {
                return $query->where('level', 'LIKE', '%' . $request->get('level') . '%');
            });
        }

        $shooters = $builder->get();

        $featuredShooters = SecondShooter::query()
            ->has('frontendUser')
            ->where('featured' , '=' ,1)
            ->with('frontendUser.photo', 'samples', 'orders')
            ->latest()->get();

        $shooterOrders = Order::with('orderable')
                               ->where('orderable_type','=','App\Models\SecondShooter')
                               ->where('status','=','pending')
                               ->where('frontend_user_id','=',$user->id)->get();

        return Response::json(['shooters' => $shooters, 'topShooters' => $featuredShooters, 'shooterOrders' => $shooterOrders],200);
    }

    public function topten(Request $request)
    {
        //dd('dddd');
        $builder = SecondShooter::query()
            ->has('frontendUser')
            ->with('frontendUser.photo', 'samples', 'orders')
            ->latest();

        if ($request->has('q')) {
            $builder->whereHas('frontendUser', function ($query) use ($request) {
                return $query->where(DB::raw('concat(first_name," ",last_name)') , 'LIKE', '%' . $request->get('q') . '%')
                    ->orWhere('email', 'LIKE', '%' . $request->get('q') . '%');
            });

        }
        $shoot = $builder->get();

        $shooters = $shoot->filter(function ($item) {
            return $item->featured == 1;
        });


        return response()->json(['status'=>'01','data'=>['top10' => $shooters ] ],200);
    }

    public function doJoin(Request $request)
    {
        $user = auth('api')->user();
        if($user == null){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }

        $request->validate([
            'about' => 'required',
            'area' =>'required',
        ]) ;

        $input = $request->only('about', 'area');
        $input['frontend_user_id'] = $user->id;
        $input['samples'] = $request->samples;
        $input['featured'] = 0;
        $input['rating'] = 0;
        $input['approved'] = 1;

        $shooter = new SecondShooter($input);
        if ($shooter->save()) {

            if ($request->file('samples')) {

                foreach ($input['samples'] as $img) {
                    
                    $name= rand(). '.'.$img->getClientOriginalExtension();
                    
                    $img->move(env('UPLOAD_PATH').'images',$name);

                    $newImage = new Image();
                    $newImage->image_filename = $name;
                    $newImage->original_filename = $name;
                    $newImage->thumb_filename='thumb_'.$name;
                    $newImage->imageable_id = $shooter->id;
                    $newImage->imageable_type = 'App\Models\SecondShooter';
                    $newImage->save();
                }
            }

            return response()->json(['status'=>'01','message'=>'success'],200);

        } else {

            return response()->json(['status'=>'02','error'=>['failed']],200);
        }
    }

    public function destroySample($id)
    {
        $image = Image::find($id);

        if (empty($image)) {

            return 0;
        }

        $var = $image->delete();

        if ($var) {

            return 1;
        } else {
            return 0;
        }
    }

    public function storeSamples(Request $request)
    {
        $user = auth('api')->user() ;
        $shooter = SecondShooter::find($user->id);
        if (!$shooter) {

            return response()->json(['status'=>'02','error' => ['Shoot not found ']],200);
        }


        if ($request->file('samples')) {
            try {
                DB::beginTransaction();
                foreach ($request->samples as $img) {
                    
                    $name= rand(). '.'.$img->getClientOriginalExtension();
                    
                    $img->move(env('UPLOAD_PATH').'images',$name);

                    $newImage = new Image();
                    $newImage->image_filename = $name;
                    $newImage->original_filename = $name;
                    $newImage->thumb_filename='thumb_'.$name;
                    $newImage->imageable_id = $shooter->id;
                    $newImage->imageable_type = 'App\Models\SecondShooter';
                    $newImage->save(); 
                    
                }
                DB::commit();
                return response()->json(['status'=>'01','message' => 'Samples has been added successfully'],200);
            } catch (\Exception $e) {
                DB::rollBack();
                 return response()->json(['status'=>'02','error' => ['failed']],200);
            }
        }

         

    }

     public function del(Request $request){
        $user=auth('api')->user();
        if(!$user){
            return response()->json(['status'=>'02','error'=>['User not found']]);
        }

        if ( Hash::check($request->password, $user->password) ){
             $second=SecondShooter::where('frontend_user_id',$user->id)->delete();
                if($second){
                
                return response()->json(['status'=>'01','message' => 'Deleted successfully'],200);  
            }else{
                return response()->json(['status'=>'02','error' => ['failed']],200);
            }
        }else{
            return response()->json(['status'=>'02','error' => ['Assistant not found']],200);
        }
        
    }

    public function update(Request $request){
        $user=auth('api')->user() ;
        if(!$user){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }
       
        $request->only('about','area');
        $input['about'] = $request->about ;
        $input['area'] = $request->area ;
        $input['frontend_user_id'] = $user->id ;
        $second=SecondShooter::where('frontend_user_id',$user->id);
        $update_secound = $second->update($input);
        if($update_secound){

            if ($request->samples) {
               foreach ($request->samples as $img) {

                    $name= rand(). '.'.$img->getClientOriginalExtension();
                    
                    $img->move(env('UPLOAD_PATH').'images',$name);

                    $newImage = new Image();
                    $newImage->image_filename = $name;
                    $newImage->original_filename = $name;
                    $newImage->thumb_filename='thumb_'.$name;
                    $newImage->imageable_id = $update_secound;
                    $newImage->imageable_type = 'App\Models\SecondShooter';
                    $newImage->save();
                }  
            }
            return response()->json(['status'=>'01','message' => 'success'],200); 
        }else{
            return response()->json(['status'=>'02','error' => ['failed']],200);
        }
    }


}
