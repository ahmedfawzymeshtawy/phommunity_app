<?php

namespace App\Http\Controllers\Api;

use App\Models\Image;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Lab;
use Response;

class ApiLabController extends Controller
{
    public function index()
    {
        $options = Lab::with('images')->where('available', 1)->latest()->get();
        if($options == null || $options->count() == 0){
            return Response::json(['status'=>'02','error' => ['No Labs available'] ] ,200);
        }
        $slider = Image::where('imageable_type','=','App\Models\Lab')->get();

        return Response::json(['status'=>'01','data'=> ['labs' => ['options' => $options , 'slider' => $slider] ] ],200);
    }

    public function productbranches(){
        $product = ['Maadi','Nasr City'];
        return Response::json(['status'=>'01','branches' =>$product ],200);
    }


}
