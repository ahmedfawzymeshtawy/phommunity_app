<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Arr ;
use App\Http\Controllers\Controller;
use App\Models\FrontendUser;
use App\Models\PasswordReset;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ApiAuthController extends Controller
{

 

    public function logout() {
        Auth::logout();
        return response()->json([
            'status'  => '01',
            'message' => 'logout'
        ], 200);
    }

    public function doForgetPassword(Request $request)
    {

        $this->validate($request, [
            'email'     => 'required|email|max:255',
        ]);

        $user = FrontendUser::where('email', $request->get('email'))->first();

        if ($user) {
            $record = PasswordReset::where('email', $user->email)->firstOrCreate([
                'email' => $user->email,
                'token' => Str::random(64),
                'created_at' => Carbon::now()
            ]);

            $mail = \Mail::send('emails.forget_password', ['user' => $user, 'record' => $record], function ($m) use ($user) {
                $user_fullName = $user->first_name.' '.$user->last_name ;
                $m->to($user->email, $user_fullName);
                $m->subject('Reset Password');
            });

            if ($mail == 1) {
                return response()->json([
                     'status' => '01',
                     'message' =>'email has been sent',
                     200
                 ]);
            } else {
                return response()->json([
                     'status' => '02',
                     'error'=>['email not sent'],200
                 ]);
            }

        } else {
            return response()->json([
                'status' => '02',
                'error'=>['user could not be found'],200]);
        }
        
    }

    public function doResetPassword(Request $request)
    {
        $this->redirectHash = '#reset-password';

        $token = $request->get('token');
        $record = \DB::table('password_resets')->where('token', $token)->first();

        if (!$token || !$record) {
            throw new NotFoundHttpException();
        }

        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
        ]);

        $user = FrontendUser::where('email', $record->email)->firstOrFail();

        $user->fill(['password' => bcrypt($request->get('password'))])->save();

        toast("Your password has been reset successfully.");
        return redirect()->route('site.home');
    }


    public function doActivateAccount($token)
    {
        $account = FrontendUser::where('verification_token', $token)->firstOrFail();

        $account->fill(['active' => 1])->save();

        $notfidata = ['link'=> url('/'), 'message' => 'Welcome To Phommunity The First Photography Community In Egypt Now You Can Log In With Your ID ', 'title'=> 'Welcome'];

        send_notification_users($account->id, $notfidata);

        toast("Your account has been activated successfully.");

        return view('site.home');
    }

    
}
