<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\FrontendUser;
use App\Models\PasswordReset;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth; 
use App\Models\Image;
use App\Notification\WelcomeNotification;
use Validator;
use NotificationChannels\PusherPushNotifications\PusherChannel;
use NotificationChannels\PusherPushNotifications\PusherMessage;
use Illuminate\Notifications\Notification;

class ApiLoginController extends Controller
{
    public function index(){
        return view('welcome');
    }

    //Login Function
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
            'Firebase_token' => 'required'
        ]);

         $user = FrontendUser::where('id', "=", $request->input('email'))
            ->orWhere('email', "=", $request->input('email'))
            ->orWhere('mobile', "=", $request->input('email'))->first();
            
        // Check if user account active
        if(!$user){
            return response()->json(['status' => '02',
                'error' => ['Username not correct']],200);
        }
        if ($user && $user->active == 0 ) return response()->json(['status' =>'02','error' =>['account not active'] ],200);

       // $credentials = $request->only('password');
        //dd($credentials);
        if (!Auth::attempt(['email' => $user->email, 'password' => $request->input('password')], $request->input('remember_me')))
            return response()->json([
                 'status' => '02',
                'error' =>[ 'Unauthorized to login' ] 
            ],200);

        $user = $request->user();
        $tokenResult = $user->createToken('APP_NAME');
        $token = $tokenResult->token;

        $token->save();

        $frontendUser = Image::where('imageable_id','=',Auth::user()->id )
            ->where('imageable_type','=','App\Models\FrontendUser' )->get()->first();
        if( $frontendUser){
            $avatar =  $frontendUser->imageFullUrll();
        }else{
            $avatar = '' ;
        }

        $firstUserToken = FrontendUser::find(Auth::user()->id);
        $firstUserToken->pusher_token = $request->input('Firebase_token');
        $firstUserToken->save();
        
        
        if(Auth::user()->first_sign == 0 ){
            $title = "Welcome Notification" ;
            $message = "Welcome To Phommunity The First Photography Community In Egypt Now You Can Log In With Your ID is ".Auth::user()->id."" ;

            $this->notification($request->input('Firebase_token'),$message, $title);
            
            $firstUser = FrontendUser::find(Auth::user()->id);
            $firstUser->first_sign = 1;
            $firstUser->save();

        }
        
        return response()->json([
            'status' => '01',
            'data' =>[
                'access_token' => $tokenResult->accessToken,
                //'pusher_token' => $beamsToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString(),
                'avatar' => $avatar ,
                'name' => Auth::user()->first_name.' '.Auth::user()->last_name,
                'email' => Auth::user()->email,
                'mobile' => Auth::user()->mobile,
                'id' =>Auth::user()->id
            ]
        ],200);
    }

    function notification($token,$message, $title)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token=$token;

        $notification = [
            'title' => $title,
            'messgae'=>$message,
            'sound' => true,
        ];
        
        $extraNotificationData = ["message" => $notification];

        $fcmNotification = [
            'to'        => $token, 
            'data' => $extraNotificationData
        ];

        $headers = array(
            'Authorization: key=' .env('FCM_LEGACY_KEY'),
            'Content-Type: application/json'
        );


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
           // dd($result);
        return true;
    }

    
}