<?php

namespace App\Http\Controllers\Api;

use App\Models\Comment;
use App\Models\FrontendUser;
use App\Models\Group;
use App\Models\Group_users;
use App\Models\Image;
use App\Models\Like;
use App\Models\Notification;
use App\Models\Post;
use App\Models\Question;
use App\Models\Tip;
use App\Models\Video;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use App\Models\Files;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Event;
use Carbon\Carbon;
use App\Support\Services\AddImagesToEntity;
use App\Support\Services\MiscContainer;
use App\Models\Wishlist;
//use App\Models\ScoreSuggest;
use App\Models\Evaluation;

use Response;

class ApiIlearnController extends Controller
{
    protected $user_id;
    protected $group_id;

    public function courses()
    {
        $coursess = Course::with('images', 'instructors','organizers')->get();
        
        if( $coursess == null || $coursess->count() == 0 ){
            return response()->json(['status' => '02','error'=>['no courses found']],200);
        }
        $courseArr = array() ;
        $count = 0 ;
        foreach ($coursess as $course) {
            
            $courseArr['id'] = $course->id ;
            $courseArr['name'] = $course->name ;
            $courseArr['descriptions'] = $course->descriptions ;
            $courseArr['cover_photo'] = $course->cover_photo ;
            $courseArr['lecture_hours'] = $course->lecture_hours ;
            $courseArr['lectures_count'] = $course->lectures_count ;
            $courseArr['date_from'] = $course->date_from ;
            $courseArr['date_to'] = $course->date_to ;
            $courseArr['lectures'] = json_decode($course->lectures) ;
            $courseArr['faq'] = $course->faq ;
            $courseArr['video_url'] = $course->video_url ;
            $courseArr['phone_numbers'] = $course->phone_numbers ;
            $courseArr['position'] = $course->position ;
            $courseArr['price'] = $course->price ;
            $courseArr['next_rounds'] = $course->next_rounds ;
            $courseArr['requirements'] = $course->requirements ;
            $courseArr['related_events'] = $course->related_events ;
            $courseArr['related_courses'] = $course->related_courses ;
            $courseArr['created_at'] = $course->created_at ;
            $courseArr['updated_at'] = $course->updated_at ;
            $courseArr['courses_related'] = $course->courses_related ;
            $courseArr['events_related'] = $course->events_related ;
            $courseArr['images'] = $course->images ;
            $courseArr['instructors'] = $course->instructors ;
            $courseArr['organizers'] = $course->organizers ;
            $courses[] = $courseArr ;
        }
        $p =  json_encode($courses);
        $p2 = json_decode($p, TRUE);
        return Response::json(['status' => '01','data'=> ["products"=> array_values($p2) ]],200);
    }

    public function course($id)
    {
        $coursess = Course::where('id',$id)->with('images','instructorImage')->get();

        if ( $coursess == null || $coursess->count() == 0 )  {

            return response()->json(['status' => '02','error'=>['this course not found']],200);
        }

         foreach ($coursess as $course) {
            $courseArr['id'] = $course->id ;
            $courseArr['name'] = $course->name ;
            $courseArr['descriptions'] = $course->descriptions ;
            $courseArr['cover_photo'] = $course->cover_photo ;
            $courseArr['lecture_hours'] = $course->lecture_hours ;
            $courseArr['lectures_count'] = $course->lectures_count ;
            $courseArr['date_from'] = $course->date_from ;
            $courseArr['date_to'] = $course->date_to ;
            $courseArr['lectures'] = json_decode($course->lectures) ;
            $courseArr['faq'] = $course->faq ;
            $courseArr['video_url'] = $course->video_url ;
            $courseArr['phone_numbers'] = $course->phone_numbers ;
            $courseArr['position'] = $course->position ;
            $courseArr['price'] = $course->price ;
            $courseArr['next_rounds'] = $course->next_rounds ;
            $courseArr['requirements'] = $course->requirements ;
            $courseArr['related_events'] = $course->related_events ;
            $courseArr['related_courses'] = $course->related_courses ;
            $courseArr['created_at'] = $course->created_at ;
            $courseArr['updated_at'] = $course->updated_at ;
            $courseArr['courses_related'] = $course->courses_related ;
            $courseArr['events_related'] = $course->events_related ;
            $courseArr['images'] = $course->images ;
            $courseArr['instructors'] = $course->instructors ;
            $courseArr['organizers'] = $course->organizers ;
            $courses[] = $courseArr ;
        }
        $p =  json_encode($courses);
        $p2 = json_decode($p, TRUE);
        $user = auth('api')->user();
         if($user){
            $wishlist = Wishlist::where('frontend_user_id', $user->id)
                            ->where('type', 'course')
                            ->where('type_id', $id)
                            ->get();
            if ($wishlist->count() > 0) {
                $isFav = 1;
            }else{
                $isFav = 0;
            }
        }else{
            $isFav = 0;
        }
       
        return Response::json(['status' => '01','data'=>['course' => array_values($p2) ,'isFavourite'=>$isFav ] ],200);
    }

    public function events()
    {
        $events = Event::with('images', 'instructors','organizers')->latest()->get();

        if( $events == null || $events->count() == 0  ){
            return response()->json(['status' => '02','error'=>['no events found']],200);
        }
        $p =  json_encode($events);
        $p2 = json_decode($p, TRUE);

        return Response::json(['status' => '01','data'=> [ 'events' => array_values($p2)] ],200);

    }

    public function event($id)
    {
        $event = Event::where('id',$id)->with('images','instructors','organizers')->get()->first();

        if ( $event == null || $event->count() == 0  ) {

            return response()->json(['status' => '02','error'=>['this event not found']],200);
        }
        $p =  json_encode($event);
        $p2 = json_decode($p, TRUE);
        $user = auth('api')->user();
        if($user){
            $wishlist = Wishlist::where('frontend_user_id', $user->id)
                            ->where('type', 'event')
                            ->where('type_id', $id)
                            ->get();
            if ($wishlist->count() > 0) {
                $isFav = 1;
            }else{
                $isFav = 0;
            }
        }else{
            $isFav = 0;
        }

        return Response::json(['status' => '01','data'=>$p2 ,'isFavourite'=>$isFav],200);
    }

    public function videos()
    {
        $videos = Video::where('active', 1)->latest()->paginate(5);

        if( $videos == null || $videos->count() == 0 ){
            return response()->json(['status' => '02','error'=>['no videos found']],200);
        }

        $videosData = array() ;
        foreach ($videos as $video) {
            $videosData['id'] = $video->id ; 
            $videosData['url'] = $video->url ; 
            $videosData['description'] = $video->description ; 
            $videosData['name'] = $video->getInfo('title') ; 
            $videosData['thumbnail_url'] = $video->getInfo('thumbnail_url') ; 
            $videosData['created_at'] = $video->updated_at ; 
            $videosData['updated_at'] = $video->updated_at ;
            $testVideo[] = $videosData;
        }
        
        //dd($videosData['id']);
        return Response::json(['status' => '01','data'=>[ 'videos' => $testVideo ]],200);
    }

    public function groups()
    {

        $user = auth('api')->user();

        if (!$user) {

            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }
        $this->user_id = $user->id;
        $groups = Group::whereHas('users', function ($query) {
            return $query->where('frontend_user_id',$this->user_id);
        })->with('users')->with('users.photo')->get()->load([
            'posts' => function ($query) {
                return $query->latest();
            },
            'posts.comments' => function ($query) {
                return $query->latest();
            },
            'posts.comments.frontendUser',
            'posts.comments.frontendUser.photo',
            'posts.likes',
            'posts.frontendUser.photo',
            'posts.images'
        ]);

        if(!empty($groups)){
            return Response::json(['status' => '01','data'=>['groups' => $groups]],200);
        }else{
             return Response::json(['status' => '02','error'=>['You are not joined any group yet!']],200);
        }
        
    }


    public function group($group_id)
    {
        $this->group_id = $group_id;

        $group = Group::find($group_id);

        if ($group == null || $group->count() ==0   ) {

            return response()->json(['status' => '02','error'=>['this group not found']],200);
        }

        $groups = Group::whereHas('users', function ($query) {

            return $query->where('group_id',$this->group_id);
        })->with('users')->with('users.photo')->get()->load([
            'posts' => function ($query) {
                return $query->latest();
            },
            'posts.comments' => function ($query) {
                return $query->latest();
            },
            'posts.comments.frontendUser',
            'posts.comments.frontendUser.photo',
            'posts.likes',
            'posts.frontendUser.photo',
            'posts.images'
        ]);
        return Response::json(['status' => '01','data'=>['group' => $groups ]],200);
    }

     public function LeaveGroup($group_id){
        $user = auth('api')->user() ;
        if (!$user) {

            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }
        $group=Group_users::where(['group_id'=>$group_id,'frontend_user_id' => $user->id ]) ;
        if($group){
            $group->delete();
            return response()->json(['status' => '01','message'=>'You left the group '],200);
        }else{
            return response()->json(['status' => '02','error'=>['You are not a group member']],200);
        }
        
    }

    public function storePost($groupId, Request $request)
    {
        $request->validate(['message'=>'required']);

        $user = auth('api')->user();

        if(!$user){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }

        $user = FrontendUser::where('id','=',$user->id)->get();
       
        if ($user ) {
            $group = Group::find($groupId);
            
            if($group == null ){
                return response()->json(['status' => '02','error'=>['Group not found']],200);
            } 

            $newPost = new Post([
                'group_id' => $group->id,
                'frontend_user_id' =>auth('api')->user()->id,
                'body' => $request->get('message')
            ]);

            if ($newPost->save()) {

                if ($request->file('images')) {
                    foreach ($request->file('images') as $img){
                    
                    $name= rand(). '.'.$img->getClientOriginalExtension();
                    
                    $img->move(env('UPLOAD_PATH').'images',$name); 

                        $newImage = new Image();
                        $newImage->image_filename = $name;
                        $newImage->original_filename = $name;
                        $newImage->thumb_filename='thumb_'.$name;
                        $newImage->imageable_id = $newPost->id;
                        $newImage->imageable_type = 'App\Models\Post';
                        $newImage->save();
                    }
                }

                if($request->file('files')){
                    foreach ($request->file('files') as $file){

                        $des = env('UPLOAD_PATH').'groups/files/';

                        $file_name_Original = $file->getClientOriginalName();
                        $new_file_name= rand() . time() . '.' . $file->getClientOriginalExtension();
                        $file->move($des, $new_file_name);
                        $file_path = '/uploads/groups/files/' . $new_file_name;
                        $add_file=new Files();
                        $add_file->post_id=$newPost->id;
                        $add_file->file_name=$file_name_Original;
                        $add_file->file_path=$file_path;
                        $add_file->save();
                    }
                }
        
                $post = Post::where('id','=',$newPost->id)->with('frontendUser','frontendUser.photo','images' ,'files')->get();

                return response()->json(['status' => '01','data'=> compact('post') ],200);

            } else {
                return response()->json(['status' => '02','error'=>['failed']],200);
            }
        }else{
            return response()->json(['status' => '02','error'=>['user not found']],200);

        }

    }


    public function storeComment($postId, Request $request )
    {

        $user = auth('api')->user();

        if(!$user){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }

        $authUser = FrontendUser::findOrFail($user->id);

        $user = FrontendUser::where('id','=',$user->id)->get();

            $request->validate(['comment' => 'required']) ;
            $post = Post::findOrFail($postId);

            $newComment = new Comment([
                'post_id' => $post->id,
                'frontend_user_id' => auth('api')->user()->id,
                'body' => $request->get('comment')
            ]);


            if ($newComment->save()) {
                
                if ($authUser->id = $post->frontend_user_id) {
                    
                    $this->notifyWriter($post, $authUser);
                }

                $comment = Comment::where('id','=',$newComment->id)->with('frontendUser','frontendUser.photo')->get();

                return response()->json( ['status' => '01','data'=>compact('comment')],200);

            } else {

                return response()->json(['status' => '02','error'=>['failed']],200);
            }

     }

    public function notifyWriter(Post $post, FrontendUser $user, $action = 'comment')
    {
        if ($action == 'comment') {
            $notificationTitle = 'New Comment';
            $notificationMessage = $user->full_name . ' has commented on your post.';
        } else {
            $notificationTitle = 'New Like';
            $notificationMessage = $user->full_name . ' has liked your post.';
        }

        $notification = new Notification([
            'title' => $notificationTitle,
            'message' => $notificationMessage,
            'type' => '0',
            'user_id' => $post->frontend_user_id
        ]);
        $notification->save();

        $notification->users()->attach([$post->frontend_user_id], ['new' => 1]);

        // $pusher = app()->make('Pusher');
        // $pusher->trigger("user-{$post->frontend_user_id}", 'notification', [
        //     'title' => $notification->title,
        //     'message' => $notification->message
        // ]);
    }


    public function processLike($postId , $userId)
    {
        $authUser = auth('api')->user() ;

        if(!$authUser){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }

        $post = Post::findOrFail($postId);

        $params = [
            'post_id' => $post->id,
            'frontend_user_id' => $authUser->id,
        ];

        $like = Like::where($params)->first();

        if ($like) {
            $like->delete();
            return response()->json(['status'=>'01' ,'count'=> $post->likes->count() , 'action' => 'deslike'],200);

        } else {

            if ($authUser->id != $post->frontend_user_id) {
                $this->notifyWriter($post, $authUser, 'like');
            }
            Like::Create($params);
            return response()->json(['status'=>'01','count'=> $post->likes->count() , 'action' => 'like'],200);

        }

    }


    public function tips()
    {
        $tips = Tip::with('images')->latest()->get();

        if( $tips == null || $tips->count() == 0 ){
            return response()->json(['status' => '02','error'=>['no tips found']],200);
        }

        return Response::json(['status' => '01','data'=>['tips' => $tips ] ],200);
    }

    public function tip($id)
    {
        $tip = Tip::where('id',$id)->with('images')->get()->first();

        if ( $tip == null || $tip->count() == 0 ) {

            return response()->json(['status' => '02','error'=>['this tip not found'] ],200);
        }

        return Response::json(['status' => '01','data'=>['tip' => $tip ] ],200);
    }

    public function evaluation($category)
    {
        $questions = Question::with('image')->where('category', $category)->limit(10)->inRandomOrder()->get();

        return Response::json([
            
                'status' => '01',
                'data'=> [ 'evaluation' =>[
                    'category' => $category,
                    'questions' => $questions 
                ] ]
        ],200);

    }

    public function reviewEvaluation($category, Request $request, MiscContainer $misc)
    {
        $submittedData = collect($request->get('data'))->keyBy('questionId')->toArray();
         
        $selectedQues = $request->get('data');

        
        $quesIds = [];
        if ( count($selectedQues) > 0 ) {
            for ($i=0; $i < count($selectedQues) ; $i++) { 
                array_push( $quesIds, $selectedQues[$i]['questionId'] );
            }
        }
         $questions = Question::findOrfail($quesIds);

        $totalMark = $userMark = 0;
        foreach ($questions as $id => $question) {
            $totalMark += $question->mark;

            $rightAnswer = '';
            foreach ($question->answers as $answer) {
                if ($answer->right) {
                    $rightAnswer = $answer->body;
                    break;
                }
            }

            if (array_key_exists($question->id, $submittedData)) {
                $item = $submittedData[$question->id];

                if ($item['answer'] == $rightAnswer) {
                    $userMark += $question->mark;
                }
            }
        }

        $feedback = '';
        foreach ($misc->evaluation_feedback as $item) {
            if ($userMark >= $item['from'] && $userMark <= $item['to']) {
                $feedback = $item['message'];
            }
        }

        $mark = $userMark . '/' . $totalMark;

        // $scoreSuggest = ScoreSuggest::where('category', $category)->where('score', $userMark)->get();
        // $scoreSuggestCourse = !$scoreSuggest->isEmpty() ? $scoreSuggest->first()->course() : null;
        // $scoreSuggestName = $scoreSuggestCourse != null ? $scoreSuggestCourse->name : null;

        $user = auth('api')->user();

        $evaluation = new Evaluation;

        $evaluation->user_id    = $user->id;
        $evaluation->user_name  = $user->full_name;
        $evaluation->category   = $category;
        $evaluation->mark       = $mark;
        $evaluation->feedback   = $feedback;

        $evaluation->save();

        return response()->json([
            'status' => '01',
            'data'=>[
                //'suggestName' => $scoreSuggestName,
                'mark' => $mark,
                'feedback' => $feedback
            ]
        ],200);
    }

}
