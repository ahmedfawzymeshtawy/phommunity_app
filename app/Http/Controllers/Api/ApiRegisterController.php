<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ApiRegisterRequest;
use App\Models\Brand;
use App\Models\Interested;
use App\Models\Cambrand;
use App\Models\CamModel;
use App\Models\FrontendUser;
use App\Support\Services\AddImagesToEntity;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Arr ;
use App\Http\Requests;
use Faker\Provider\Uuid;
use App\Models\Image;
use App\Http\Controllers\Controller;
use Response;




class ApiRegisterController extends Controller
{

    public function register(Request $request)
    {
        // validate registeration request
        $this->validate($request, [
            'first_name'    => 'required|string|max:255',
            'last_name'     => 'required|string|max:255',
            'email'         => 'required|email|max:255|unique:frontend_users,email',
            'password'      => 'required|confirmed|min:6|max:255',
            'mobile'        => 'required|string|min:8|max:15|unique:frontend_users,mobile',
            'level'         => 'nullable|string|max:255',
            'camera_brand'  => 'nullable|numeric',
            'camera_model'  => 'nullable|numeric',
            'interested_in' => 'required|array',
            'photo'         => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
            'gender'        => 'required|max:10',
             'birthday' => 'required|string|max:200',
            'country' => 'required|string|max:200',
            'state' => 'required|string|max:200',
            'city' => 'nullable|string|max:200'
        ]);

        $input = $request->only(
            'first_name', 'last_name', 'email', 'password', 'mobile', 'level',
            'camera_brand', 'camera_model', 'interested_in','gender','birthday',
            'country','state','city'
        );

        $input['password'] = bcrypt($request->get('password'));
        $input['active'] = 0;
        $input['provider'] = 'email';
        $input['verification_token'] = Str::random(64);

        if ( isset($request->interested_in) ) {
            $input['interested_in']  = json_encode($request->interested_in);
        }
        //dd($input);
        $user = new FrontendUser($input);
        $user->level            = $request->level;
        $user->camera_brand     = $request->camera_brand;
        $user->camera_model     = $request->camera_model;
        $user->current_points   = 0;

        if ($user->save()) {
            if ($request->hasFile('photo')) {
                if ($request->hasFile('photo')) {

                    $img = $request->photo ;
                    
                    $name= rand(). '.'.$img->getClientOriginalExtension();
                    
                    $img->move(env('UPLOAD_PATH').'images',$name); 
                    
                    //file_put_contents($path, $img);

                    $newImage = new Image();
                    $newImage->image_filename = $name;
                    $newImage->original_filename = $name;
                    $newImage->thumb_filename='thumb_'.$name;
                    $newImage->imageable_id = $user->id;
                    $newImage->imageable_type = 'App\Models\FrontendUser';
                    $newImage->save();
            }
            }
            $this->sendVerificationEmail($user);
            return response()->json(['status'  => '01','message'=>'success'],200);
        }

        return response()->json([
            'status'  => '02',
            'error'=>['failed, please try again']
        ],200);
    }


    protected function sendVerificationEmail($user)
    {
        \Mail::send('emails.account_verification_new', ['user' => $user], function ($m) use ($user) {
            $user_fullName = $user->first_name.' '.$user->last_name ;
            $m->to($user->email, $user_fullName);
            $m->subject('Account Verification');
        });
    }


    public function Interests(){
        $brands = Interested::all();
        return response()->json([
            'status'=>'01',
            'data' => ['interests' => $brands] ],200);
    }
    
    public function Brands(){
        $brands = Cambrand::all();
        return response()->json([
            'status'=>'01',
            'data' => ['brands' => $brands] ],200);
    }

    public function BrandsModels($id){
        $brandWithModels = Cambrand::with('models')->find($id);
        return response()->json([
            'status'=>'01',
            'data' => ['models' => $brandWithModels ]
        ],200);
    }

}
