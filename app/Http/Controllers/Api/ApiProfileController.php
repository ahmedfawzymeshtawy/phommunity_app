<?php

namespace App\Http\Controllers\Api;

use App\Models\Image;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use App\Models\Interested ;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\FrontendUser;
use Response;
use App\Models\Order;
use App\Models\OrderFeedback;
use App\Models\Cambrand;
use App\Models\CamModel;
use App\Models\Wishlist;
use App\Models\Course;
use App\Models\Event;
use App\Models\Product;
use App\Models\Notification;
use App\Models\AdminUser;
use App\Support\Services\AddImagesToEntity;
use Hash;



class ApiProfileController extends Controller
{
    public function profile()
    {
        $user = auth('api')->user();

        if ( $user ){
            $frontendUser = FrontendUser::with('photo')->findOrfail($user->id);

            $ProfileArr['id']  = $frontendUser->id ; 
            $ProfileArr['first_name']  = $frontendUser->first_name ; 
            $ProfileArr['last_name']  = $frontendUser->last_name ; 
            $ProfileArr['email']  = $frontendUser->email ; 
            $ProfileArr['mobile']  = $frontendUser->mobile ; 
            $ProfileArr['level']  = $frontendUser->level ; 
            $ProfileArr['current_points']  = $frontendUser->current_points ; 
            $ProfileArr['active']  = $frontendUser->active ; 
            $ProfileArr['first_sign']  = $frontendUser->first_sign ; 
            $ProfileArr['provider_id']  = $frontendUser->provider_id ; 
            $ProfileArr['provider_email']  = $frontendUser->provider_email ; 
            $ProfileArr['gender']  = $frontendUser->gender ; 
            $ProfileArr['birthday']  = $frontendUser->birthday ; 
            $ProfileArr['country']  = $frontendUser->country ; 
            $ProfileArr['state']  = $frontendUser->state ; 
            $ProfileArr['city']  = $frontendUser->city ; 
            $ProfileArr['created_at']  = $frontendUser->created_at ; 
            
            $ProfileArr['chatNewMessagesCount']  = $frontendUser->chatNewMessagesCount ; 
            $ProfileArr['photo']  = empty($frontendUser->photo)? NULL:env('UPLOAD_URL') . "images/" .$frontendUser->photo->thumb_filename ; 
            $ProfileArr['points']  = $frontendUser->getCurrentPointsAttribute() ; 
            $ProfileArr['new_chat_messages_count_relation']  = $frontendUser->new_chat_messages_count_relation ; 


            if($frontendUser->interested_in != null)
            {
                $groups = json_decode($frontendUser->interested_in) ;
                for ($i = 0; $i < count($groups); $i++){
                    $interestsIdsArr[] = Interested::where('id','=',$groups[$i])->get()->first();
                } 
                    
                foreach ($interestsIdsArr as  $value) {
                    $interest[] = ["id" => $value->id,"name" => $value->name ];

                }

                $ProfileArr['interested_in']  = $interest ;   
        
            }else{
                $ProfileArr['interested_in']  = [] ; 
            }

            if($frontendUser->camera_brand){
                $ProfileArr['camera_brand'] = Cambrand::where('id','=',$frontendUser->camera_brand)->get();
                $ProfileArr['camera_model'] = CamModel::where('id','=',$frontendUser->camera_model)->get();
            }

            return Response::json(['status'=>'01','data'=>['frontendUser' => $ProfileArr ] ],200);
        } else {
             return response()->json(['status'=>'02','error'=>['User not found']],200);
        }
    }

    function removeslashes($string)
    {
        $string=implode("",explode("\"",$string));
        return stripslashes(trim($string));
    }


    public function update(Request $request)
    {
        // validate registeration request
        $this->validate($request, [
            'first_name'    => 'required|string|max:255',
            'last_name'     => 'required|string|max:255',
            'email'         => 'required|email|max:255',
            'mobile'        => 'required|string|min:8|max:15',
            'gender'        => 'required|max:10',
            'level'         => 'nullable|string|max:255',
            'camera_brand'  => 'nullable|numeric',
            'camera_model'  => 'nullable|numeric',
            'interested_in' => 'required|array',
            'birthday'      => 'required|string|max:200',
            'country'       => 'required|string|max:200',
            'state'         => 'required|string|max:200',
            'city'          => 'nullable|string|max:200',
            'photo'         => 'nullable|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $input = $request->only(
            'first_name', 'last_name', 'email', 'mobile', 'level',
            'camera_brand', 'camera_model', 'interested_in','gender','birthday','country','state','city'
        );

        if ( isset($request->interested_in) ) {
            $input['interested_in']  = json_encode($request->interested_in);
        }

        $user = auth('api')->user();

        if ($user) {   

            if ( $user->update($input) ) {

                $user->level            = $request->level;
                $user->camera_brand     = $request->camera_brand;
                $user->camera_model     = $request->camera_model;
                $user->current_points   = 0;
                if ($user->save()) {
                   
                    if ($request->hasFile('photo')) {
                            if(!empty($user->photo)){
                                $user->photo->delete();
                                // if user upload image we remove avatar image
                                $user->avatar = '';
                                $user->save();
                            }
                            
                        $img = $request->photo ;
                        $name= rand(). '.'.$img->getClientOriginalExtension();
                    
                        $img->move(env('UPLOAD_PATH').'images',$name);
        
                        
                      //  file_put_contents($path, $image);

                        $newImage = new Image();
                        $newImage->image_filename = $name;
                        $newImage->original_filename = $name;
                        $newImage->thumb_filename='thumb_'.$name;
                        $newImage->imageable_id = $user->id;
                        $newImage->imageable_type = 'App\Models\FrontendUser';
                        $newImage->save();
                    }
                    
                    return response()->json(['status'=>'01','message'=>'Your profile has been Updated successfully.'],200);
                }
            }
        }

         return response()->json(['status'=>'02','error'=>['failed, please try again']],200);
    }

    public function myOrders(){

        $user = auth('api')->user();

        if(!$user){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }
        $courses  = collect();
        $events   = collect();
        $rent    = collect();
        $rooms    = collect();
        $studios  = collect();
        $isells   = collect();
        
        foreach ($user->orders as $order) {
            if ( $order->type == 'Courses' ){
                $order->course = Course::where('id',$order->orderable_id)->get();
                //$courses->push($order);

                $p =  json_encode($order->meta);
                $p2 = json_decode($p, TRUE);
                $order->meta = $p2;
                $courses->push($order);
            }
            if ( $order->type == 'Events'  ){
                $events->event = Event::where('id',$order->orderable_id)->get()->first();
                $events->push($order);
            }
            if ( $order->type == 'I-Rent'   ){
                $p =  json_encode($order->meta);
                $p2 = json_decode($p, TRUE);
                $p3 = json_decode($p2['products']);
                $p2['products']=$p3;
                $order->meta = $p2;
                $rent->push($order);
            } 
            if ( $order->type == 'Rooms'  ){
                $p =  json_encode($order->meta);
                $p2 = json_decode($p, TRUE);
                $order->meta = $p2;
                $rooms->push($order);
            }
            if ( $order->type == 'Studios' ){
                $p =  json_encode($order->meta);
                $p2 = json_decode($p, TRUE);
                $order->meta = $p2;
                $studios->push($order);
            }
            if ( $order->type == 'I-Sell'   ){
                $p =  json_encode($order->meta);
                $p2 = json_decode($p, TRUE);
                $order->meta = $p2;
                $isells->push($order);
            }
        }

        return response()->json(['status'=>'01', 'data'=>[compact('courses','events','rent','rooms','studios','isells')] ]);
    }

    // public function viewOrder($id){
        
    //     $order = Order::findOrFail($id);
    //      $p =  json_encode($order);
    //     $p2 = json_decode($p, TRUE);
    
    //     return response()->json(['status'=>'01', 'order'=>$p2]);

    // }

    public function cancelOrder(Request $request){

        $this->validate($request, [
            'order_id'      => 'required|numeric',
            'feedback'      => 'nullable|string|max:255',
        ]);
        
        $order = Order::findOrFail($request->order_id);
        $order->status = 'canceled';

        
        if($order->save()) {
            // if user enter a feedback            
            if( $request->filled('feedback') ) {
    
                $feedback = new OrderFeedback;
                $feedback->order_id = $request->order_id;
                $feedback->feedback = $request->feedback;
                $feedback->save();
            }
            
            // $notification = new Notification([
            //     'message' => 'Order No ' . $order->id.' Cancelled by user',
            //     'title' => 'Order Cancellation',
            //     'type' => '0',
            //     'user_id' => auth('api')->user()->id,
            //     'link' => url(''),
            // ]);
            //$notification->save();

            $chatMessage = 'Order No ' . $order->id.' Cancelled by user' ;

            sendPusherData("Notification", "Order Cancellation", $chatMessage);

                $data = [
                    'link' => url(''),
                    'message' => $chatMessage,
                    'title' => 'Order Cancellation' 
                ];

                $userIds = AdminUser::pluck('id')->toArray();
                send_notification_admins($userIds, $data);

        }
        
         return response()->json(['status'=>'01', 'message'=>'Your Order Has been canceled.']);
    }

    protected function sendVerificationEmail($user)
    {
        \Mail::send('emails.account_verification', ['user' => $user], function ($m) use ($user) {
            $user_fullName = $user->first_name.' '.$user->last_name ;
            $m->to($user->email, $user_fullName);
            $m->subject('Account Verification');
        });
    }



    public function changePassword(Request $request) 
    {
        // validate change password request
        $request->validate( [
            'old_password'  => 'required|min:6|max:255',
            'password'      => 'required|confirmed|min:6|max:255',
        ]);

        $user = auth('api')->user();

        if(!$user){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }
        
        if ( !Hash::check($request->old_password, $user->password) ) {

            return response()->json(['status'=>'02','error' => ['your old password does not match with the new password']],200);
            
        } elseif ( $request->old_password == $request->password ) {
            
            return response()->json(['status'=>'02','error' =>  ['your old password cannot be the same as your new password']],200);
            
        } else {
            $user->password = bcrypt($request->password);
            if ($user->save()) {
                return response()->json(['status'=>'01','message' => 'password updated'],200);
            }
        }
        
        return response()->json(['status'=>'02','error' => ['could not update your password']],200);
    }

    
    public function deactivateAccount(Request $request) 
    {
        // validate change password request
       $request->validate( ['password' => 'required|confirmed|min:6|max:255']);

        $user = auth('api')->user();

        if(!$user){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }

        if ( !Hash::check($request->password, $user->password) ) {
            return response()->json(['status'=>'02','error'=>['Wrong Password']],200);
        }

        if ( $user->delete() ) {
            return response()->json([
                'status'  => '01',
                'message' => 'your account deleted successfully'
            ],200);
        }

        return response()->json(['status'=>'02','error' =>  ['could not delete your account']],200);

    }

     public function Favourite(Request $request)
    {
        $user = auth('api')->user();

        if(!$user){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }
//         $courses = [];
//         $wishlistCourses = Wishlist::where('frontend_user_id', $user->id)->where('type', 'course')->orderBy('created_at','desc')->get();
//     if($wishlistCourses->count() > 0){
//         $coursess = Course::find($wishlistCourses->pluck('type_id'));
//         foreach ($coursess as $course) {
//             $courseArr['id'] = $course->id ;
//             $courseArr['name'] = $course->name ;
//             $courseArr['descriptions'] = $course->descriptions ;
//             $courseArr['cover_photo'] = $course->cover_photo ;
//             $courseArr['lecture_hours'] = $course->lecture_hours ;
//             $courseArr['lectures_count'] = $course->lectures_count ;
//             $courseArr['date_from'] = $course->date_from ;
//             $courseArr['date_to'] = $course->date_to ;
//             $courseArr['lectures'] = json_decode($course->lectures) ;
//             $courseArr['faq'] = $course->faq ;
//             $courseArr['video_url'] = $course->video_url ;
//             $courseArr['phone_numbers'] = $course->phone_numbers ;
//             $courseArr['position'] = $course->position ;
//             $courseArr['price'] = $course->price ;
//             $courseArr['next_rounds'] = $course->next_rounds ;
//             $courseArr['requirements'] = $course->requirements ;
//             $courseArr['related_events'] = $course->related_events ;
//             $courseArr['related_courses'] = $course->related_courses ;
//             $courseArr['created_at'] = $course->created_at ;
//             $courseArr['updated_at'] = $course->updated_at ;
//             $courseArr['courses_related'] = $course->courses_related ;
//             $courseArr['events_related'] = $course->events_related ;
//             $courseArr['images'] = $course->images ;
//             $courseArr['instructors'] = $course->instructors ;
//             $courseArr['organizers'] = $course->organizers ;
//             $courses[] = $courseArr ;
//         }
// //        $p =  json_encode($courses);
// //        $p2 = json_decode($p, TRUE);
//     }

//         $wishlistEvents = Wishlist::where('frontend_user_id', $user->id)->where('type', 'event')->orderBy('created_at','desc')->get();
//         $events = Event::find($wishlistEvents->pluck('type_id'));
        
//         $wishlistIrentProduct = Wishlist::where('frontend_user_id', $user->id)->where('type', 'irent_product')->orderBy('created_at','desc')->get();
//         $irentProducts = Product::with('images')->find($wishlistIrentProduct->pluck('type_id'));
        
//         $wishlistIsellProduct = Wishlist::where('frontend_user_id', $user->id)->where('type', 'isell_product')->orderBy('created_at','desc')->get();
//         $isellProducts = Product::with('images')->find($wishlistIsellProduct->pluck('type_id'));
        $data = collect();
            foreach ($user->wishlist as $order) {
            if ( $order->type == 'course' ){
                $course = Course::where('id',$order->type_id)->first();
                if(!empty($course)){
                    $order->name = $course->name;
                    $order->image = 'https://phommunity.com/' . $course->cover_photo;
                } else {
                    $order->name = '';
                    $order->image = '';
                }
                $data->push($order);
            }
            if ( $order->type == 'event'  ){
                $event = Event::where('id',$order->type_id)->first();
                 if(!empty($event)){
                    $order->name = $event->name;
                    $order->image = 'https://phommunity.com/' . $event->cover_photo;
                 } else {
                    $order->name = '';
                    $order->image = '';
                }
                $data->push($order);
            }
            if ( $order->type == 'irent_product' ||  $order->type == 'isell_product'  ){
                $irproduct =  Product::with('images')->where('id',$order->type_id)->first();
                $order->name = $irproduct->name;
                if(isset($irproduct->images[0]->thumb_filename)){
                    $order->image = 'https://phommunity.com/uploads/thumbs/' . $irproduct->images[0]->thumb_filename;
                } else {
                    $order->image = '';
                }
                $data->push($order);
            } 
//            if ( $order->type == 'isell_product'  ){
//                $order->isell_product = Product::with('images')->where('id',$order->type_id)->get();
//                $data->push($order);
//            }
            
        }
        
        return response()->json([
                'status'  => '01',
                'data' => $data
           
        ],200);
    }


    public function StoreFavourite(Request $request)
    {

        $this->validate($request, [
            'type' => 'required',
            'type_id' => 'required'
        ]);

        $user = auth('api')->user();

        if(!$user){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }

        $oldWishlist = Wishlist::where('frontend_user_id', $user->id)
                                ->where('type', $request->type)
                                ->where('type_id', $request->type_id)
                                ->get();


        if ($oldWishlist->count() == 0) {

            $wishlist = $user->wishlist()->create([
                'frontend_user_id' => $user->id ,
                'type' => $request->input('type'),
                'type_id' =>  $request->input('type_id'),
            ]);
            return response()->json([
                'status'  => '01',
                'message' => 'added'
            ],200);
        }else{
            return response()->json([
                'status'  => '01',
                'message' => 'exist'
            ],200);
        }

        
    }

    public function deleteFavourite(Request $request)
    {
        $this->validate($request, [
            'type' => 'required',
            'type_id' => 'required'
        ]);

        $user = auth('api')->user();

        if(!$user){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }
        
        $wishlist = Wishlist::where('frontend_user_id', $user->id)
                            ->where('type', $request->type)
                            ->where('type_id', $request->type_id)
                            ->get();
        
        if ($wishlist->count() >= 1) {
   
            $wishlist->first()->delete();
        }
        
        return response()->json([
                'status'  => '01',
                'message' => 'removed'
            ],200);

    }


        public function removeCourse($id)
    {

        $user = auth('api')->user();

        if(!$user){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }
        
        $wishlist = Wishlist::where('frontend_user_id', $user->id)
                            ->where('type', 'course')
                            ->where('type_id', $id)
                            ->get();

        // dd($wishlist->toArray());
        
        if ($wishlist->count() >= 1) {
   
            $wishlist->first()->delete();
            return response()->json([
                'status'  => '01',
                'message' => 'Course Removed'
            ],200);
        }else{
            return response()->json([
                'status'  => '02',
                'message' => 'No Product '
            ],200);
        }
        
       
    }


    public function removeEvent($id)
    {

        $user = auth('api')->user();

        if(!$user){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }
        
        $wishlist = Wishlist::where('frontend_user_id', $user->id)
                            ->where('type', 'event')
                            ->where('type_id', $id)
                            ->get();
        
        if ($wishlist->count() >= 1) {
   
            $wishlist->first()->delete();
            return response()->json([
                'status'  => '01',
                'message' => 'Event Removed'
            ],200);
        }else{
            return response()->json([
                'status'  => '02',
                'message' => 'No Product '
            ],200);
        }
        
        
    }


    public function removeIrentProduct($id)
    {

        $user = auth('api')->user();

        if(!$user){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }
        
        $wishlist = Wishlist::where('frontend_user_id', $user->id)
                            ->where('type', 'irent_product')
                            ->where('type_id', $id)
                            ->get();
        
        if ($wishlist->count() >= 1) {
   
            $wishlist->first()->delete();
            return response()->json([
                'status'  => '01',
                'message' => 'I-Rent Product Removed'
            ],200);
        }else{
            return response()->json([
                'status'  => '02',
                'message' => 'No Product '
            ],200);
        }
        
        
    }


    public function removeIsellProduct($id)
    {

        $user = auth('api')->user();

        if(!$user){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }
        
        $wishlist = Wishlist::where('frontend_user_id', $user->id)
                            ->where('type', 'isell_product')
                            ->where('type_id', $id)
                            ->get();
        
        if ($wishlist->count() >= 1) {
            $wishlist->first()->delete();
            return response()->json([
                'status'  => '01',
                'message' => 'I-Rent Product Removed'
            ],200);
        }else{
            return response()->json([
                'status'  => '02',
                'message' => 'No Product '
            ],200);
        }
        
        
    }


}
