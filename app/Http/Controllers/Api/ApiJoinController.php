<?php

namespace App\Http\Controllers\Api;

use Faker\Provider\Uuid;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\JoinOurTeam;

class ApiJoinController extends Controller
{

    public function store(Request $request)
    {

       $request->validate( [
            "name"  => 'required|string|max:255',
            "email" => 'required|email',
            "job"   => 'required|string|max:255',
            "note"  => 'nullable|string|max:2000',
            'cv'    => 'nullable|file|max:5000|mimes:doc,docx,pdf,jpg,png,jpeg,zip'
        ]);

        $join = new JoinOurTeam();
        $join->name = $request->name;
        $join->email = $request->email;
        $join->job = $request->job;
        $join->note = $request->note;


        if(isset($request->cv) && !empty($request->cv) ) {
            $join->addDocument($request->cv);
        }

        $join->save();

        \Mail::send('emails.join_our_team', ['join' => $join], function ($m) {
            $m->to('info@cairophotography.club');
            $m->subject('Join Our Team');
        });

        if ($join->save()) {

             return Response()->json(['status'=>'01','message'=>'success' ],200);

        } else {

             return Response()->json(['status'=>'02','error'=>['failed'] ],200);
        }
    }

}
