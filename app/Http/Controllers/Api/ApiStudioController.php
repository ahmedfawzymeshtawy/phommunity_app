<?php

namespace App\Http\Controllers\Api;

use App\Models\Misc;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Models\StudioOption;
use App\Models\Product;
use App\Support\Services\MiscContainer;
use Carbon\Carbon;
use Response;

class ApiStudioController extends Controller
{
    public function home(MiscContainer $miscContainer)
    {
        $options = StudioOption::with('image', 'schedules')->latest()->get();
        $slides = Image::whereIn('id', $miscContainer->studio_slides)->get();

        return Response()->json(['status'=>'01','data' =>['studio'=> ['options'=>$options , 'slides' =>$slides  ] ]  ] ,200 );
    }


    public function calender(Request $request)
    {
        $request->validate(['option_id' => 'required']) ;
        $option = StudioOption::findOrFail($request->get('option_id'));
        if($option == null || $option->count() == 0){
            return Response::json(['status'=>'02','error' => ['No studios found' ]] ,200);
        }
        $date = Carbon::parse($request->get('date'));

        $allTimes = $this->getTimesList($date);

        foreach ($option->schedules()->where('date', $date)->get() as $schedule) {
            for ($i = 0; $i < count($allTimes); $i++) {
                if (
                    $allTimes[$i]['from'] == $schedule->from->format('H:i') &&
                    $allTimes[$i]['to'] == $schedule->to->format('H:i')
                ) {
                    $allTimes[$i]['available'] = 0;
                }
            }
        }

        return Response::json(['status'=>'01','data' => ['times' => $allTimes ]],200);
    }

    protected function getTimesList($date)
    {
        $allTimes = [];

        for ($i = 0; $i < 24; $i++) {
            $allTimes[] = [
                'date' => $date->format('d/m/Y'),
                'from' => str_pad($i, 2, '0', STR_PAD_LEFT) . ':00',
                'to' => str_pad($i + 1, 2, '0', STR_PAD_LEFT) . ':00',
                'available' => 1
            ];
        }

        return $allTimes;
    }

    public function productbranches(){
        $product = ['Maadi','Nasr City'];
        return Response::json(['status'=>'01','branches' =>$product ],200);
    }

}
