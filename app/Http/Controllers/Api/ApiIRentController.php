<?php

namespace App\Http\Controllers\Api;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Misc;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Wishlist;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;

class ApiIRentController extends Controller
{
    public function getCategoriesAndBrands(){

        $categories = Category::where('type', 'irent')->orderBy('name')->get();

        if($categories == null || $categories->count() == 0){
            return response()->json(['status'=>'02','error'=>['no categories found']],200);
        }

        $brands = Brand::where('type', 'irent')->orderBy('name')->get();

        if($brands == null || $brands->count() == 0){
            return response()->json(['status'=>'02','error'=>['no brands found']],200);
        }

        return Response::json(['status'=>'01','data'=>['categories' => $categories, 'brands' => $brands] ],200);
    }

    public function home(Request $request)
    {
        $products = $this->getProducts($request);
        $p =  json_encode($products);
        $p2 = json_decode($p, TRUE);

        return Response::json(['status'=>'01','data'=> ["products"=> array_values($p2) ] ],200);

    }

    public function getSignedProducts(Request $request)
    {
        $builder = Product::with('category', 'brand')->whereType('irent')->where('available', 1)
            ->where('sign', '!=', '')->latest();

        if ($request->has('q')) {
            $builder->where('name', 'LIKE', '%' . $request->get('q') . '%');
        }

        if ($request->has('category') && $request->get('category') != 0) {
            $builder->where('category_id', $request->get('category'));
        }

        if ($request->has('brand') && $request->get('brand') != 0) {
            $builder->where('brand_id', $request->get('brand'));
        }

        return $builder->take(20)->get();
    }

    public function getProducts(Request $request)
    {
        $builder = Product::with('category', 'brand', 'images')->whereType('irent')->where('available', 1)
            ->where('sign', '=', '')->latest();

        if ($request->has('q')) {
            $builder->where('name', 'LIKE', '%' . $request->get('q') . '%');
        }

        if ($request->has('category') && $request->get('category') != 0) {
            $builder->where('category_id', $request->get('category'));
        }

        if ($request->has('brand') && $request->get('brand') != 0) {
            $builder->where('brand_id', $request->get('brand'));
        }

        return $builder->get();
    }

    public function misc()
    {

        $misc = Misc::all();
        if($misc == null || $misc->count() == 0){
            return response()->json(['status'=>'02','error'=>['no data found']]);
        }
        return Response::json(['status'=>'01','data'=>['misc' =>$misc ] ],200);

    }

    public function productbranches($id){
        $product = Product::whereType('irent')->findOrfail($id)->getBranchesAttribute();
        if(empty($product)){
             return Response::json(['status'=>'02','error'=>['Product not available']]);
        }
        return Response::json(['status'=>'01','branches'=>$product ],200);
    }

    public function fetchProduct($id){

        $product = Product::with('category', 'brand')->whereType('irent')->findOrfail($id);
        if(empty($product)){
             return Response::json(['status'=>'02','error'=>['Product not available']]);
        }
        
        $user = auth('api')->user();

        if($user){
            $wishlist = Wishlist::where('frontend_user_id', $user->id)
                            ->where('type', 'irent_product')
                            ->where('type_id', $id)
                            ->get();
        if ($wishlist->count() > 0) {
            $isFav = 1;
        }else{
            $isFav = 0;
        }
        }else{
            $isFav = 0;
        }

        

        return Response::json(['status'=>'01','data'=>['product' =>$product , 'isFavourite'=>$isFav ] ],200);
    }
}
