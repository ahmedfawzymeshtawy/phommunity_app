<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\FrontendUser;
use App\Models\Message;
use App\Models\VisitorMessage;
use Illuminate\Http\Request;
use App\Models\AdminUser;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Response;
use App\Models\Image;


class ApiHelpersController extends Controller
{
    
    public function home()
    {
        $authUser       = auth('api')->user();
        $user = FrontendUser::find($authUser->id);

        if ($user == null) {
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }

        $messages   = Message::where('frontend_user_id', $user->id)->get();
        return Response::json(['status'=>'01','data'=>['messages' => $messages]],200);
    }



    public function messageSeen($userid)
    {
        $user = FrontendUser::findOrFail($userid);
        if ($user == null) {
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }
        $chatMessage = Message::where('frontend_user_id', $user->id)->where('seen', 0)->update(['seen' => 1]);
       return response()->json(['status' => '01','mesasge'=>'success'],200);
    }

    public function store(Request $request)
    {
        // dd( $request->all() );
        $docPath = env('UPLOAD_PATH').'ihelp-chat-files/';
        $uploadImgPath = $docPath . 'images/';
        $uploadFilesPath = $docPath . 'files/';
        $filePath = '';
        $isimage = 0;
        $isPdf = 0;

        $authUser = auth('api')->user();
        if(!$authUser){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }
        if ( $request->has('file')  ) {
            
            $this->validate($request, [
                'file' => 'required|file|max:5000|mimes:pdf,jpg,png,jpeg',
                'file_type' => 'required|string|max:100'
            ]);

            if ( $request->file_type != 'image' && $request->file_type != 'pdf' ) {
                $returnData = ['status' => '02', 'message' => 'image and pdf only allowed'];
                return Response::json($returnData, 200);
            }

            if ( $request->file_type === 'image' ) {
                    $img = $request->file ;
                    
                    $name= time() . '.' . $img->getClientOriginalExtension();
                    
                    $img->move($uploadImgPath,$name); 

                    $newImage = new Image();
                    $newImage->image_filename = $name;
                    $newImage->original_filename = $name;
                    $newImage->thumb_filename='thumb_'.$name;
                    $newImage->imageable_id = $authUser->id;
                    $newImage->imageable_type = 'App\Models\I-Help';
                    $newImage->save();
                $filePath = $uploadImgPath.$name ;
                $isimage = 1;
            } elseif ( $request->file_type === 'pdf' ) {
                $file = $request->file ;
                $files = time() . '_' . $file->getClientOriginalName();
                $file->move($uploadFilesPath, $files);
                $filePath = $uploadFilesPath.$files;
                $isPdf = 1;
            }

        }

        
        $user = FrontendUser::find($authUser->id);

        $message        = new Message;
        $message->message  = $request->get('message');
        $message->frontend_user_id =  auth('api')->check() ? $authUser->id : 0 ;
        $message->seen   = 0;
        $message->admin_id   = 0;
        $message->admin  = 0;
        $message->file_path  = $filePath;
        $message->file  = $isPdf;
        $message->image  = $isimage;
        $message->save() ;


        $chatMessage = Message::with('frontendUser', 'adminUser')->find($message->id);
        
        sendPusherData("helper_chat", "helper_chat_send", $chatMessage);

        $data = [
            'link' => url(''),
            'message' => 'New Help Request from user '.$authUser->first_name.' '.$authUser->last_name,
            'title' => 'I-Help' 
        ];

        $userIds = AdminUser::pluck('id')->toArray();
        send_notification_admins($userIds, $data);

        return Response::json(['status' =>'01' ,'message'=>'Message Sent!']);
    }


    public function deleteMessage($id)
    {

        $userId = auth('api')->user()->id;
        $requestedMessage = Message::where('frontend_user_id', $userId)->where('id', $id)->get();

        if ($requestedMessage->isEmpty()) {
            $returnData = ['status' => '02', 'message' => 'message not found'];
            return Response::json($returnData, 200);
        }

        $message = $requestedMessage->first();
        
        if ( $message->image == true || $message->file == true ) {
            $oldPath = public_path($message->file_path); // server path
            if ( $oldPath != '' ) {
                if (file_exists($oldPath)) unlink($oldPath);
            }
        }        

        $message->delete();

        sendPusherData("helper_chat", "helper_message_delete", ['user_id' => $userId , 'message_id' => $id] );

        return response()->json(['status'=>'01','message'=>'success'],200);
    }
}

