<?php

namespace App\Http\Controllers\Api;

use App\Models\FrontendUser;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Assistant;
use App\Models\Course;
use App\Models\Event;
use App\Models\Lab;
use App\Models\Notification;
use App\Models\Order;
use App\Models\SecondShooter;
use App\Models\StudioOption;
use App\Models\AdminUser;
//use Pusher\Pusher;

class ApiOrdersController extends Controller
{
    public function secondShooter(Request $request)
    {
        $user = auth('api')->user() ;
        if(!$user){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }

        $shooter = SecondShooter::where(['frontend_user_id' => $user->id ])->first();

        if (!$shooter) {

            return response()->json(['status'=>'02','error' =>[ 'User does not have secondshooter']],200);
        }

        $request->validate([
            'title' => 'required|string|max:255',
            'client_name' => 'required|string',
            'client_phone_number' => 'required|string',
            'client_email_address' => 'required|string',
            'notes' => 'nullable|string'
        ]);

        $var =  $shooter->orders()->create([
            'frontend_user_id' => $user->id,
            'type' => 'SecondShooter',
            'new' =>0,
            'meta' => [
                        'title' => $request->get('title'),
                        'client_name' => $request->get('client_name'),
                        'client_phone_number' => $request->get('client_phone_number'),
                        'client_email_address' => $request->get('client_email_address'),
                        'notes' => $request->get('notes')
            ],
            'status' => 'pending'
        ]);

        $notification = new Notification([
            'message' => 'New Request from: ' . $shooter->frontendUser->full_name,
            'title' => 'SecondShooter',
            'type' => '0',
            'user_id' => $shooter->frontendUser->id,
            'link' => url('orders/second-shooter', $shooter->frontend_user_id ),
        ]);
        $notification->save();

        $notification->users()->attach([$shooter->frontend_user_id], ['new' => 1]);

        // $pusher->trigger("user-{$shooter->frontend_user_id}", 'notification', [
        //     'title' => $notification->title,
        // ]);

        if ($var) {

             return response()->json(['status'=>'01','message'=>'success'],200);
        } else {
             return response()->json(['status'=>'02','error'=>['failed']],200);
        }
    }


    public function assistant(Request $request)
    {

        $user = auth('api')->user() ;
        if(!$user){
            return response()->json(['status'=>'02','error' => ['User not found ']],200);
        }

        $assistant = Assistant::where(['frontend_user_id' => $user->id])->first();

        if (!$assistant) {

            return response()->json(['status'=>'02','error' => ['User does not have assistants ']],200);
        }
        $request->validate([
            'title' => 'required|string|max:255',
            'client_name' => 'required|string',
            'client_phone_number' => 'required|string',
            'client_email_address' => 'required|string',
            'notes' => 'nullable|string'
        ]);

        $var =  $assistant->orders()->create([
            'frontend_user_id' => $user->id,
            'type' => 'Assistant',
            'new' => 1,
            'meta' => [
                        'title' => $request->get('title'),
                        'client_name' => $request->get('client_name'),
                        'client_phone_number' => $request->get('client_phone_number'),
                        'client_email_address' => $request->get('client_email_address'),
                        'notes' => $request->get('notes')
                    ],
            'status' => 'pending'
        ]);

        $notification = new Notification([
            'message' => 'New Request from: ' . $assistant->frontendUser->full_name,
            'title' => 'Assistant',
            'type' => '0',
            'user_id' => $assistant->frontendUser->id,
            'link' => url('orders/assistant', $assistant->frontend_user_id ),
        ]);
        $notification->save();

        $notification->users()->attach([$assistant->frontend_user_id], ['new' => 1]);

        // $pusher->trigger("user-{$assistant->frontend_user_id}", 'notification', [
        //     'title' => $notification->title,
        // ]);

        if ($var) {
             return response()->json(['status'=>'01','message'=>'success'],200);
        } else {
             return response()->json(['status'=>'02','error'=>['failed']],200);
        }
    }

    public function studioOption(Request $request)
    {
        $user = auth('api')->user();

        if($user == null){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }

        $request->validate([
            'title' => 'required|string|max:255',
            'client_name' => 'required|string',
            'client_phone_number' => 'required|string',
            'client_email_address' => 'required|string',
            'notes' => 'nullable|string',
            'branch' => 'required|string',
            'chrome_color' => 'nullable|string',
            'chrome_color_2' => 'nullable|string',
            'date' => 'required|string',
            'start_time' => 'required|string',
            'end_time' => 'required|string',
        ]);

        if ($request->get('studio_option_id')) {
            $option = StudioOption::find($request->get('studio_option_id'));

                    $var = $option->orders()->create([
                    'frontend_user_id' => $user->id,
                    'type' => 'Studios',
                    'new' => 0,
                    'meta' => [
                            'title' => $request->get('title'),
                            'client_name' => $request->get('client_name'),
                            'client_phone_number' => $request->get('client_phone_number'),
                            'client_email_address' => $request->get('client_email_address'),
                            'notes' => $request->get('notes'),
                            'branch' => $request->get('branch'),
                            'chrome_color' => $request->get('chrome_color'),
                            'chrome_color_2' => $request->get('chrome_color_2'),
                            'date' => $request->get('date'),
                            'start_time' => $request->get('start_time'),
                            'end_time' => $request->get('end_time'),
                    ],
                    'status' => 'new'
                ]);
                
                if ($var->save()) {
                $chatMessage = 'Studio : ' . $option->title.' has been reserved by user '. $user->first_name;

                sendPusherData("Notification", "Order_Reservation", $chatMessage);

                $data = [
                    'link' => url('studio'),
                    'message' => $chatMessage,
                    'title' => 'Studio' 
                ];

                $userIds = AdminUser::pluck('id')->toArray();
                send_notification_admins($userIds, $data);

                return response()->json(['status'=>'01',
                'message' => 'Your Order Has Sent Successfully We Will Call You To Confirm Your Reservation'],200);

                } else {

                     return response()->json(['status'=>'02','error'=>['failed']],200);

                }
        }else{
             return response()->json(['status'=>'02','error'=>['failed']],200);

        }
    }

    public function labOption(Request $request)
    {
        $user =auth('api')->user() ;
        if(!$user){
            return response()->json(['status'=>'02','error'=>['User not found']]);
        }

        $request->validate([
            'client_name' => 'required|string',
            'client_phone_number' => 'required|string',
            'client_email_address' => 'required|string',
            'notes' => 'nullable|string',
            'branch' => 'required|string',
            'date' => 'required|string',
            'start_time' => 'required|string',
            'end_time' => 'required|string'
        ]);

        $option = Lab::where('id',$request->get('lab_option_id'))->get()->first();

        if($option == null || $option->count() == 0){
            return Response()->json(['status'=>'02','error' => ['No Room available' ]] ,200);
        }

        $var =  $option->orders()->create([
            'frontend_user_id' => $user->id,
            'type' => 'Rooms',
            'new' => 0,
            'meta' => [
                'client_name' => $request->get('client_name'),
                'client_phone_number' => $request->get('client_phone_number'),
                'client_email_address' => $request->get('client_email_address'),
                'notes' => $request->get('notes'),
                'branch' => $request->get('branch'),
                'date' => $request->get('date'),
                'start_time' => $request->get('start_time'),
                'end_time' => $request->get('end_time'),
            ],
            'status' => 'new'
        ]);

      if ($var->save()){
         
                $chatMessage = 'Room : ' . $option->title.' has been reserved by user '. $user->first_name;

                sendPusherData("Notification", "New Order Reservation", $chatMessage);

                $data = [
                    'link' => url('labs'),
                    'message' => $chatMessage,
                    'title' => 'Room' 
                ];

                $userIds = AdminUser::pluck('id')->toArray();
                send_notification_admins($userIds, $data);
            return response()->json(['status'=>'01','message'=>'Your Order Has Sent Successfully We Will Call You To Confirm Your Reservation'],200);
        } else {
            return response()->json(['status'=>'02','error'=>['failed']],200);

        }
    }

    public function course(Request $request)
    {

        $user = auth('api')->user() ;

        if(!$user){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }

        $request->validate([
            'client_name' => 'required|string',
            'client_phone_number' => 'required|string',
            'client_email_address' => 'required|string',
            'notes' => 'nullable|string'
        ]);

        $course = Course::find($request->get('course_id'));

        if ($course == null || $course->count() == 0) {

            return response()->json(['status'=>'02','error'=>['Course not found']],200);
        }

        $var = $course->orders()->create([
            'frontend_user_id' => auth('api')->user()->id,
            'type' => 'Courses',
            'new' =>0,
            'meta' => [
                'client_name' => $request->get('client_name'),
                'client_phone_number' => $request->get('client_phone_number'),
                'client_email_address' => $request->get('client_email_address'),
                'notes' => $request->get('notes'),
            ],
            'status' => 'new'
        ]);

        if ($var->save()) {
            // $notification = new Notification([
            //     'message' => 'Course : ' . $course->name.' has been reserved by You',
            //     'title' => 'Course Reservation',
            //     'type' => '0',
            //     'user_id' => auth('api')->user()->id,
            //     'link' => url('course', $course->id ),
            // ]);
            // $notification->save();


                $chatMessage = 'Course : ' . $course->name.' has been reserved by user '. auth('api')->user()->first_name;

                sendPusherData("Notification", "New Course Reservation", $chatMessage);

                $data = [
                    'link' => url('course', $course->id ),
                    'message' => $chatMessage,
                    'title' => 'Course' 
                ];

                $userIds = AdminUser::pluck('id')->toArray();
                send_notification_admins($userIds, $data);

            return response()->json(['status'=>'01','message'=>'success'],200);

        } else {
            return response()->json(['status'=>'02','error'=>['Failed']],200);

        }
    }


    public function event(Request $request)
    {

        $user = auth('api')->user() ;

        if(!$user){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }

        $request->validate([
            'client_name' => 'required|string',
            'client_phone_number' => 'required|string',
            'client_email_address' => 'required|string',
            'notes' => 'nullable|string'
        ]);

        $event = Event::find($request->get('event_id'));

        if ($event == null || $event->count() == 0) {

             return response()->json(['status'=>'02','error'=>['Event not found']],200);
        }

        $var = $event->orders()->create([
            'frontend_user_id' => $user->id,
            'type' => 'Events',
            'new' => 0,
            'meta' => [
                'client_name' => $request->get('client_name'),
                'client_phone_number' => $request->get('client_phone_number'),
                'client_email_address' => $request->get('client_email_address'),
                'notes' => $request->get('notes'),
            ],
            'status' => 'new'

        ]);

        if ($var->save()) {
            // $notification = new Notification([
            //     'message' => 'Event : ' . $event->name.' has been reserved by You',
            //     'title' => 'Event Reservation',
            //     'type' => '0',
            //     'user_id' => auth('api')->user()->id,
            //     'link' => url('event', $event->id ),
            // ]);
            // $notification->save();

             $chatMessage = 'Event : ' . $event->name.' has been reserved by user '. auth('api')->user()->first_name;

                sendPusherData("Notification", "New Event Reservation", $chatMessage);

                $data = [
                    'link' => url('event', $event->id ),
                    'message' => $chatMessage,
                    'title' => 'Event' 
                ];

                $userIds = AdminUser::pluck('id')->toArray();
                send_notification_admins($userIds, $data);

             return response()->json(['status'=>'01','message'=>'Success'],200);

        } else {
             return response()->json(['status'=>'02','error'=>['failed']],200);

        }
    }

    public function irent(Request $request)
    {
        $user = auth('api')->user() ;
        if(!$user){
            return response()->json(['status'=>'02','error'=>['User not found']],200);
        }

        $request->validate([
            'date' => 'required|string',
            'time' => 'required|string',
            'branch' => 'required|string',
            'phone_number' => 'required',
            'email' => 'required|email',
            'notes' => 'nullable|string',
            'products' => 'required|array'
        ]);

    if ( isset( $request->products ) && $request->filled('products') )  {
        //dd($request->products);
        foreach ($request->products as  $key => $value) {
                $product = Product::with('images')->whereType('irent')->findOrfail($key);
               
               $products['id'] = $product->id ;
               $products['price24'] = $product->price ;
               $products['price12'] = $product->priceHalf ;
               $products['amount'] = $value['amount'] ;
               $products['day24'] = $value['day24'] ;
               $products['name'] = $product->name ;
               if($product->images){
                    foreach ($product->images as $value) {
                        $products['image'] = env("UPLOAD_URL"). "thumbs/" .$value->thumb_filename;
                    }
               }
               
               $arrOfProduct[] = $products ;
               
        }
        $pjson = json_encode($arrOfProduct);
           //dd($arrOfProduct); 
            $metaAttributes = [
                'date'          => $request->get('date'),
                'time'          => $request->get('time'),
                'branch'        => $request->get('branch'),
                'phone_number'  => $request->get('phone_number'),
                'email'         => $request->get('email'),
                'notes'         => $request->get('notes'),
                'products'      => $pjson,
            ];

            $irentOder = new Order;
            
            $irentOder->type    = 'I-Rent';
            $irentOder->status  = 'new';
            $irentOder->meta    = $metaAttributes;
            $irentOder->frontend_user_id = $user->id ;


        if ($irentOder->save()) {
            return response()->json(['status'=>'01','message'=>'success'],200);
        } else {
            return response()->json(['status'=>'02','error'=>['failed']],200);

        }
    }else{
        return response()->json(['status'=>'02','error'=>['Products not found']],200);
    }
    }


    public function isell(Request $request)
    {
        $metaAttributes = [
            'client_name' => $request->get('client_name'),
            'client_phone_number' => $request->get('client_phone_number'),
            'client_email_address' => $request->get('client_email_address'),
            'notes' => $request->get('notes'),
        ];

        $request->validate([
            'client_name' => 'required|string',
            'client_phone_number' => 'required|string',
            'client_email_address' => 'required|string',
            'notes' => 'nullable|string'
        ]);

        $product = Product::find($request->get('product_id'));

//        if($product == null || $product->count() == 0){
//            return response()->json(['status'=>'02','error'=>['no product found']],200);
//        }

        if ($product) {
            $var = $product->orders()->create([
                'frontend_user_id' => auth('api')->user()->id,
                'type' => 'I-Sell',
                'new' => 0,
                'status' => 'new',
                'meta' => $metaAttributes
            ]);
        } else {
            $var = Order::create([
                'frontend_user_id' => auth('api')->user()->id,
                'type' => 'I-Sell',
                'new' => 0,
                'status' => 'new',
                'meta' => $metaAttributes
            ]);
        }

        if ($var->save()) {
                    // $notification = new Notification([
                    //     'message' => 'New Request Product from '. auth('api')->user()->first_name.'',
                    //     'title' => 'New Request Product',
                    //     'type' => '0',
                    //     'user_id' => auth('api')->user()->id,
                    //     'link' => url('isell'),
                    // ]);
                    // $notification->save();

                $chatMessage =' New Request Product from '. auth('api')->user()->first_name;

                sendPusherData("Notification", "New Request Product", $chatMessage);

                $data = [
                    'link' => url('isell'),
                    'message' => $chatMessage,
                    'title' => 'I-Sell ' 
                ];

                $userIds = AdminUser::pluck('id')->toArray();
                send_notification_admins($userIds, $data);
             return response()->json(['status'=>'01','message'=>'success'],200);

        } else {
             return response()->json(['status'=>'02','error'=>['failed']],200);

        }
    }

}
