<?php

namespace App\Http\Controllers\Site;

use App\Models\FrontendUser;
use App\Support\Message\Toast;
use App\Support\Services\AddImagesToEntity;
use Illuminate\Http\Request;

class RegistrationController extends SiteController
{
    
    public function signup()
    {
        return view('site.auth.signup');
    }

    public function showRegister()
    {
        return view('site.register.show');
    }

    public function doRegister(Request $request)
    {
        // dd($request->all());

        $this->isValidInput($request);

        $input = $request->only(
            'first_name', 'last_name', 'email', 'password', 'mobile', 'level',
            'camera_brand', 'camera_model', 'interested_in','gender'
        );
        $input['password'] = bcrypt($request->get('password'));
        $input['active'] = 0;
        $input['provider'] = 'email';
        $input['verification_token'] = str_random(64);

        if($request->interested_in){
            $input['interested_in']  = json_encode($request->interested_in);
        }


        $user = new FrontendUser($input);

        if ($user->save()) {

            if ($request->hasFile('photo')) {
                (new AddImagesToEntity($request->file('photo'), $user, 'photo'))->execute();
            }

            $this->sendVerificationEmail($user);

            //toast('Your account has been registered successfully. Please, follow the instructions sent to your email address to activate your account.');

            return redirect(route('site.home') . '#registration-success');
        }

        flashMessage('Request failed, try again later', 'error');
        return redirect()->back()->withInput();
    }

    protected function sendVerificationEmail($user)
    {
        \Mail::send('emails.account_verification_new', ['user' => $user], function ($m) use ($user) {
            $m->to($user->email, $user->fullName);
            $m->subject('Account Verification');
        });
    }

    protected function isValidInput(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:frontend_users,email',
            'password' => 'required|min:6',
            'mobile' => 'required|numeric|unique:frontend_users,mobile',
            'gender' => 'required',
            'interested_in' => 'required'
        ]);
    }

    protected function getRedirectUrl()
    {
        return url()->previous() . '#register';
    }
}