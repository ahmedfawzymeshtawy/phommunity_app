<?php
namespace App\Http\Controllers\Site;

use App\Models\FrontendUser;
use App\Models\AdminUser;
use App\Models\Order;
use App\Models\OrderFeedback;
use App\Support\Services\AddImagesToEntity;
use Illuminate\Http\Request;
use Hash;

class ProfileController extends SiteController
{

    public function show()
    {
     
        $user = auth()->user();

        $frontendUser = FrontendUser::with('photo')->find($user->id);


        // dd($user->photo);

        return view('site.profile.home', ['frontendUser' => $user]);
    }

    public function edit()
    {
        // dd(auth('frontend')->user()->first_name);
        return view('site.profile.edit');
    }

    public function update(Request $request)
    {

        $user = auth('frontend')->user();

        $this->validate($request, [
            'first_name'    => 'required|string|max:255',
            'last_name'     => 'required|string|max:255',
            'email'         => 'required|email|max:255',
            'password'      => 'required|confirmed|min:6|max:255',
            'mobile'        => 'required|string|min:8|max:15|unique:frontend_users,mobile,'.$user->id,
            'gender'        => 'required|max:10',
            'level'         => 'required|string|max:255',
            'camera_brand'  => 'nullable|numeric',
            'camera_model'  => 'nullable|numeric',
            'interested_in' => 'required|array',
            'photo'         => 'nullable|image|mimes:jpeg,png,jpg|max:2048'
        ]);
   

        $inputs = $request->only(
            'first_name', 'last_name', 'email', 'mobile', 'level',
            'camera_brand', 'camera_model', 'interested_in','gender'
        );

        $inputs['password'] = bcrypt($request->get('password'));

        $user->fill($inputs);

        if($request->interested_in){
            $user->interested_in = json_encode($request->interested_in);
        }

        $user->save();
        if ($request->hasFile('photo')) {
            if(!empty($user->photo)){
                $user->photo->delete();
                // if user upload image we remove avatar image
                $user->avatar = '';
                $user->save();
            }
            
            (new AddImagesToEntity($request->file('photo'), $user, 'photo'))->execute();
        }

        toast("Your profile has been updateds successfully.");
        return redirect()->back();
    }


    public function myOrders(){

        $authUser = auth('frontend')->user();
        $courses  = collect();
        $events   = collect();
        $gears    = collect();
        $rooms    = collect();
        $studios  = collect();
        $isells   = collect();
        
        foreach ($authUser->orders as $order) {
            if ( $order->type == $this->sections_names['course'] ) $courses->push($order);
            if ( $order->type == $this->sections_names['event']  ) $events->push($order);
            if ( $order->type == $this->sections_names['rent']   ) $gears->push($order);
            if ( $order->type == $this->sections_names['rooms']  ) $rooms->push($order);
            if ( $order->type == $this->sections_names['studio'] ) $studios->push($order);
            if ( $order->type == $this->sections_names['sell']   ) $isells->push($order);
        }

        // dd( $isells->first() );

        return view('site.user-orders', compact('courses','events','gears','rooms','studios','isells') );
    }


    public function viewOrder($id){
        
        $order = Order::findOrFail($id);
        return view('site.profile.order_modal', compact('order'));
    }


    public function cancelOrder(Request $request){

        $this->validate($request, [
            'order_id'      => 'required|numeric',
            'feedback'      => 'nullable|string|max:255',
        ]);
        
        $order = Order::findOrFail($request->order_id);
        $order->status = 'canceled';

        // dd($order->type, $this->getSectionLink($order->type));
        
        if($order->save()) {
            // if user enter a feedback            
            if( $request->filled('feedback') ) {
    
                $feedback = new OrderFeedback;
                $feedback->order_id = $request->order_id;
                $feedback->feedback = $request->feedback;
                $feedback->save();
            }
            
            $data = [
                'link'      => $this->getSectionLink($order->type),
                'message'   => 'Order Number '.$order->id.' in '.$order->type.' has been canceled by User',
                'title'     => $order->type
            ];
            $userIds = AdminUser::pluck('id')->toArray();
            send_notification_admins($userIds,$data);
            
            \Toast::success('Your Order Has been canceled.')->flash();
        }
        
        return redirect()->back();
    }



    public function getSectionLink($name){

        $sections_names = [
            "Courses",
            "Events",
            "I-Rent",
            "I-Sell",
            "Studios",
            "Rooms",
        ];

        $sections_links = [
            route('admin.ilearn.courses.index'),
            route('admin.ilearn.events.index'),
            route('admin.irent.orders.index'),
            route('admin.isell.orders.index'),
            route('admin.studio_options.index'),
            route('admin.rooms.index'),
        ];

        $key = array_search($name, $sections_names);

        return $key === false ? '' : $sections_links[$key];
    }


    public function delete(){

        $user = auth('frontend')->id();

        auth('frontend')->logout();

        $del = FrontendUser::findOrFail($user)->delete();

        toast("Your profile has been deleted successfully.");
        
        return redirect()->route('site.home');
    }
}