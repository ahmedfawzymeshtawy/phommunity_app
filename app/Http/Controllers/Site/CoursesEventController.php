<?php

namespace App\Http\Controllers\Site;
use Illuminate\Http\Request;
use App\Models\CoursesEvent;
use App\Models\CoursesEventCH;
use App\Models\CoursesEventSH;

class CoursesEventController extends SiteController {

    public function index(){
        return view( 'site.events.courses_june.index' );
    }

    public function finishedRequest(){
        return view('site.events.courses_june.finished');
    }

    public function store(Request $request)
    {  
        
        $coursesEvent = new CoursesEvent;        
        $coursesEvent->name      = $request->get('name');
        $coursesEvent->email     = $request->get('email');
        $coursesEvent->branch    = $request->get('branch');
        $coursesEvent->mobile    = $request->get('mobile');
        $coursesEvent->job       = $request->get('job');
        $coursesEvent->courses   = json_encode([ 'courses' => $request->get('courses') ]);
        $coursesEvent->note      = $request->get('note');
        $coursesEvent->status    = 'new';

        if($coursesEvent->save()) {
            return redirect()->route('coursesEvent.finishedRequest');
        }
    }



}