<?php
namespace App\Http\Controllers\Site;
use App\Models\Pages;
use App\Models\Lab;
use App\Models\Image;
use App\Support\Services\MiscContainer;

class LabsController extends SiteController {

    public function index(MiscContainer $miscContainer)
    {
        $options = Lab::with('images')->where('available', 1)->latest()->get()->sortBy('position');
        $slides = Image::whereIn('id', $miscContainer->lab_slides)->get();
        $page = Pages::with('metaTags')->where('name','lab')->get()->first();

        return view('site.labs.index', compact('options','slides','page'));
    }
}