<?php
namespace App\Http\Controllers\Site;

use App\Models\Notification;
use App\Models\Order;
use Illuminate\Http\Request;

class RequestsController extends SiteController
{
    public function accept($id)
    {
        return $this->changeStatus($id, 'accepted');
    }

    public function reject($id)
    {
        return $this->changeStatus($id, 'rejected');
    }

    public function finish($id)
    {
        return $this->changeStatus($id, 'finished');
    }

    protected function changeStatus($orderId, $newStatus, $label = null)
    {
        $order = Order::findOrFail($orderId);

        $order->fill(['status' => $newStatus])->save();

        $label = $label ?: $newStatus;
        $message = "Your request for '{$order->orderable->frontendUser->first_name}' has been {$label}.";
//       $this->notifyUser($order->frontendUser, $message);
        $data=['link'=>url('assistants'),'message'=>$message,'title'=>'Requests'];
        send_notification_users([$order->frontendUser->id],$data);

        return response()->json('success');
    }

    public function cancel($id)
    {
        $order = Order::findOrFail($id);

        $order->fill(['status' => 'cancelled'])->save();

        if ($order->frontend_user_id == auth()->user()->id) {
            $user = $order->orderable->frontendUser;
        } else {
            $user = $order->frontendUser;
        }

        $message = "Your request for '{$order->orderable->frontendUser->full_name}' has been cancelled.";
        $this->notifyUser($user, $message);

        return response()->json('success');
    }

    protected function notifyUser($user, $message)
    {
        $notification = new Notification([
            'title' => $message
        ]);
        $notification->save();

        $notification->users()->attach([$user->id], ['new' => 1]);

        app('Pusher\Pusher')->trigger("user-{$user->id}", 'notification', [
            'title' => $notification->title,
        ]);
    }

    public function review($id, Request $request)
    {
        $order = Order::findOrFail($id);

        $target = $order->orderable;
        $target->fill([
            'rating' => ceil(($target->rating + $request->get('rating')) / 2)
        ])->save();

        $order->fill([
            'status' => 'reviewed',
            'meta' => json_encode([
                'review' => $request->get('review'),
                'rating' => $request->get('rating')
            ])
        ])->save();

        toast("Thank you for your review.");
        return redirect()->back();
    }
}