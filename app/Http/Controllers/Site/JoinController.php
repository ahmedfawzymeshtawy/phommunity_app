<?php

namespace App\Http\Controllers\Site;

use App\Models\JoinOurTeam;
use Illuminate\Http\Request;
use App\Support\Message\Toast;
use App\Support\Message\AbstractMessage;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class JoinController extends SiteController
{

    protected $validationRules = [
        "name"  => 'required|string|max:255',
        "email" => 'required|email',
        "job"   => 'required|string|max:255',
        "note"  => 'nullable|string|max:2000',
        'cv'    => 'nullable|file|max:5000|mimes:doc,docx,pdf,jpg,png,jpeg,zip',
    ];


    public function store(Request $request)
    {
        $this->validate($request, $this->validationRules);
        $input = $this->getInput($request);
        $join  = new JoinOurTeam($input);

        if(isset($input['cv']) && !empty($input['cv']) ) {
            $join->addDocument($input['cv']);
        }

        $join->save();

        \Mail::send('emails.join_our_team', ['join' => $join], function ($m) {
            $m->to('hr@cairophotography.club');
            $m->subject('Join Our Team');
        });

        \Toast::success('Message sent successfully.')->flash();

        return redirect()->back();
    }


    public function getInput(Request $request)
    {
        $input = $request->only(
            'name', 'email', 'job', 'note', 'cv'
        );

        return $input;
    }


}
