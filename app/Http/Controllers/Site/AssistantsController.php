<?php

namespace App\Http\Controllers\Site;

use App\Models\Assistant;
use Illuminate\Http\Request;
use DB;

class AssistantsController extends SiteController {

    public function index(Request $request)
    {
        $builder = Assistant::query()
            ->has('frontendUser')
            ->with('frontendUser.photo', 'orders')
            ->latest();

        if ($request->has('q')) {
            $builder->whereHas('frontendUser', function ($query) use ($request) {
                return $query->where(DB::raw('concat(first_name," ",last_name)') , 'LIKE', '%' . $request->get('q') . '%')
                    ->orWhere('email', 'LIKE', '%' . $request->get('q') . '%');
            });

        }


        $assistants = $builder->get();
        $featuredAssistants = $assistants->filter(function ($item) {
            return $item->featured == 1;
        });

        return view('site.assistants.index', compact('assistants', 'featuredAssistants'));
    }

    public function showJoin()
    {
        return response()->json(
            view('site.assistants._join')->render()
        );
    }

    public function doJoin(Request $request)
    {
        $this->isValidInput($request);

        $input = $request->only('about', 'area');
        $input['featured'] = 0;
        $input['rating'] = 0;
        $input['approved'] = 1;
        $input['frontend_user_id'] = auth('frontend')->user()->id;

        $assistant = new Assistant($input);
        if ($assistant->save()) {
            flashMessage('You have successfully joined assistants team.');
            return redirect()->route('site.assistants.index');
        }

        flashMessage('Request failed, try again later', 'error');
        return redirect()->back()->withInput();
    }

    protected function isValidInput(Request $request)
    {
        $this->validate($request, [
            'about' => 'required',
            'area' => 'required'
        ]);
    }


    public function topten(Request $request)
    {
        $builder = Assistant::query()
            ->has('frontendUser')
            ->with('frontendUser.photo', 'orders')
            ->latest();

        if ($request->has('q')) {
            $builder->whereHas('frontendUser', function ($query) use ($request) {
                return $query->where(DB::raw('concat(first_name," ",last_name)') , 'LIKE', '%' . $request->get('q') . '%')
                    ->orWhere('email', 'LIKE', '%' . $request->get('q') . '%');
            });

        }


        $assist = $builder->get();
        $assistants = $assist->filter(function ($item) {
            return $item->featured == 1;
        });

        return view('site.assistants.top10', compact('assistants'));
    }

}