<?php

namespace App\Http\Controllers\Site;

use App\Models\FrontendUser;
use App\Models\PasswordReset;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AuthController extends SiteController
{
    protected $redirectHash;

    public function doLogin(Request $request)
    {

        // dd($request->all());

        $user = FrontendUser::where('email','=',$request->get('email'))->get()->first();

        // dd($user);

        if(($user != null) && ($user->active == 0)){
            toast("Please Activate Your Account Via Email First", "error");
            return redirect()->to(url()->previous() . '#login');
        }

        // if user logedin from social media 
        if ( ($user != null) && ($user->password == '' || $user->password == null) ) {
            toast("PLease finish your profile information befor login", "error");
            return redirect()->route("site.signup.provider" , $user->provider_id );
        }

        $creds = [
            "email" => $request->get('email'),
            'password' => $request->get('password'),
            'active' => 1
        ];

        
        // set the remember me cookie if the user check the box
        $remember = ($request->has('remember')) ? true : false;
        

        // dd(auth('frontend')->attempt($creds, $remember) );
        // dd($creds, $request->all());
        if (auth('frontend')->attempt($creds, $remember)) {
            
            toast("You've signed in successfully.");

            if( auth('frontend')->user()->first_sign == 0  ){
                $firstUser = FrontendUser::find(auth('frontend')->user()->id);
                $firstUser->first_sign = 1;
                $firstUser->save();
                return redirect('profile/#');
            }
            return redirect('/home');
        }
        
        toast("Email and password don't match.", "error");
        return redirect()->to(url()->previous() . '#login');
    }

    public function doLogout()
    {
        auth('frontend')->logout();

        toast("You've logged out successfully.");
        return redirect()->route('site.home');
    }


    public function getForgetPassword()
    {
       return view('site.auth.forget_password');
    }


    public function doForgetPassword(Request $request)
    {
        $this->redirectHash = '#forget-password';

        $this->validate($request, [
            'email' => 'required|email|exists:frontend_users,email'
        ]);

        $user = FrontendUser::where('email', $request->get('email'))->first();

        $record = PasswordReset::where('email', $user->email)->firstOrCreate([
            'email' => $user->email,
            'token' => str_random(64),
            'created_at' => Carbon::now()
        ]);

        \Mail::send('emails.forget_password', ['user' => $user, 'record' => $record], function ($m) use ($user) {
            $m->to($user->email, $user->first_name.' '.$user->last_name);
            $m->subject('Reset Password');
        });

        toast("An email has been sent to your email address containing a link to reset your password.");
        return redirect()->back();
    }


    public function showResetPassword($token)
    {
        $record = \DB::table('password_resets')->where('token', $token)->first();
        if (!$token || !$record) {
            throw new NotFoundHttpException();
        }

        return view('site.auth.reset_password',compact('token'));
    }


    public function doResetPassword(Request $request)
    {
        $this->redirectHash = '#reset-password';

        $token = $request->get('token');
        $record = \DB::table('password_resets')->where('token', $token)->first();

        if (!$token || !$record) {
            throw new NotFoundHttpException();
        }

        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
        ]);

        $user = FrontendUser::where('email', $record->email)->firstOrFail();

        $user->fill(['password' => bcrypt($request->get('password'))])->save();

        toast("Your password has been reset successfully.");
        return redirect()->route('site.home');
    }

    public function doActivateAccount($token)
    {
        
        $account = FrontendUser::where('verification_token', $token)->firstOrFail();

        $account->fill(['active' => 1])->save();

        $notfidata = ['link'=> url('/'), 'message' => 'Welcome To Phommunity The First Photography Community In Egypt Now You Can Log In With Your ID ', 'title'=> 'Welcome'];

        //send_notification_users($account->id, $notfidata);

        //toast("Your account has been activated successfully.");

        return view('site.home');
    }

    protected function getRedirectUrl()
    {
        return url()->previous() . $this->redirectHash;
    }

}
