<?php

namespace App\Http\Controllers\Site;
use Illuminate\Http\Request;
use App\Models\EventStudio;

class StudioEventController extends SiteController {

    public function index(){
        return view( 'site.events.studio.index' );
    }

    public function finishedRequest(){
        return view('site.events.studio.finished');
    }

    public function store(Request $request){

        $eventStudio = new EventStudio;        
        $eventStudio->name   = $request->get('name');
        $eventStudio->email  = $request->get('email');
        $eventStudio->branch = $request->get('branch');
        $eventStudio->mobile = $request->get('mobile');
        $eventStudio->job    = $request->get('job');
        $eventStudio->type   = $request->get('type');
        $eventStudio->note   = $request->get('note');

        if($eventStudio->save()) {
            return redirect()->route('studioEvent.finishedRequest');
        }
    }

}