<?php

namespace App\Http\Controllers\Site;


use App\Models\Comment;
use App\Models\Course;
use App\Models\Instructor;
use App\Models\Event;
use App\Models\Files;
use App\Models\FrontendUser;
use App\Models\Group;
use App\Models\Group_users;
use App\Models\Like;
use App\Models\Notification;
use App\Models\Post;
use App\Models\Question;
use App\Models\Tip;
use App\Models\Video;
use App\Models\CourseType;
use App\Models\Pages;
use App\Models\Evaluation;
use App\Support\Services\AddImagesToEntity;
use App\Support\Services\MiscContainer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Pusher\Pusher;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Route;

use DB;

class ILearnController extends SiteController
{

    public function courses()
    {

        // dd( env('MAIL_USERNAME') );

        $pastCourses = Course::with('images')->where('date_to' ,'<' , Carbon::now())->orderBy('id', 'asc')->paginate(12);
        $currentCourses = Course::with('images')->where('date_to' ,'>' , Carbon::now())->orderBy('position', 'asc')->get();

        $page = Pages::with('metaTags')->where('name','courses')->get()->first();

        return view('site.ilearn.courses', [
            'courses'       => $currentCourses,
            'pastCourses'   => $pastCourses,
            'page'          => $page
        ]);
    }


    public function course($id)
    {
        $course = Course::with('images', 'instructorImage', 'instructors','organizers')->findOrFail($id);

        // dd($course->first());

        return view('site.ilearn.course', ['course' => $course]);
    }

    
    public function events()
    {
        $pastEvents = Event::with('images')->where('date_to' ,'<' , Carbon::now())->orderBy('date_from','desc')->paginate(12);
        $currentEvents = Event::with('images')->where('date_to' ,'>' , Carbon::now())->orderBy('position', 'asc')->get();

        $page = Pages::with('metaTags')->where('name','events')->get()->first();
        return view('site.ilearn.events', [
            'events' => $currentEvents, 
            'pastEvents' => $pastEvents, 
            'page' => $page
        ]);
    }


    public function event($id)
    {
        $event = Event::findOrFail($id);
        return view('site.ilearn.event', ['event' => $event]);
    }

    public function downloadEventDocument($id)
    {
        $event = Event::findOrFail($id);
        return response()->download(DOCUMENT_PATH . $event->document);
    }

    public function groups()
    {
        $groups = Group::whereHas('users', function ($query) {
            return $query->where('frontend_user_id', auth()->user()->id);
        })->with('users')->get()->load([
            'posts' => function ($query) {
                return $query->latest();
            },
            'posts.comments' => function ($query) {
                return $query->latest();
            },
            'posts.comments.frontendUser',
            'posts.likes',
            'posts.frontendUser.photo',
            'posts.images',
            'posts.files'
        ]);

        $page = Pages::with('metaTags')->where('name','groups')->get()->first();
        return view('site.ilearn.groups', compact('groups','page'));
    }

    public function storePost($groupId, Request $request)
    {
        $this->validate($request,[
           'files.*'=>'mimes:docx,docm,dotx,dotm,docb,ppt,pps,pot,pptx,pptm,potx,potm,ppam,ppsx,ppsm,sldx,sldm,jpg,png,jpeg,xlsx,xlsm,xls,pdf,txt',
        ]);
        $group = Group::findOrFail($groupId);


        if(count($request->file('files')) and count($request->file('images')) and $request->file('images')[0]!="" and  $request->file('files')[0]!="" ){

            \Toast::error('Please Attach Only Images Or Files.')->flash();
            return redirect()->back();
        }

        $post = new Post([
            'group_id' => $group->id,
            'frontend_user_id' => auth('frontend')->user()->id,
            'body' => $request->get('body')
        ]);

        if ($post->save()) {
            if (is_array($uploadedImages = array_filter($request->file('images')))) {
                (new AddImagesToEntity($uploadedImages, $post))->execute();
            }
            if(count($request->file('files')) and $request->file('files')[0]!=""){
                foreach ($request->file('files') as $file){
                    $des = public_path('/groups/files');
                    $file_name_Original = $file->getClientOriginalName();
                    $new_file_name= rand() . time() . '.' . $file->getClientOriginalExtension();
                    $file->move($des, $new_file_name);
                    $file_path = '/groups/files/' . $new_file_name;
                    $add_file=new Files();
                    $add_file->post_id=$post->id;
                    $add_file->file_name=$file_name_Original;
                    $add_file->file_path=$file_path;
                    $add_file->save();
                }
            }
            $users=[];
           foreach ($group->users()->where('frontend_user_id','!=',auth('frontend')->user()->id)->get() as $user){
               $users[]=$user->id;
           }
            $data=['link'=>url('school/groups'),'message'=>'new Post Added At '.$group->name.' Group ','title'=>'new post'];
           send_notification_users($users,$data,0);
            \Toast::success('Entry saved successfully.')->flash();
        } else {
            \Toast::error('Request failed, try again later.')->flash();
        }

        return redirect()->route('site.ilearn.groups');
    }





    public function storeComment($postId, Request $request, Pusher $pusher)
    {
        $post = Post::findOrFail($postId);

        $comment = new Comment([
            'post_id' => $post->id,
            'frontend_user_id' => auth('frontend')->user()->id,
            'body' => $request->get('body')
        ]);

        if ($comment->save()) {
            if (auth('frontend')->user()->id != $post->frontend_user_id) {
                $data=['link'=>url('school/groups'),'message'=>'new Comment Added On Your Post','title'=>'Groups'];
                send_notification_users([$post->frontend_user_id],$data,0);
            }

            \Toast::success('Entry saved successfully.')->flash();
        } else {
            \Toast::error('Request failed, try again later.')->flash();
        }

        return redirect()->route('site.ilearn.groups');
    }




    public function storeCommentEnd(Request $request)
    {
        $post = Post::findOrFail($request->post_id);

        $comment = new Comment([
            'post_id' => $post->id,
            'frontend_user_id' => auth('frontend')->user()->id,
            'body' => $request->get('body')
        ]);

        $comment->save();
        if(auth('frontend')->user()->id!=$post->frontend_user_id){
            $data=['link'=>url('school/groups'),'message'=>'New Comment Added To Your Post','title'=>'Groups'];
            send_notification_users([$post->frontend_user_id],$data);
        }

        return view('site.ilearn.ajax.displayComment',['comment' => $comment]);

    }



    public function DelPost($id){
            $del=Post::findOrfail($id);
        if($del->frontend_user_id!=auth('frontend')->id()){
            toast('Error Post','error');
            return redirect()->back();
        }
        $del->delete();
        toast('Done Delete Post');
        return redirect()->back();
    }

    public function DelComment($id){
        $del=Comment::findOrfail($id);
        if($del->frontend_user_id!=auth('frontend')->id()){
            toast('Error Comment','error');
            return redirect()->back();
        }
        $del->delete();
        toast('Done Delete Comment');
        return redirect()->back();
    }

    public function LeaveGroup($id){
        $group=Group_users::where(['group_id'=>$id,'frontend_user_id' => auth('frontend')->id()])->delete();
        toast('Done Leave Group');
        return redirect()->back();
    }

    public function viewMoreComments(Request $request)
    {
         if($request->ajax())
         {

            $post_id = $request->post_id;

            $check = Post::where("id",$post_id)->get()->first();
            if($check)
            {
                $count = $request->count;
                $limit = $count * 5;

                $data['count']      = $count + 1;
                $data['post_id']    = $post_id;
                $data['limit']      = $limit;



                $data['comments'] =  DB::table('comments')
                ->join("frontend_users","comments.frontend_user_id","=","frontend_users.id")
                ->where('post_id',$post_id)
                ->offset(0)
                ->limit($limit)
                ->get();

                //dd($data['limit']);

                 $data['count_table'] =   DB::table('comments')
                 ->where('post_id',$post_id)->count();



                return view('site.ilearn.ajax.displayMoreComents')->with($data);
            }

            //dd($check);
        }
    }




    public function processLike($postId)
    {
        $post = Post::findOrFail($postId);

        $params = [
            'post_id' => $post->id,
            'frontend_user_id' => auth('frontend')->user()->id,
        ];

        $like = Like::where($params)->first();

        if ($like) {
            $like->delete();
        } else {
            if (auth('frontend')->user()->id != $post->frontend_user_id) {
                $data=['link'=>url('school/groups'),'message'=>'New Like At Your Post','title'=>'Groups'];
                send_notification_users([$post->frontend_user_id],$data);
            }

            Like::Create($params);
        }

        return response()->json($post->likes->count());
    }



    public function notifyWriter(Post $post, FrontendUser $user, $action = 'comment')
    {
        if ($action == 'comment') {
            $notificationTitle = 'New Comment';
            $notificationMessage = $user->full_name . ' has commented on your post.';
        } else {
            $notificationTitle = 'New Like';
            $notificationMessage = $user->full_name . ' has liked your post.';
        }

        $notification = new Notification([
            'title' => $notificationTitle,
            'message' => $notificationMessage,
            'link' => route('site.ilearn.groups')
        ]);
        $notification->save();

        $notification->users()->attach([$post->frontend_user_id], ['new' => 1]);

        $pusher = app()->make('Pusher\Pusher');
        $pusher->trigger("user-{$post->frontend_user_id}", 'notification', [
            'title' => $notification->title,
            'message' => $notification->message,
            'link' => route('site.ilearn.groups')
        ]);
    }

    public function videos()
    {
        $videos = Video::where('active', 1)->latest()->paginate(12);
        $page = Pages::with('metaTags')->where('name','videos')->get()->first();
        return view('site.ilearn.videos', compact('videos', 'page'));
    }

    public function tips(Request $request)
    {
        $page = Pages::with('metaTags')->where('name','tips')->get()->first();
        
        if ($request->has('q')) {
            $tips = Tip::with('images')
                       ->where('title', 'LIKE', '%' . $request->get('q') . '%')
                       ->orWhere('description', 'LIKE', '%' . $request->get('q') . '%')
                       ->paginate(15);
            return view('site.ilearn.tips', compact('tips' ,'page'));
        }

        $tips = Tip::with('images')->latest()->paginate(15);
        
        return view('site.ilearn.tips', compact('tips' ,'page'));
    }

    public function evaluation($category)
    {
        $questions = Question::with('image')->where('category', $category)->inRandomOrder()->take(10)->get();
        $page = Pages::with('metaTags')->where('name','evaluations')->get()->first();

        // dd( $questions->toArray() );

        return view('site.ilearn.evaluation', [
            'category' => $category,
            'questions' => $questions,
            'page' => $page
        ]);
    }

    public function reviewEvaluation($category, Request $request, MiscContainer $misc)
    {
        $submittedData = collect($request->get('data'))->keyBy('questionId')->toArray();

        $selectedQues = $request->get('data');
        
        $quesIds = [];
        if ( count($selectedQues) > 0 ) {
            for ($i=0; $i < count($selectedQues) ; $i++) { 
                array_push( $quesIds, $selectedQues[$i]['questionId'] );
            }
        }
        
        $questions = Question::findOrfail($quesIds);
        
        // dd( $newArray , $questions->toArray() );

        $totalMark = $userMark = 0;
        foreach ($questions as $id => $question) {
            $totalMark += $question->mark;

            $rightAnswer = '';
            foreach ($question->answers as $answer) {
                if ($answer->right) {
                    $rightAnswer = $answer->body;
                    break;
                }
            }

            if (array_key_exists($question->id, $submittedData)) {
                $item = $submittedData[$question->id];

                if ($item['answer'] == $rightAnswer) {
                    $userMark += $question->mark;
                }
            }
        }

        $feedback = '';
        foreach ($misc->evaluation_feedback as $item) {
            if ($userMark >= $item['from'] && $userMark <= $item['to']) {
                $feedback = $item['message'];
            }
        }

        $mark = $userMark . '/' . $totalMark;

        $user = auth()->user();

        $evaluation = new Evaluation;

        $evaluation->user_id    = $user->id;
        $evaluation->user_name  = $user->full_name;
        $evaluation->category   = $category;
        $evaluation->mark       = $mark;
        $evaluation->feedback   = $feedback;

        $evaluation->save();

        return response()->json([
            'mark' => $mark,
            'feedback' => $feedback
        ]);
    }
}
