<?php

namespace App\Http\Controllers\Site;
use App\Models\Pages;
use App\Models\Image;
use App\Models\StudioOption;
use App\Support\Services\MiscContainer;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StudioController extends SiteController
{
    public function home(MiscContainer $miscContainer)
    {
        $options = StudioOption::with('image')->latest()->get()->sortBy('position');
        $slides = Image::whereIn('id', $miscContainer->studio_slides)->get();
        $page = Pages::with('metaTags')->where('name','studios')->get()->first();

        return view('site.studio.home', compact('options', 'slides','page'));
    }

    public function calender(Request $request)
    {

        $option = StudioOption::findOrFail($request->option_id);
        $date = Carbon::parse($request->get('date'));

        $allTimes = $this->getTimesList($date);

        foreach ($option->schedules()->where('date', $date)->get() as $schedule) {
            for ($i = 0; $i < count($allTimes); $i++) {
                if (
                    $allTimes[$i]['from'] == $schedule->from->format('H:i') &&
                    $allTimes[$i]['to'] == $schedule->to->format('H:i')
                ) {
                    $allTimes[$i]['available'] = 0;
                }
            }
        }

        return response()->json($allTimes);
    }

    protected function getTimesList($date)
    {
        $allTimes = [];

        for ($i = 0; $i < 24; $i++) {

             
            $allTimes[] = [
                'date' => $date->format('d/m/Y'),
                'from' => date("h:i a", strtotime("$i:00")),
                'to' => date("h:i a", strtotime(($i +1) .":00")),
                'available' => 1
            ];
        }

        return $allTimes;
    }
}