<?php

namespace App\Http\Controllers\Site;

use App\Models\Pages;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ISellController extends SiteController
{
    public function __construct()
    {
        view()->composer('site.isell.home', function ($view) {
            $categories = Category::where('type', 'isell')->orderBy('name')->get();
            $brands = Brand::where('type', 'isell')->orderBy('name')->get();

            $view->with([
                'categories' => $categories,
                'cambrands' => $brands
            ]);
        });

        parent::__construct();
    }

    public function home(Request $request)
    {

        $signedProducts = $this->getSignedProducts($request);
        $products = $this->getProducts($request);
        $page = Pages::with('metaTags')->where('name','isell')->get()->first();
        return view('site.isell.home', compact('products', 'signedProducts','page'));
    }

    public function getSignedProducts(Request $request)
    {
        $builder = Product::with('category', 'brand', 'images')->whereType('isell')->where('available', 1)
            ->where('sign', '!=', '')->latest();

        if ($request->has('q')) {
            $builder->where('name', 'LIKE', '%' . $request->get('q') . '%');
        }

        if ($request->has('category') && $request->get('category') != 0) {
            $builder->where('category_id', $request->get('category'));
        }

        if ($request->has('brand') && $request->get('brand') != 0) {
            $builder->where('brand_id', $request->get('brand'));
        }

        return $builder->get();
    }

    public function getProducts(Request $request)
    {
        $builder = Product::with('category', 'brand', 'images')->whereType('isell')->where('available', 1)
            ->where('sign', '=', '')->latest();

        if ($request->has('q')) {
            $builder->where('name', 'LIKE', '%' . $request->get('q') . '%');
        }

        if ($request->has('category') && $request->get('category') != 0) {
            $builder->where('category_id', $request->get('category'));
        }

        if ($request->has('brand') && $request->get('brand') != 0) {
            $builder->where('brand_id', $request->get('brand'));
        }

        return $builder->get();
    }

    public function fetchProduct($id){

        $product = Product::with('category', 'brand')->whereType('isell')->findOrfail($id);

        // return view('site.isell._modal', compact('product'));
        return view('site.isell.product', compact('product'));
    }

}