<?php

namespace App\Http\Controllers\Site;

use App\Models\Aboutus;
use App\Models\Plan;
use App\Models\Partner;
use App\Models\Pages;
use App\Models\Point;


class PagesController extends SiteController
{

    public function home()
    {
        
            return view('site.home');
    
    }

    public function about()
    {
        $aboutus = Aboutus::latest()->first();
        return response()->json([ 
            'status' => '01',
            'data'=>[
                'aboutus'=>$aboutus
            ]
        ],200);
    }

    public function pixels(){
        $authUser = auth('api')->user() ;

        if(!$authUser){
            return response()->json(['status'=>'02','error'=>['User not found']]);
        }

        $plans = Plan::latest()->get();
        $partners = Partner::latest()->get();
        //$page = Pages::where('name','pixels')->get()->first();
        $records = Point::where('frontend_user_id', $authUser->id)
                    ->sum('count');
        return response()->json([
            'status' => '01',
            'data' =>[
                'plans' => $plans,
                'partners' => $partners,
               // 'page' => $page ,
                'total-pixels' => $records
            ]
            
        ],200);
    }

}
