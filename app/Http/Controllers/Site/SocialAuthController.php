<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Models\FrontendUser;
use App\Support\Services\AddImagesToEntity; 
use Socialize;
use File;

class SocialAuthController extends SiteController
{
  
    public function redirect($provider)
    {
        return Socialize::driver($provider)->redirect();
    }


    public function callback($provider, Request $request)
    {

        if ( (!$request->has('code') || $request->has('denied')) && $provider != 'twitter' ) {
            toast("Please accept our app request.");
            return redirect('/');
        }

        $getInfo = Socialize::driver($provider)->stateless()->user();

        // dd($getInfo);
        
        $user = FrontendUser::where( 'provider_id', $getInfo->getId() )->first();

        // if user first time login
        if ($user == null) {
        
            $userByEmail = FrontendUser::where('email', '=' , $getInfo->getEmail() )->first();
            // if duplicated email
            if ($userByEmail != null && $userByEmail->email == $getInfo->getEmail() ) {
            
                if ( $userByEmail->active == '0' || ( $userByEmail->password == null || $userByEmail->password == '' ) ) {

                    $inputs = $this->getUser($getInfo, $provider, true);

                    $userByEmail->update($inputs);

                    $this->saveUserPhoto( $getInfo, $userByEmail, $provider);

                    toast("Please finish your profile information before login");
                    return redirect("social-signup/" . $userByEmail->provider_id . '/#' );
                    
                } else {

                    if ($userByEmail->provider != '' && $userByEmail->provider != null ) {

                        toast("You already signed up with ". $userByEmail->provider." account '".$userByEmail->email."', please use another email");
                    } else {
                        toast("Email alerady taken: " .$getInfo->getEmail(). " , please use another account ");
                    }

                    return redirect()->route('site.home');
                }
            
            } else {

                if ($provider == 'facebook') {
                    if (!$picture = file_get_contents($getInfo->getAvatar())) {
                        toast("We can't fetch your profile image from facebook , please upload your profile image or sign up with another account");
                    }
                }
                
                $user = $this->getUser($getInfo, $provider, $userByEmail);

                $this->saveUserPhoto( $getInfo, $user, $provider);
                
            }
            
        }

        // dd($user);

        // if user not first time login
        // if user found but not active  ( stage 1 )  * user didnt finish his profile information
        if ( $user->active == 0 ) {

            toast("Please finish your profile information before login");
            return redirect("social-signup/" . $user->provider_id . '/#' );

        } else {

            // if user found and passwrod found try login ( stage 2 ) 
            auth('frontend')->login($user, true);

            if ( auth('frontend')->check() ) {
                
                toast("You've signed in successfully.");
                
                if( auth('frontend')->user()->first_sign == 0  ){
                    $firstUser = FrontendUser::find(auth('frontend')->user()->id);
                    $firstUser->first_sign = 1;
                    $firstUser->save();
                    return redirect('profile/#');
                }

                return redirect('home/#');
            }
            
            toast("Email and password don't match.", "error");
            return redirect('home/#');

        }
    }
    

    
    public function provider($id) {
        
        $user = FrontendUser::where('provider_id', $id)->first();

        if (!$user) {
            abort(404);
        }
        
        // if user found but no password found  ( stage 1 )  * user didnt finish his profile information
        if ( $user->password == '' || $user->password == null ) {   
            toast("Please finish your profile information before login");
        } else {
            
            return redirect()->route('site.home');
        }

        return view('site.auth.signup_provider', [
            "user" => $user
        ]);
    }
    
    
    public function finishRegister($id, Request $request) {
        
        // dd($request->all());

        $user = FrontendUser::where('provider_id', '=' , $id)->firstOrFail();

        $this->isValidInput($request);

        $input = $request->only(
            'first_name', 'last_name', 'email', 'password', 'mobile', 'level',
            'camera_brand', 'camera_model', 'interested_in','gender'
        );

        $input['password'] = bcrypt($request->get('password'));
        $input['active'] = 1;

        if ($request->interested_in) {
            $input['interested_in']  = json_encode($request->interested_in);
        }

        if ($user->update($input)) {

            if ($request->hasFile('photo')) {
                // delete old provider image if exist
                if( $user->photo != null && $user->photo != '' ) $user->photo->delete();
                $user->avatar = '';
                $user->save();
                (new AddImagesToEntity($request->file('photo'), $user, 'photo'))->execute();
            }

            $creds = [
                "email" => $request->get('email'),
                'password' => $request->get('password'),
                'active' => 1
            ];

            if ( auth()->attempt($creds, true)) {
                toast("You've signed in successfully.");
                return redirect('profile/');
            }
        }

        flashMessage('Request failed, try again later', 'error');
        return redirect()->back()->withInput();        
        
    }
    

    protected function isValidInput(Request $request)
    {
        $this->validate($request, [
            'first_name'    => 'required|string|max:255',
            'last_name'     => 'required|string|max:255',
            'email'         => 'required|email|max:255',
            'password'      => 'required|confirmed|min:6|max:255',
            'mobile'        => 'required|string|min:8|max:15|unique:frontend_users,mobile',
            'gender'        => 'required|max:10',
            'level'         => 'nullable|string|max:255',
            'camera_brand'  => 'nullable|numeric',
            'camera_model'  => 'nullable|numeric',
            'interested_in' => 'required|array',
            'photo'         => 'nullable|image|mimes:jpeg,png,jpg|max:2048'
        ]);
    }


    protected function getUser($getInfo, $provider, $returnArray = false) {
        
        $nameArr = (isset($getInfo->name) && $getInfo->getName() != "") ? explode(" ", $getInfo->getName()) : [];

        $avatarPath = $provider != 'facebook' ? $getInfo->getAvatar() : '';

        $userArray = [
            'first_name'    => isset($nameArr[0]) ? $nameArr[0] : '',
            'last_name'     => isset($nameArr[1]) ? $nameArr[1] : '',
            'avatar'        => $avatarPath,
            'email'         => $getInfo->email,
            'active'        => 0,
            'provider'      => $provider,
            'provider_id'   => $getInfo->getId(),
            'provider_email'=> $getInfo->getEmail(),
        ];

        if ( $returnArray ) {
            return $userArray;
        }
            
        $user = FrontendUser::create($userArray);

        return $user;
    }

    private function getFileInstance($path, $fileImage)
    {
        if ($path != '') {
            $file = new UploadedFile($path, $fileImage, 'image/jpg', filesize($path), null, true);
            return $file;
        }
    }

    private function saveUserPhoto($getInfo, $user, $provider) {
        // facebook case only ( row image data )
        if ( $provider == 'facebook' && ($getInfo->getAvatar() != null && $getInfo->getAvatar() != '') ) {
            //if avatar exist
            if (!$picture = file_get_contents($getInfo->getAvatar())) {
                // error
            } else {
                // success
                // 1- create fake image
                $picName = 'uploads/profile/' . $getInfo->getId() . ".jpg";
                $fileName = '/home4/pfppvkmy/public_html/phommunity/public/'.$picName;
                File::put($fileName, $picture);
                // 2- pass img to the correct images folder and save path in database 
                (new AddImagesToEntity( $this->getFileInstance($fileName, $getInfo->getId() . ".jpg") , $user, 'photo'))->execute();
                // 3- delete fake image
                if (file_exists($fileName)) unlink($fileName);
            }
        }
    }


}
