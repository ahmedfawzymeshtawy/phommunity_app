<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\Order;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Models\Misc;

class SiteController extends Controller
{

    protected $sections_names = [
        "monthly"       => "Monthly Courses",
        "course"        => "Courses",
        "event"         => "Events",
        "rent"          => "I-Rent",
        "sell"          => "I-Sell",
        "help"          => "I-Help",
        "studio"        => "Studios",
        "rooms"         => "Rooms",
        "assistant"     => "Assistant & Second Shooters",
        "pixels"        => "Pixels",
        "join"          => "Join Our Team",
    ];


    public function __construct()
    {
        view()->share('currentUser', auth('api')->user());
        //view()->share('misc', app('misc'));
        view()->share('userNotifications');

        $this->shareRequests();
        $this->shareReviewRequired();
        $this->shareUserNotification();
    }

    public function shareUserNotification()
    {
        if (auth()->user()) {
            $currentUser = auth()->user()->id;
            $notifications = DB::table('notifications')
                ->leftJoin('frontend_user_notification', 'notifications.id', '=', 'frontend_user_notification.notification_id')
                ->select('notifications.id as notification_id', 'notifications.title', 'notifications.message','notifications.link')
                ->where('frontend_user_notification.frontend_user_id', '=', $currentUser)
                ->orderBy('notifications.id', 'desc')
                ->take(20)
                ->get();
            $notificationsCount =  DB::table('notifications')
                ->leftJoin('frontend_user_notification', 'notifications.id', '=', 'frontend_user_notification.notification_id')
                ->select('notifications.id as notification_id', 'notifications.title', 'notifications.message'.'notifications.link')
                ->where('frontend_user_notification.frontend_user_id', '=', $currentUser)
                ->where('frontend_user_notification.new', '=', '1')
                ->count();
            $data = ['notificationsCount' => $notificationsCount, 'notifications' => $notifications];
            view()->share('userNotifications', $data);
        }
    }

    protected function shareRequests()
    {
        $requests = new Collection();
        if (auth()->check()) {
            $secondShooterRequests = new EloquentCollection();
            if (auth()->user()->secondShooter) {
                $secondShooterRequests = auth()->user()->secondShooter->orders()->has('frontendUser')->get();
            }
            $secondShooterRequests->load('frontendUser');

            $assistantRequests = new EloquentCollection();
            if (auth()->user()->assistant) {
                $assistantRequests = auth()->user()->assistant->orders()->has('frontendUser')->get();
            }
            $assistantRequests->load('frontendUser');

            $requests = $secondShooterRequests->merge($assistantRequests);
        }

        view()->share('requests', $requests);
    }

    public function shareReviewRequired()
    {
        $reviewRequired = new Collection();
        if (auth()->check()) {
            $reviewRequired = Order::where([
                'frontend_user_id' => auth()->user()->id,
                'status' => 'finished'
            ])->whereIn('type', ['assistant', 'second-shooter'])->get();

            $reviewRequired->load('orderable.frontendUser');
        }

        view()->share('reviewRequired', $reviewRequired);
    }
}