<?php
namespace App\Http\Controllers\Site;

use App\Models\AdminUser;
use App\Models\Assistant;
use App\Models\Course;
use App\Models\Event;
use App\Models\Lab;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Product;
use App\Models\IrentOrders;
use App\Models\IrentOrdersProducts;
use App\Models\SecondShooter;
use App\Models\StudioOption;
use Illuminate\Http\Request;
use Pusher\Pusher;

class OrdersController extends SiteController {

    public function irent(Request $request)
    {
        // dd($request->toArray());
        if ( isset( $request->products ) && $request->filled('products') )  {

            $irentOder = new Order;

            $metaAttributes = [
                'date'          => $request->get('date'),
                'time'          => $request->get('receiving_time'),
                'branch'        => $request->get('branch'),
                'phone_number'  => $request->get('phone_number'),
                'email'         => $request->get('email'),
                'notes'         => $request->get('note'),
                'products'      => $request->get('products'),
            ];

            $irentOder->type    = $this->sections_names['rent'];
            $irentOder->status  = 'new';
            $irentOder->meta    = json_encode($metaAttributes);
            $irentOder->frontend_user_id = auth('frontend')->id();
            
            if($irentOder->save()) {
                
                $data = ['link'=>url('admin/irent/orders'),'message'=>'New Request For I-Rent','title'=> $this->sections_names['rent'] ];
                $userIds = AdminUser::pluck('id')->toArray();
                send_notification_admins($userIds,$data);
                
                \Toast::success('Your Order Has Sent Successfully. We Will Call You To Confirm Your Reservation.')->flash();
                return redirect()->back();
            }   
        }
    }

    public function isell(Request $request)
    {
        $metaAttributes = [
            'client_name' => $request->get('client_name'),
            'client_phone_number' => $request->get('client_phone_number'),
            'client_email_address' => $request->get('client_email_address'),
            'notes' => $request->get('notes'),
        ];

        $product = Product::find($request->get('product_id'));
        if ($product) {
            $product->orders()->create([
                'frontend_user_id' => auth('frontend')->id(),
                'type' => $this->sections_names['sell'],
                'status' => 'new',
                'meta' => json_encode($metaAttributes)
            ]);
        } else {
            Order::create([
                'frontend_user_id' => auth('frontend')->id(),
                'type' => $this->sections_names['sell'],
                'status' => 'new',
                'meta' => json_encode($metaAttributes)
            ]);
        }

        $data = ['link' => route('admin.isell.orders.index'), 'message'=>'New Isell request', 'title' => $this->sections_names['sell'] ];
        $userIds = AdminUser::pluck('id')->toArray();
        send_notification_admins($userIds,$data);
        \Toast::success('Your Order Has Sent Successfully. We Will Call You To Confirm Your Reservation.')->flash();
        return redirect()->back();
    }

    public function course(Request $request)
    {
        $course = Course::findOrFail($request->get('course_id'));
        $course->orders()->create([
            'frontend_user_id' => auth('frontend')->id(),
            'type' => $this->sections_names['course'],
            'status' => 'new',
            'meta' => json_encode([
                'client_name' => $request->get('client_name'),
                'client_phone_number' => $request->get('client_phone_number'),
                'client_email_address' => $request->get('client_email_address'),
                'notes' => $request->get('notes'),
            ])
        ]);
        $data=['link'=>url('admin/school/courses'),'message'=>'New Request For '.$course->name.'','title'=> $this->sections_names['course'] ];
        $userIds = AdminUser::pluck('id')->toArray();
        send_notification_admins($userIds,$data);

        \Toast::success('Your order has been saved successfully.')->flash();
        return redirect()->back();
    }

    public function event(Request $request)
    {
        $event = Event::findOrFail($request->get('course_id'));
        $event->orders()->create([
            'frontend_user_id' => auth('frontend')->id(),
            'type' => $this->sections_names['event'],
            'status' => 'new',
            'meta' => json_encode([
                'client_name' => $request->get('client_name'),
                'client_phone_number' => $request->get('client_phone_number'),
                'client_email_address' => $request->get('client_email_address'),
                'notes' => $request->get('notes'),
            ])
        ]);


        $data=['link'=>url('admin/school/events'),'message'=>'New Request For Event '.$event->name.'','title'=> $this->sections_names['event'] ];
        $userIds = AdminUser::pluck('id')->toArray();
        send_notification_admins($userIds,$data);
        \Toast::success('Your order has been saved successfully.')->flash();
        return redirect()->back();
    }

    public function labOption(Request $request)
    {
        $option = Lab::findOrFail($request->get('lab_option_id'));
        $option->orders()->create([
            'frontend_user_id' => auth('frontend')->id(),
            'type' => $this->sections_names['rooms'],
            'status' => 'new',
            'meta' => json_encode([
                'client_name' => $request->get('client_name'),
                'client_phone_number' => $request->get('client_phone_number'),
                'client_email_address' => $request->get('client_email_address'),
                'notes' => $request->get('notes'),
            ])
        ]);

        $data=['link'=>url('admin/rooms'),'message'=>'New Request For '.$option->title.'','title'=> $this->sections_names['rooms'] ];
        $userIds = AdminUser::pluck('id')->toArray();
        send_notification_admins($userIds,$data);
        \Toast::success('Your Order Has Sent Successfully. We Will Call You To Confirm Your Reservation.')->flash();
        return redirect()->back();
    }

    public function studioOption(Request $request)
    {

        $this->validate($request, [
            'title' => 'required|string|max:255',
            'client_name' => 'required|string',
            'client_phone_number' => 'required|string',
            'client_email_address' => 'required|string',
            'notes' => 'nullable|string',
            'reserve_type' => 'required|string',
            'time' => 'required|string',
            'date' => 'required|string'
        ]);

        $option = StudioOption::findOrFail($request->get('studio_option_id'));
        $option->orders()->create([
            'frontend_user_id' => auth('frontend')->id(),
            'type' => $this->sections_names['studio'],
            'status' => 'new',
            'meta' => json_encode([
                'title' => $request->get('title'),
                'client_name' => $request->get('client_name'),
                'client_phone_number' => $request->get('client_phone_number'),
                'client_email_address' => $request->get('client_email_address'),
                'notes' => $request->get('notes'),
                'reserve_type' => $request->get('reserve_type'),
                'time' => $request->get('time'),
                'date' => $request->get('date'),
            ])
        ]);
        
        $data=['link'=>url('admin/studio-options'),'message'=>'New Request Studio '.$option->title.'','title' => $this->sections_names['studio'] ];
        $userIds = AdminUser::pluck('id')->toArray();
        send_notification_admins($userIds,$data);
        \Toast::success('Your Order Has Sent Successfully. We Will Call You To Confirm Your Reservation.')->flash();
        return redirect()->back();
    }

    // public function assistant(Request $request, Pusher $pusher)
    // {
    //     $assistant = Assistant::where(['frontend_user_id' => $request->get('frontend_user_id')])->firstOrFail();
    //     $assistant->orders()->create([
    //         'frontend_user_id' => auth('frontend')->id(),
    //         'type' => 'assistant',
    //         'new' => 1,
    //         'notes' => $request->get('notes'),
    //         'status' => Order::STATUS_PENDING
    //     ]);

    //     $data=['link'=>url('admin/assistants'),'message'=>'New Request ','title'=> $this->sections_names['assistant'] ];
    //     $userIds = AdminUser::pluck('id')->toArray();
    //     send_notification_admins($userIds,$data);
    //     \Toast::success('Your order has been saved successfully.')->flash();
    //     return redirect()->back();
    // }

    // public function secondShooter(Request $request, Pusher $pusher)
    // {
    //     $shooter = SecondShooter::where(['frontend_user_id' => $request->get('frontend_user_id')])->firstOrFail();
    //     $shooter->orders()->create([
    //         'frontend_user_id' => auth('frontend')->id(),
    //         'type' => 'second-shooter',
    //         'new' => 1,
    //         'notes' => $request->get('notes'),
    //         'status' => Order::STATUS_PENDING
    //     ]);

    //     $data=['link'=>url('admin/assistants'),'message'=>'New Request ','title'=>'SecondShooter'];
    //     $userIds = AdminUser::pluck('id')->toArray();
    //     send_notification_admins($userIds,$data);
    //     \Toast::success('Your order has been saved successfully.')->flash();
    //     return redirect()->back();
    // }

}