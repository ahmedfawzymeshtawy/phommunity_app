<?php

namespace App\Http\Controllers\Site;
use Illuminate\Http\Request;
use App\Models\Ramadan;
use App\Models\RamadanCommentHistory;
use App\Models\RamadanStatusHistory;

class RamadanEventController extends SiteController {

    public function index(){
        return view( 'site.events.ramadan_kareem.index' );
    }

    public function finishedRequest(){
        return view('site.events.ramadan_kareem.finished');
    }

    public function store(Request $request)
    {  
        
        $ramadan = new Ramadan;        
        $ramadan->name = $request->get('name');
        $ramadan->email = $request->get('email');
        $ramadan->branch = $request->get('branch');
        $ramadan->mobile = $request->get('mobile');
        $ramadan->job = $request->get('job');
        $ramadan->courses = json_encode([ 'courses' => $request->get('courses') ]);
        $ramadan->workshops = json_encode([ 'workshops' => $request->get('workshops') ]);
        $ramadan->note = $request->get('note');
        $ramadan->status = 'new';

        if($ramadan->save()) {
            return redirect()->route('ramadanKareem.finishedRequest');
        }
    }

    
    public function updateComment($id, Request $request) {

        $ramadan = Ramadan::findOrFail($id);

        $ramadan->comment = $request->data;

        $ramadan->admin_id = auth('admin')->id() ? auth('admin')->id() : '';

        if ( $ramadan->save() ){
            $commentHistory = new RamadanCommentHistory;
            $commentHistory->ramadan_id = $ramadan->id;
            $commentHistory->user_id = $ramadan->admin_id;
            $commentHistory->comment = $ramadan->comment;
            $commentHistory->save();

            return response()->json(['message' => 'Comment saved successfully.'], 201);
        }

        return response()->json(['message' => 'Comment not saved.'], 500);

    }


    public function updateStutus(Request $request, $id)
    {
        $ramadan = Ramadan::findOrFail($id);
        
        $this->validate($request,[
            'data'=>'required',
        ]);

        $status = $request->get('data');

        if ( trim($status, " ") != '') {
            $ramadan->status = $status;
            if ( $ramadan->save() ){
                $statusHistory = new RamadanStatusHistory;
                $statusHistory->ramadan_id = $ramadan->id;
                $statusHistory->user_id  = auth('admin')->id() ? auth('admin')->id() : '';
                $statusHistory->status   = $ramadan->status;
                $statusHistory->save();

                return response()->json(['message' => 'Status saved successfully.'], 201);
            }
        }
        return response()->json(['message' => 'Status not saved.'], 500);
    }


}