<?php
namespace App\Http\Controllers\Site;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\Order;
use App\Models\Pages;
use Illuminate\Http\Request;
use Carbon\Carbon;
class IRentController extends SiteController
{

    public function __construct()
    {
        view()->composer('site.irent.home', function ($view) {
            $categories = Category::where('type', 'irent')->orderBy('name')->get();
            $brands = Brand::where('type', 'irent')->orderBy('name')->get();

            $view->with([
                'categories' => $categories,
                'brands' => $brands
            ]);
        });

        parent::__construct();
    }
    
    public function home(Request $request)
    {
        $signedProducts = $this->getSignedProducts($request);
        $products = $this->getProducts($request);

        $allProducts = $products->merge($signedProducts);

        $page = Pages::with('metaTags')->where('name','irent')->get()->first();

        return view('site.irent.home', compact('products', 'signedProducts','page', 'allProducts'));
    }


    public function getSignedProducts(Request $request)
    {
        $builder = Product::with('category', 'brand')->whereType('irent')->where('available', 1)
            ->where('sign', '!=', '')->latest();

        if ($request->has('q')) {
            $builder->where('name', 'LIKE', '%' . $request->get('q') . '%');
        }

        if ($request->has('category') && $request->get('category') != 0) {
            $builder->where('category_id', $request->get('category'));
        }

        if ($request->has('brand') && $request->get('brand') != 0) {
            $builder->where('brand_id', $request->get('brand'));
        }

        return $builder->take(20)->get();
    }

    public function getProducts(Request $request)
    {
        $builder = Product::with('category', 'brand')->whereType('irent')->where('available', 1)
            ->where('sign', '=', '')->latest();

        if ($request->has('q')) {
            $builder->where('name', 'LIKE', '%' . $request->get('q') . '%');
        }

        if ($request->has('category') && $request->get('category') != 0) {
            $builder->where('category_id', $request->get('category'));
        }

        if ($request->has('brand') && $request->get('brand') != 0) {
            $builder->where('brand_id', $request->get('brand'));
        }

        return $builder->paginate(12);
    }
    
    public function disableDates()
    {
        $id=$_GET['productId'];
        $orders=Order::where('orderable_id',$id)->get();
        $dates=array();
        foreach($orders as $order)
        {
            if($order->meta('rent_date') !="")
            array_push($dates,$order->meta('rent_date'));
        }
        return $dates;
    }
    
    public function fetchProduct($id){

        $product = Product::with('category', 'brand')->whereType('irent')->findOrfail($id);

        $allProducts = Product::with('category', 'brand','images')->whereType('irent')->where('available', 1)->get();

        $page = Pages::with('metaTags')->where('name','irent')->get()->first();

        // dd($allProducts->first()->toArray());
    
        return view('site.irent.product', compact('product', 'allProducts', 'page'));
    }

    
}
