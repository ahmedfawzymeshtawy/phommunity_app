<?php
namespace App\Http\Controllers\Site;

use App\Http\Requests\FeedbackRequest;
use App\Models\AdminUser;
use App\Models\Feedback;
use App\Models\FrontendUser;
use Illuminate\Http\Request;

class FeedbackController extends SiteController {


    public function store(FeedbackRequest $request) {

        Feedback::create([
            'frontend_user_id' => auth()->user()->id,
            'body' => $request->get('body')
        ]);

        $user = FrontendUser::find(auth()->user()->id);

        $data = [ 'link' => url('admin/feedback'), 'message' => 'new feedback from user ' . $user->first_name . ' ','title' => 'Feedback'];

        $userIds = AdminUser::pluck('id')->toArray();

        send_notification_admins($userIds, $data);
        
        \Toast::success('Feedback saved successfully.')->flash();

        return redirect()->back();
    }
}