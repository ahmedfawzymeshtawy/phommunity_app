<?php

namespace App\Http\Controllers\Site;

use App\Models\FrontendUser;
use App\Models\Wishlist;
use App\Models\Course;
use App\Models\Event;
use App\Models\Product;
use Illuminate\Http\Request;

class WishlistController extends SiteController {

    public function index()
    {
        $userID = auth('frontend')->id();
        
        // $wishlists = Wishlist::with('courses','events')->where('frontend_user_id', $userID)->get();

        $wishlistCourses = Wishlist::where('frontend_user_id', $userID)->where('type', 'course')->get();
        $courses = Course::find($wishlistCourses->pluck('type_id'));

        $wishlistEvents = Wishlist::where('frontend_user_id', $userID)->where('type', 'event')->get();
        $events = Event::find($wishlistEvents->pluck('type_id'));
        
        $wishlistIrentProduct = Wishlist::where('frontend_user_id', $userID)->where('type', 'irent_product')->get();
        $irentProducts = Product::with('images')->find($wishlistIrentProduct->pluck('type_id'));
        
        $wishlistIsellProduct = Wishlist::where('frontend_user_id', $userID)->where('type', 'isell_product')->get();
        $isellProducts = Product::with('images')->find($wishlistIsellProduct->pluck('type_id'));

        // dd($irentProducts);
        
        return view('site.wishlist', [
            'courses' => $courses,
            'events' => $events,
            'irentProducts' => $irentProducts,
            'isellProducts' => $isellProducts
        ]);
    }


    public function store(Request $request)
    {


        $this->validate($request, [
            'type' => 'required',
            'type_id' => 'required'
        ]);

        $user = auth('frontend')->user();

        $oldWishlist = Wishlist::where('frontend_user_id', $user->id)
                                ->where('type', $request->type)
                                ->where('type_id', $request->type_id)
                                ->get();


        if ($oldWishlist->count() == 0) {

            $wishlist = $user->wishlist()->create([
                'frontend_user_id' => auth('frontend')->check() ? auth('frontend')->user()->id : 0,
                'type' => $request->input('type'),
                'type_id' =>  $request->input('type_id'),
            ]);
        }

        return ['status' => 'added'];
    }
    
    public function delete(Request $request)
    {
        $this->validate($request, [
            'type' => 'required',
            'type_id' => 'required'
        ]);

        $user = auth('frontend')->user();
        
        $wishlist = Wishlist::where('frontend_user_id', $user->id)
                            ->where('type', $request->type)
                            ->where('type_id', $request->type_id)
                            ->get();
        
        if ($wishlist->count() >= 1) {
   
            $wishlist->first()->delete();
        }
        
        return ['status' => 'removed'];

    }



    public function removeCourse($id)
    {

        $user = auth('frontend')->user();
        
        $wishlist = Wishlist::where('frontend_user_id', $user->id)
                            ->where('type', 'course')
                            ->where('type_id', $id)
                            ->get();

        // dd($wishlist->toArray());
        
        if ($wishlist->count() >= 1) {
   
            $wishlist->first()->delete();

            \Toast::success('Course Removed')->flash();
        }
        
        return redirect()->back();
    }


    public function removeEvent($id)
    {

        $user = auth('frontend')->user();
        
        $wishlist = Wishlist::where('frontend_user_id', $user->id)
                            ->where('type', 'event')
                            ->where('type_id', $id)
                            ->get();
        
        if ($wishlist->count() >= 1) {
   
            $wishlist->first()->delete();

            \Toast::success('Event Removed')->flash();
        }
        
        return redirect()->back();
    }


    public function removeIrentProduct($id)
    {

        $user = auth('frontend')->user();
        
        $wishlist = Wishlist::where('frontend_user_id', $user->id)
                            ->where('type', 'irent_product')
                            ->where('type_id', $id)
                            ->get();
        
        if ($wishlist->count() >= 1) {
   
            $wishlist->first()->delete();

            \Toast::success('I-Rent Product Removed')->flash();
        }
        
        return redirect()->back();
    }


    public function removeIsellProduct($id)
    {

        $user = auth('frontend')->user();
        
        $wishlist = Wishlist::where('frontend_user_id', $user->id)
                            ->where('type', 'isell_product')
                            ->where('type_id', $id)
                            ->get();
        
        if ($wishlist->count() >= 1) {
   
            $wishlist->first()->delete();

            \Toast::success('I-Sell Product Removed')->flash();
        }
        
        return redirect()->back();
    }


}