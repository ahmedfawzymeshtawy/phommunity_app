<?php

namespace App\Http\Controllers\Site;

use App\Models\Assistant;
use App\Models\Image;
use App\Models\SecondShooter;
use App\Support\Services\AddImagesToEntity;
use Illuminate\Http\Request;
use DB;
class SecondShootersController extends SiteController
{

    public function index(Request $request)
    {
        $builder = SecondShooter::query()
            ->has('frontendUser')
            ->with('frontendUser.photo', 'samples', 'orders')
            ->latest();



        if ($request->has('q')) {
            $builder->whereHas('frontendUser', function ($query) use ($request) {
                return $query->where(DB::raw('concat(first_name," ",last_name)') , 'LIKE', '%' . $request->get('q') . '%')
                    ->orWhere('email', 'LIKE', '%' . $request->get('q') . '%');
            });

        }


        if ($request->has('level')) {
            $builder->whereHas('frontendUser', function ($query) use ($request) {
                return $query->where('level', 'LIKE', '%' . $request->get('level') . '%');
            });
        }


        $shooters = $builder->get();
        $featuredShooters = $shooters->filter(function ($item) {
            return $item->featured == 1;
        });

        return view('site.second_shooters.index', compact('shooters', 'featuredShooters'));
    }

    public function showJoin()
    {
        return response()->json(
            view('site.second_shooters._join')->render()
        );
    }

    public function doJoin(Request $request)
    {
        $this->isValidInput($request);

        $input = $request->only('about', 'area');
        $input['samples'] = is_array($request->file('samples')) ? $request->file('samples') : [];
        $input['featured'] = 0;
        $input['rating'] = 0;
        $input['approved'] = 1;
        $input['frontend_user_id'] = auth('frontend')->user()->id;

        $shooter = new SecondShooter($input);
        if ($shooter->save()) {
            (new AddImagesToEntity($input['samples'], $shooter, 'samples'))->execute();

            flashMessage('You have successfully joined second shooters team.');
            return redirect()->route('site.second_shooters.index');
        }

        flashMessage('Request failed, try again later', 'error');
        return redirect()->back()->withInput();
    }

    public function destroySample($id)
    {
        return Image::destroy($id);
    }

    public function storeSamples(Request $request)
    {
        $files = array_filter($request->file('samples'));
        (new AddImagesToEntity($files, auth()->user()->secondShooter, 'samples'))->execute();

        toast('Samples has been added successfully.');
        return redirect()->back();
    }

    protected function isValidInput(Request $request)
    {
        $this->validate($request, [
            'about' => 'required',
            'area' => 'required'
        ]);
    }

    public function topten(Request $request)
    {
        $builder = SecondShooter::query()
            ->has('frontendUser')
            ->with('frontendUser.photo', 'samples', 'orders')
            ->latest();

        if ($request->has('q')) {
            $builder->whereHas('frontendUser', function ($query) use ($request) {
                return $query->where(DB::raw('concat(first_name," ",last_name)') , 'LIKE', '%' . $request->get('q') . '%')
                    ->orWhere('email', 'LIKE', '%' . $request->get('q') . '%');
            });

        }
        $shoot = $builder->get();
        $shooters = $shoot->filter(function ($item) {
            return $item->featured == 1;
        });

        return view('site.second_shooters.top10', compact('shooters'));
    }
    public function del(){
        $user=auth('frontend')->user()->id;

        $second=SecondShooter::where('frontend_user_id',$user);
        $del_samples=Image::where('imageable_id',$second->first()->id)->where('imageable_type','App\Models\SecondShooter')->delete();
        $del_second=$second->delete();
        toast('Done Deleted From Second Shooters.');
        return redirect()->back();
    }

    public function del_sample($id){
        $sample=Image::findOrFail($id);
        if($sample->imageable_id!=auth('frontend')->user()->id){
            flashMessage('Error You Can\'t delete This Sample', 'error');
        }
        $del=$sample->delete();
        toast('Done Deleted Image From Samples.');
        return redirect()->back();
    }

    public function update(Request $request){
        $update_secound=auth('frontend')->user()->secondShooter->update($request->all());
            $files = array_filter($request->file('samples'));
        (new AddImagesToEntity($files, auth('frontend')->user()->secondShooter, 'samples'))->execute();
        toast('Second Shooter Data has been updated.');
        return redirect()->back();
    }

}