<?php

namespace App\Http\Controllers\Site;

use App\Models\FrontendUser;
use App\Models\AdminUser;
use App\Models\ChatMessage;
use Illuminate\Http\Request;

class ChatsController extends SiteController {

    public function fetchMessages()
    {
        $messages = auth('frontend')->user()->chatMessage()->get();
        return $messages;
    }

    public function sendMessage(Request $request)
    {

        $user = FrontendUser::find(auth('frontend')->user()->id);

        $message = $user->chatMessage()->create([
            'message' => $request->input('message'),
            'frontend_user_id' => auth('frontend')->check() ? auth('frontend')->user()->id : 0,
            'admin_id' => 0,
            'admin' =>  0,
            'seen' =>  0,
        ]);

        $chatMessage = ChatMessage::with('frontendUser', 'adminUser')->find($message->id);

        sendPusherData("chat","chatsend", $chatMessage);

        return ['status' => 'Message Sent!'];
    }

        
    public function messageSeen($id)
    {
        $frontendUser = FrontendUser::findOrFail($id);
        $chatMessage = ChatMessage::where('frontend_user_id', $id)->where('admin', 1)->update(['seen' => 1]);
        return response()->json('success');
    }


}