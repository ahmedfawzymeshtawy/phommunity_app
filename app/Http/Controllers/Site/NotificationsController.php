<?php

namespace App\Http\Controllers\Site;

use App\Models\Notification;
use Carbon\Carbon;
use App\Support\Message\Toast;

class NotificationsController extends SiteController
{
    public function index()
    {
        $currentUser = auth('frontend')->user();
        
        $notifications = \App\Models\Notification::orderBy('id','desc')->where('user_id', $currentUser->id)->where('type',0)->paginate(30);

        // dd($notifications);

        return view( 'site.user-notifications', compact('notifications') );
    }

    public function unread()
    {
        $currentUser = auth()->user();
        Notification::where('user_id',$currentUser->id)->where('type',0)->update(['readed'=>1]);
        return response()->json(['status'=>'done']);
    }


    public function showNotifications()
    {
        $currentUser = auth()->user();
        $notifications = $currentUser->notifications()->latest()->get();
        $currentUser->notifications()
            ->newPivotStatement()
            ->where('frontend_user_id', $currentUser->id)
            ->update(['new' => 0, 'updated_at' => Carbon::now()]);
    }


    public function delete($id){

        $notification = \App\Models\Notification::find($id);

        $notification->delete();

        (new Toast())->success('Entry deleted successfully.')->flash();

        return redirect()->back();
    }


}