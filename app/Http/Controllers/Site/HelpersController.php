<?php

namespace App\Http\Controllers\Site;

use App\Models\AdminUser;
use App\Models\FrontendUser;
use App\Models\Message;
use App\Models\VisitorMessage;
use Illuminate\Http\Request;

class HelpersController extends SiteController {
    
    public function home()
    {
        return view('site.helpers.home');
    }


    public function fetchMessages()
    {
        $userId = auth('frontend')->user()->id;
        $messages = Message::where('frontend_user_id', $userId)->get();
        return $messages;
    }


    public function store(Request $request)
    {
        $authUser = auth('frontend')->user();

        $user = FrontendUser::find($authUser->id);
        
        $message = $user->messages()->create([
            'message' => $request->input('message'),
            'frontend_user_id' => auth('frontend')->check() ? $authUser->id : 0,
            'admin_id' => 0,
            'admin' =>  0,
            'seen' =>  0,
        ]); 
        
        $chatMessage = Message::with('frontendUser', 'adminUser')->find($message->id);
        
        sendPusherData("helper_chat", "helper_chat_send", $chatMessage);

        $data = [
            'link' => route('admin.helpers.home', $authUser->id ),
            'message' => 'New Help Request from user '.$authUser->first_name.' '.$authUser->last_name,
            'title' => $this->sections_names['help'] 
        ];

        $userIds = AdminUser::pluck('id')->toArray();
        send_notification_admins($userIds, $data);

        return ['status' => 'Message Sent!'];
    }


            
    public function messageSeen($id)
    {
        $frontendUser = FrontendUser::findOrFail($id);
        $chatMessage = Message::where('frontend_user_id', $id)->where('new', 1)->update(['new' => 0]);
        return response()->json('success');
    }

    public function storeVisitorMessage(Request $request)
    {
//        $this->validate($request, [
//            'g-recaptcha-response' => 'required|recaptcha'
//        ]);

        VisitorMessage::create([
            'body' => $request->get('body'),
            'sender_name' => $request->get('sender_name'),
            'sender_email_address' => $request->get('sender_email_address'),
            'sender_phone_number' => $request->get('sender_phone_number'),
            'new' => 1,
        ]);

        $data=['link'=>url('admin/visitor-messages'),'message'=>'new visitor messages','title'=>'visitor Messages'];
        $userIds = AdminUser::lists('id')->toArray();
        send_notification_admins($userIds,$data);

        \Toast::success('Message sent successfully.')->flash();

        return redirect()->back();
    }
}