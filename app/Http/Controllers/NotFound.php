<?php
namespace App\Http\Controllers;

class NotFound extends Controller {


    
    public function methodNotFound() {
        return view('errors.404');
    }
}