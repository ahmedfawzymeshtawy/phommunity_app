<?php
namespace App\Http\Middleware;

use Closure;

class AuthenticateFrontend {
    public function handle($request, Closure $next)
    {
        if (!auth()->guard('frontend')->check()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                \Toast::error('Please login to your account to be able to access this section.')->flash();
                return redirect()->guest(route('site.home'));
            }
        }

        return $next($request);
    }
}
