<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'Site\PagesController@home')->name('site.home');
Route::get('reset-password/{token}', 'Api\ForgotPasswordController@showResetPassword');
Route::post('reset-password', 'Site\AuthController@doResetPassword');
 Route::get('activate-account/{token}', 'Site\AuthController@doActivateAccount')->name('site.activate_account.do');
