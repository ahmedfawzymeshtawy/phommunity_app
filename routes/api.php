<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Phommunity Routes
|--------------------------------------------------------------------------
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//App Login Screen 
Route::post('login', 'Api\ApiLoginController@login');
Route::post('register', 'Api\ApiRegisterController@register');
Route::post('password/forget', 'Api\ForgotPasswordController@doForgetPassword');
Route::get('register-brands','Api\ApiRegisterController@Brands');
Route::get('register-interests','Api\ApiRegisterController@Interests');
Route::get('register-brand/models/{id}','Api\ApiRegisterController@BrandsModels');
Route::post('canon', 'Api\ApiCanonController@store');
Route::get('logout', 'Api\ApiAuthController@logout');

//Notification Screen 
Route::get('notifications', 'Api\ApiNotificationsController@index');
Route::get('notifications/unread/', 'Api\ApiNotificationsController@unread');
Route::delete('notifications/{id}', 'Api\ApiNotificationsController@delete'); 

//Settings Screen 
Route::post('change-password', 'Api\ApiProfileController@changePassword');
Route::post('deactivate-account', 'Api\ApiProfileController@deactivateAccount');

//Profile screens
Route::get('profile', 'Api\ApiProfileController@profile');
Route::post('update-profile', 'Api\ApiProfileController@update');

// I-Help section
Route::get('ihelp-messages', 'Api\ApiHelpersController@home');
Route::post('saveihepl-message', 'Api\ApiHelpersController@store');
Route::get('ihelp-messages-seen/{user_id}', 'Api\ApiHelpersController@messageSeen'); // new
Route::post('feedback', 'Api\ApiFeedbackController@sendFeedback');
Route::get('delete-ihelp-message/{id}', 'Api\ApiHelpersController@deleteMessage');
//Menu Screens 
Route::post('join-team', 'Api\ApiJoinController@store');
Route::get('about-us','Site\PagesController@about');
Route::get('pixels', 'Site\PagesController@pixels');

//Courses
Route::get('courses', 'Api\ApiIlearnController@courses');
Route::get('course/{id}', 'Api\ApiIlearnController@course');

//Events
Route::get('events', 'Api\ApiIlearnController@events');
Route::get('event/{id}', 'Api\ApiIlearnController@event');
Route::get('videos', 'Api\ApiIlearnController@videos');

//Groups
Route::get('groups', 'Api\ApiIlearnController@groups');
Route::get('group/{group_id}', 'Api\ApiIlearnController@group');
Route::post('groups/{group_id}/posts', 'Api\ApiIlearnController@storePost');
Route::post('groups/posts/{postId}/comments', 'Api\ApiIlearnController@storeComment');
Route::post('groups/posts/{postId}/likes/{userId}/{auth_id}', 'Api\ApiIlearnController@processLike');
Route::post('groups/leave/{group_id}','Api\ApiIlearnController@LeaveGroup');

//Tips
Route::get('tips', 'Api\ApiIlearnController@tips');
Route::get('tip/{id}', 'Api\ApiIlearnController@tip');

//Orders
Route::post('orders/course', 'Api\ApiOrdersController@course');
Route::post('orders/event', 'Api\ApiOrdersController@event');
Route::post('irent/order', 'Api\ApiOrdersController@irent');
Route::post('isell/order', 'Api\ApiOrdersController@isell');
Route::post('orders/studioOption', 'Api\ApiOrdersController@studioOption');
Route::post('orders/labOption', 'Api\ApiOrdersController@labOption');
Route::post('orders/second-shooter', 'Api\ApiOrdersController@secondShooter');
Route::post('orders/assistant', 'Api\ApiOrdersController@assistant');

//Evalution
Route::get('evaluation/{category}', 'Api\ApiIlearnController@evaluation');
Route::post('evaluation/{category}/review', 'Api\ApiIlearnController@reviewEvaluation');

//Rent 
Route::get('misc', 'Api\ApiIRentController@misc');
Route::get('irent/categoryBrands', 'Api\ApiIRentController@getCategoriesAndBrands');
Route::get('irent', 'Api\ApiIRentController@home');
Route::get('getirentbranches/{productid}', 'Api\ApiIRentController@productbranches'); //new
Route::get('irent/product/{id}', 'Api\ApiIRentController@fetchProduct'); //new

//I-sell
Route::get('isell/categoryBrands', 'Api\ApiISellController@getCategoriesAndBrands');
Route::get('isell', 'Api\ApiISellController@home');
Route::get('getisellbranches/{productid}', 'Api\ApiISellController@productbranches'); //new
Route::get('isell/product/{id}', 'Api\ApiISellController@fetchProduct'); //new

//Studio
Route::get('studio', 'Api\ApiStudioController@home');
Route::get('getstudiotbranches', 'Api\ApiStudioController@productbranches'); //new
Route::post('orders/studio/getTimeSlot', 'Api\ApiStudioController@calender');

//Rooms
Route::get('labs', 'Api\ApiLabController@index');
Route::get('getroomsbranches', 'Api\ApiLabController@productbranches'); //new


//secondShooter
Route::get('secondShooters/{user_id}', 'Api\ApiSecondShooterController@index');
Route::get('secondshooters/topten/', 'Api\ApiSecondShooterController@topten');
Route::post('joinSecondShooter', 'Api\ApiSecondShooterController@doJoin');
Route::post('secondShooter/samples', 'Api\ApiSecondShooterController@storeSamples');
Route::post('secondShooter/samples/{id}', 'Api\ApiSecondShooterController@destroySample');
Route::post('secondShooter/delete', 'Api\ApiSecondShooterController@del');
Route::post('secondShooter/update', 'Api\ApiSecondShooterController@update');


//Assisant 
Route::get('assistants/{user_id}', 'Api\ApiAssistantController@index');
Route::get('assistant/topten', 'Api\ApiAssistantController@topten');
Route::post('joinAssistant', 'Api\ApiAssistantController@doJoin');
Route::post('assistant/delete', 'Api\ApiAssistantController@del');
Route::post('assistant/update', 'Api\ApiAssistantController@update');

//Order 
Route::get('orders', 'Api\ApiProfileController@myOrders');
//Route::get('orders/{id}','Api\ApiProfileController@viewOrder');
Route::post('cancel/order', 'Api\ApiProfileController@cancelOrder');

//Favourite
Route::get('getfavourite','Api\ApiProfileController@Favourite');
Route::post('addfavourite','Api\ApiProfileController@StoreFavourite');
Route::post('deletefavourite','Api\ApiProfileController@deleteFavourite');

Route::post('removeIsellProduct/{id}','Api\ApiProfileController@removeIsellProduct');
Route::post('removeIrentProduct/{id}','Api\ApiProfileController@removeIrentProduct');
Route::post('removeEvent/{id}','Api\ApiProfileController@removeEvent');
Route::post('removeCourse/{id}','Api\ApiProfileController@removeCourse');

//Social Register And Login
Route::post('social-sign','Api\ApiSocialRegister@SocialSignup');
Route::post('finishregister/{provider_id}','Api\ApiSocialRegister@finishRegister');
Route::post('social-login/{provider_id}','Api\ApiSocialRegister@finishRegister');


