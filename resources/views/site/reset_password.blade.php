 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Set up a new password for Phommunity</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <style type="text/css">

        *:not(br):not(tr):not(html) {
            box-sizing: border-box;
        }
            
        body{
            width: 100%;
            height: 100%;
            background-color: #E4E5E6;
            padding-top: 70px;
            font-family: 'Roboto', sans-serif;
        }

        .content{
            width: 508px;
            background-color: #E9BE25;
            border-radius: 50px;
            margin: auto;
            padding: 24px;
        }
        .text{

            height: 70%;
            width: 500px;
            margin: auto;
            background-color: #E4E5E6;
            border-radius: 30px;
            text-align: center;
            overflow: hidden;
        }
        
        .button{
            height: 70px;
            width: 200px;
            text-align: center;
            text-decoration: none;
            color: white;
            background-color: #3C3C3B;
            margin: auto;
            line-height: 66px;
            border-radius: 12px;
        }
    </style>
</head>
<body>

    <div class="content">

        <div class="text">

            <br />

            <img src="{{ asset('/images/logo.png') }}" style="height: 135px"/>
            

                <form id="editProfile" action="{{ route('site.reset_password.do', ['token' => request('token')]) }}" method="post">

                        @csrf

                        
                        <div class="form-control">
                                    <input type="password" name="password" id="password" class="form-control"
                                        placeholder="Password" required minlength="6"
                                        value="{{ old('password') }}">
                                </div>
                        <br />
                        <div class="form-control">
                                <input type="password" name="password_confirmation" id="password" class="form-control"
                                        placeholder="Confirmation" required
                                        value="{{ old('password_confirmation') }}">
                                </div>

                      
                        <div class="clearfix"></div>

                        <input type="hidden" name="token" value="{{$token}}">

                        <button type="submit" class="button" style="font-size: 16px; font-family: 'Roboto', sans-serif;">Reset</button>
                    
                    </form>

            


    </div>

    <br />
    <br />

    <p style="text-align: center; font-size: 16px; font-family: 'Roboto', sans-serif;">Copyrights &copy; Cairo Photography Club {{ date('Y') }}.</p>

</body>
</html>
