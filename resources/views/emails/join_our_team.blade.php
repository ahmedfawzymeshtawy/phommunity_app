<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Phommunity</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">

    <style type="text/css">

        body {
            width: 100% !important;
            height: 100%;
            margin: 0;
            line-height: 1.4;
            background-color: #e5e5e5;
            -webkit-text-size-adjust: none;
			font-family: 'Roboto', sans-serif;
        }

        .mail-wrapper {
            width: 420px;
            margin: 20px auto 0;
            padding: 18px 0;
            background:  #eabf25;
			border-radius: 20px;
        }

        .mail_box {
            width: 91%;
			margin: 0 auto;
            background: #e5e5e5;
			border-radius: 20px;
            text-align: center;
			padding-top: 10px
        }
		
		.mail_logo {
			width: 150px;
			margin: 0 auto;
		}
		
		.mail_logo img {
			width: inherit
		}
		
		.mail_message {
			padding: 10px 20px;
			
		}
		
		.mail_message p {
			font-size: 1.2em;
			font-weight: bold;
			text-align: left;
			margin: 5px 0;
			word-break: break-all;
            font-family: 'Roboto', sans-serif !important;
		}
		
		.mail_message span {
			font-weight: normal;
			font-size: 1em;
			font-weight: 400
		}
		
		.mail_btn {
			background: #403B3E;
			border: 2px solid #403B3E;
			width: 150px;
			text-align: center;
			margin: 18px auto 0;
			border-radius: 8px;
			padding: 0 10px;
            color: #000;
			font-weight: bold;
			font-family: 'Roboto', sans-serif;
		}
		
		.mail_btn a {
			display: block;
			color: #fff;
			width: inherit;
			height: inherit;
			padding: 15px 0;
			text-decoration: none;
			font-weight: bold;
			font-family: 'Roboto', sans-serif;
		}
		
		.no_cv { background: none; padding: 15px 0; }
		
    </style>
</head>
<body>
    <div class="mail-wrapper" width="100%">
        <div class="mail_box">
            <div class="mail_logo">
                <img src="{{ asset('assets/site/images/mail-logo.png') }}">
            </div>
            <!-- mail_logo -->
            <div class="mail_message">
				<p><span>Name: </span> {{$join->name}}</p>
				<p><span>Job Title: </span> {{$join->job}}</p>
				<p><span>Email: </span> {{$join->email}}</p>
				<p><span>Note: </span> {{$join->note}}</p>
            </div>
            <!-- mail_message -->         
        </div>
        <!-- mail_box -->
		<div class="mail_btn {{ $join->cv == '' ? 'no_cv' : ''}}">
		@if( $join->cv != '' )
			 <a href="{{'http://phommunity.com/uploads/documents/' . $join->cv}}"> Download CV </a>
		@else
			  No CV Uploaded
		@endif
		</div>
		<!-- mail_btns -->
    </div>
    <!-- mail-wrapper -->
</body>
</html>