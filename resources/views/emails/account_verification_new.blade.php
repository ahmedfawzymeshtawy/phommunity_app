
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Activate your account for Phommunity</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">

    <style type="text/css">

        *:not(br):not(tr):not(html) {
            box-sizing: border-box;
        }
            
        body{
            width: 100%;
            height: 100%;
            background-color: #E4E5E6;
            padding-top: 70px;
            font-family: 'Roboto', sans-serif;
        }

        .content{
            width: 508px;
            background-color: #E9BE25;
            border-radius: 50px;
            margin: auto;
            padding: 24px;
        }
        .text{

            height: 70%;
            width: 500px;
            margin: auto;
            background-color: #E4E5E6;
            border-radius: 30px;
            text-align: center;
            overflow: hidden;
        }
        
        .button{
            height: 70px;
            width: 200px;
            text-align: center;
            text-decoration: none;
            color: white;
            background-color: #3C3C3B;
            margin: auto;
            line-height: 66px;
            border-radius: 12px;
        }

    </style>

</head>
<body>

    <div class="content">

        <div class="text">

            <br />

            <img src="{{ asset('/img/phommunity-logo-06.png') }}" style="height: 135px"/>
            <p style="font-size: 25px; font-family: 'Roboto', sans-serif;">Just one more step</p>
            <h3 style="font-size: 28px; font-family: 'Roboto', sans-serif;">{{ $user->first_name }} {{ $user->last_name }}</h3>
            <p style="font-size: 25px; font-family: 'Roboto', sans-serif;">
                Click The button below to activate 
                <br />
                your 
                <img style="vertical-align: middle;max-width: 100px" src="{{ asset('/img/phommunity-word-2.png') }}" alt="Phommunity logo"> 
                account
            </p>

            <br />

        </div>

        <br />

        <a href="{{ route('site.activate_account.do', $user->verification_token) }}" style="text-decoration: none" >

            <div class="button" style="font-size: 16px; font-family: 'Roboto', sans-serif;">
                Activate Your Account
            </div>
        </a>

    </div>

    <br />
    <br />

    <p style="text-align: center; font-size: 18px; font-family: 'Roboto', sans-serif;">If you’re having trouble with the button above, copy and paste the URL below into your web browser.</p>
    <p style="text-align: center; font-size: 18px; font-family: 'Roboto', sans-serif;">{{ route('site.activate_account.do', $user->verification_token) }}</a>
    <p style="text-align: center; font-size: 16px; font-family: 'Roboto', sans-serif;">Copyrights &copy; Cairo Photography Club {{ date('Y') }}.</p>

</body>
</html>
